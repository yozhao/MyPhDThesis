%-------------------------------------------------------------------------------
\section{Event selection}
\label{sec:selection}
%-------------------------------------------------------------------------------

As a first step, $WW$ candidate events are selected by requiring two oppositely charged, different-flavour leptons ($e$ or $\mu$). Both leptons must satisfy the minimal quality criteria discussed in Section~\ref{sec:objects}. When ordered in $\pT$, these leptons are called the leading and subleading ones, $\pT^{\ell,{\text{(sub)lead}}}$.
In order to suppress the background  from diboson processes, a veto is imposed on events with an additional lepton with $\pT^{\ell, \text{other}}>15$\GeV.

Table~\ref{tab:selcomp_lvlv} summaries the selections and the definition of signal regions (SRs).
The variables used in the selections are the most discriminating ones chosen by a boosted decision tree (BDT)~\cite{Hocker:2007ht}, based on the NWA signal samples. These are $\pt^{\ell,\text{lead}}$, the invariant mass of the leading and subleading leptons, $m_{\ell\ell}$, and the pseudorapidity difference between the two  leptons, $\Delta\eta_{\ell\ell}$. The first two variables provide good separation between a heavy resonance signal and the $WW$ and top-quark background. The separation of signal from background based on the $\Delta\eta_{\ell\ell}$ distribution is found to have a reasonable efficiency and allows, at the same time, a control region to be defined for the $WW$ background (Section~\ref{sec:ww_bkg}). For each selected variable, the selection criterion is set by maximising the signal significance in the presence of background. The optimised selection is checked to be applicable to the LWA signals. 
\begin{table}[tbp]
\center
\caption{Selection conditions and phase space definitions used in the ggF and VBF signal regions. 
}
\label{tab:selcomp_lvlv}
    \vspace{2mm}
\resizebox{\columnwidth}{!}{
\begin{tabular}{|c|c|c|}\hline
 SR$_\text{ggF}$ & SR$_\text{VBF1J}$ & SR$_\text{VBF2J}$ \\\hline
\multicolumn{3}{|c|}{Common selections}\\\hline
\multicolumn{3}{|c|}{    $N_{\text{$b$-tag}}=0$ }\\
\multicolumn{3}{|c|}{    $|\Delta \eta_{\ell\ell}|<1.8$} \\
\multicolumn{3}{|c|}{    $m_{\ell\ell}>55$\GeV}\\
\multicolumn{3}{|c|}{    $\pt^{\ell,\text{lead}}>45$\GeV}\\
\multicolumn{3}{|c|}{    $\pt^{\ell,\text{sublead}}>30$\GeV}\\
\multicolumn{3}{|c|}{veto if $\pt^{\ell,\text{other}}>15$\GeV}\\
\multicolumn{3}{|c|}{    $\max(\mT^W)>50$\GeV}\\\hline
ggF phase space & VBF1J phase space & VBF2J phase space \\\hline
Inclusive in $N_{\text{jet}}$ but excluding  & $N_{\text{jet}}=1$ and & $N_{\text{jet}}\geq 2$ and \\
VBF1J and VBF2J phase space & $|\eta_j|>2.4$, $\min(|\Delta\eta_{j\ell}|)>1.75$ & $m_{jj}>500$\GeV, $|\Delta y_{jj}|>4$ \\\hline
\end{tabular}   
}
\end{table}

In order to further suppress the top-quark background, events with at least one $b$-tagged jet ($N_{\text{$b$-tag}}\geq 1$) are rejected from the signal regions. 
To reduce the $Z+$jets and $W+$jets contributions, two other variables are used: $\pt^{\ell,\text{sublead}}$ and the maximum value of the transverse mass calculated with either of the two leptons and the missing transverse momentum, $\mT^W$. The latter variable is defined as: 
\begin{equation}
\mT^W=\sqrt{2\pt^\ell\et^\text{miss}\left(1-\cos(\phi^\ell-\phi^{\et^\text{miss}})\right)}\,, \nonumber
\end{equation} 
where $\pT^\ell$ and $\phi^\ell$ are the transverse momentum and azimuthal angle of a given lepton and $\phi^{\eT^\text{miss}}$ is the azimuthal angle of the missing transverse momentum vector.

Three event categories are defined: two disjoint categories optimised for the VBF production, VBF $N_\text{jet}=1$ and VBF $N_\text{jet}\ge 2$ (SR$_\text{VBF1J}$ and SR$_\text{VBF2J}$), and one quasi-inclusive category (excluding the VBF phase space) dedicated to the ggF or qqA signal (SR$_\text{ggF}$). 
% for the three event categories, SR$_\text{VBF1J}$, SR$_\text{VBF2J}$ and SR$_\text{ggF}$, respectively.
For the VBF $N_\text{jet}=1$ category, two discriminating variables are used to minimise the contribution 
of the ggF signal: the pseudorapidity of the jet, $\eta_j$, and the minimum value of the pseudorapidity difference between the jet and either of the leptons, $\min(|\Delta\eta_{j\ell}|)$.  
For the VBF $N_\text{jet}\geq 2$ category, the invariant mass, $m_{jj}$, and the rapidity difference, $\Delta y_{jj}$, of the two leading jets are used to select the VBF signal. 

The NWA and LWA signal acceptance times the efficiency, after all selection requirements for a 700\GeV ggF signal, is approximately 50\% in the quasi-inclusive ggF category and 5\% or less in the VBF $N_\text{jet}=1$ and $N_\text{jet}\geq 2$ categories. For a 700\GeV VBF signal, it is between 15\% and 25\% for the three event categories. The acceptance times efficiency for the three event categories combined, as a function of resonance mass, is shown  
in Figure~\ref{fig:acceptance} for the different signals. For the spin-1 and spin-2 signals, the range up to 1\TeV is considered in the case of VBF model processes. 
For samples with lower resonance masses, the acceptance times efficiency is lower because the leptons are softer.
This is also the reason why the search is limited to signal mass values greater than about 200\GeV. 
The same selection is applied to all models and the different selection efficiencies between the models are mainly due to different $\Delta \eta_{\ell\ell}$ distributions for the different spin states.

\begin{figure}[tbp]
 \centering
 \includegraphics[width=0.49\linewidth]{fig/paper/AccggF.pdf}
 \includegraphics[width=0.49\linewidth]{fig/paper/AccVBF.pdf}
 \caption{Acceptance times efficiency as a function of signal mass for the ggF or qqA (left) and VBF (right) productions. All three signal event categories are combined. The hatched band around the NWA signal curve shows the typical size of the total statistical and systematic uncertainties.}
 \label{fig:acceptance}
\end{figure}

The discriminating variable used for the statistical analysis (Section~\ref{sec:results}) in this search is the transverse mass defined as
\begin{equation}
 \mT = \sqrt{\left(\ET^{\ell\ell} + \met\right)^2 - \left|\vpTll + \vmet\right|^2}\,, \nonumber
\end{equation}
  where
\begin{equation}
  \ET^{\ell\ell} = \sqrt{\left| \vpTll \right|^2 + m_{\ell\ell}^2}\,, \nonumber
\end{equation}
and $\vpTll$ is the transverse momentum vector of the leading and subleading leptons.
