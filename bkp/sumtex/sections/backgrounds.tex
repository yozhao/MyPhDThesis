%-------------------------------------------------------------------------------
\section{Background estimation}
\label{sec:backgrounds}
%-------------------------------------------------------------------------------

The dominant background for the $e\nu\mu\nu$ final state is due to events with top quarks and due to SM $WW$ events.
Additional contributions to the background arise from $V+$jets and the diboson
processes $VZ$, $V\gamma$ and $V\gamma^\ast$. 
Since the discriminating variable used for this search is the transverse mass, $\mT$, 
both the normalisation 
and the shape of the background $\mT$ distribution must be estimated.
The shape of the background is modelled using simulated events 
while the top-quark and $WW$ background normalisations are
determined by a simultaneous fit (Section~\ref{sec:results}) to the data in $\mT$-binned distributions in the signal regions and the total  event yields in control regions. 
The normalisation factors of the fit, named ``post-fit'' normalisation factors~\footnote{The post-fit normalisation factors are checked to be consistent within the quoted uncertainties with the pre-fit ones obtained using the control regions only.} hereafter, provide the best overall matching between the number of observed data events and the corresponding SM background expectations in all the signal and control regions.
The control regions are defined by criteria similar to those used for the signal regions, 
but with some requirements loosened or reversed to obtain signal-depleted
samples, enriched in the relevant background. These criteria are summarised in Table~\ref{tab:selcompback_lvlv}.

The following subsections describe the methods used to estimate the most 
important background processes, namely top quark, $WW$, and $W+$jets. 
The $Z/\gamma^\ast+$jets and non-$WW$ diboson background contributions are small.
The $Z/\gamma^\ast+$jets Monte Carlo (MC) samples are normalised using NNLO cross sections~\cite{Melnikov:2006kv} and the non-$WW$ ones with NLO cross sections from the \textsc{Sherpa} event generator. The small background
from the $m_h\simeq 125$\GeV Higgs boson resonance and its 
off-shell component is included and its interference with the continuum $WW$ background is taken into account.


\subsection{Top-quark background}

Events containing top quarks can be produced as a $t\bar{t}$ pair or as a single top quark in association with either a $W$ boson or a quark of another flavour. In this 
analysis, contributions from $t\bar{t}$ and single-top-quark events are estimated 
together, with their relative contributions determined by their predicted 
cross sections and by their relative acceptances  obtained from MC simulation. The single-top-quark contribution varies from about 10\% to 30\% depending on the signal event category.

The normalisation of the top-quark background for the quasi-inclusive ggF category is 
determined in a control region (Top CR$_\text{ggF}$) where one jet is required to be $b$-tagged
in addition to the signal region selection.
The purity of the top-quark background in this CR is high (97\%) and thus allows the modelling of the MC simulation to be validated.
The distribution of the simulated leading lepton $\pT$ in the Top CR$_\text{ggF}$ is found to 
disagree with the data and the ratio between the data and the simulation decreases with increasing $\pT^{\ell,\text{lead}}$.
The simulated distribution is corrected in the $\text{SR}_{\text{ggF}}$ and corresponding CRs with factors obtained by fitting the ratio with a linear function.
The correction varies between $+4\%$ and $-10\%$ as $\pT^{\ell, \text{lead}}$ increases from 50\GeV to 200\GeV.

The top-quark background control regions for the VBF categories (Top CR$_\text{VBF}$) have a small number of data events and are therefore merged. At least one jet is required to be $b$-tagged. In addition, the selection thresholds imposed on  $m_{\ell\ell}$ and $\pt^{\ell, \text{(sub)lead}}$ are relaxed to 10\GeV and 25\GeV, respectively, and the selection on $|\Delta\eta_{\ell\ell}|$ and $\max(\mT^W)$ is removed. The threshold value on $m_{\ell\ell}$ of 10\GeV is used to suppress background contributions from low-mass resonances decaying into different-flavour final states via $\tau^+\tau^-$.
In this control region, the purity of the top-quark background is 96\%, and no mis-modelling of the $\pT^{\ell,\text{lead}}$ distribution is observed. 
\begin{table}[tbp]
\center
\caption{Summary of all the selections used in the ggF and VBF  $WW$ and top-quark control regions. 
The  common selection ``veto if $\pT^{\ell, \text{other}}>15$\GeV'' applied to all the regions is not explicitly shown.
}
\label{tab:selcompback_lvlv}
\vspace{2mm}
\begin{tabular}{|c|c|c|c|}\hline
\hspace {0.4cm} $WW$ CR$_\text{ggF}$  &  Top CR$_\text{ggF}$  &  $WW$ CR$_\text{VBF1J}$  &  Top CR$_\text{VBF}$ \\\hline
$N_{\text{$b$-tag}}=0$ & $N_{\text{$b$-tag}}=1$ & $N_{\text{$b$-tag}}=0$ & $N_{\text{$b$-tag}}\geq 1$ \\
$|\Delta \eta_{\ell\ell}|>1.8$ & $|\Delta \eta_{\ell\ell}|<1.8$ & ($|\Delta \eta_{\ell\ell}|>1.8$ or & -- \\
\multicolumn{2}{|c|}{$m_{\ell\ell}>55$\GeV} & $10\GeV<m_{\ell\ell}<55$\GeV) & $m_{\ell\ell}>10$\GeV \\
\multicolumn{2}{|c|}{$\pt^{\ell,\text{lead}}>45$\GeV} & \multicolumn{2}{c|}{$\pt^{\ell,\text{lead}}>25$\GeV} \\ 
\multicolumn{2}{|c|}{$\pt^{\ell,\text{sublead}}>30$\GeV} & \multicolumn{2}{c|}{$\pt^{\ell,\text{sublead}}>25$\GeV} \\
\multicolumn{2}{|c|}{$\max(\mT^W)>50$\GeV} & \multicolumn{2}{c|}{--} \\\hline
\multicolumn{2}{|c|}{Excluding VBF1J and} & VBF1J & VBF1J and VBF2J \\
\multicolumn{2}{|c|}{VBF2J phase space}  & phase space & phase space \\
\hline
\end{tabular}   
\end{table}

The post-fit normalisation factors from the simultaneous fit 
are $0.96\pm 0.05$ and $1.12^{+0.13}_{-0.12}$ in the ggF and the VBF control regions, respectively, where the uncertainty quoted corresponds to the combined statistical and systematic uncertainties.

Figure~\ref{fig:topCR_lvlv} shows the $\mT$ distributions in the 
ggF and VBF top-quark CRs. The different background components are scaled 
according to the event yields obtained from the simultaneous fit. 
In the control regions the fit uses only the integrated event yields. The shape of the distributions is compared between data and MC predictions and found to be in good agreement after the application of the $\pT^{\ell, \text{lead}}$ correction described above for the ggF top-quark CR.
The shapes of the $\mT$ distribution for 700\GeV and 2\TeV NWA Higgs boson signals are also shown, normalised to the expected limits on $\sigma_H \times B(H\to WW)$ from this analysis.
The ggF contribution from the SM Higgs boson is included in the $WW$ component. The SM Higgs boson VBF contribution is negligibly small and is not shown in this and following figures.

\begin{figure}[tbp]
 \centering
\includegraphics[width=0.48\textwidth]{fig/paper/emme-InclusiveTopCR-MTWide-log.pdf}
\includegraphics[width=0.48\textwidth]{fig/paper/emme-VBFTopCR-MTWide-log.pdf}
 \caption{Transverse mass distribution in the ggF (left) and VBF (right) top-quark 
control regions. In each plot, 
the last bin contains the overflow. The hatched band in the upper and lower panels shows the combined statistical, experimental and theoretical uncertainties in the predictions. The arrow in the lower right panel indicates that an entry is outside of the vertical scale.
The top-quark and $WW$ background event yields are scaled using the indicated normalisation factors obtained from the simultaneous fit to all signal and control regions.
The heavy Higgs boson signal event yield, normalised to the expected limits on $\sigma_H\times B(H\to WW)$, is shown for masses of 700\GeV and 2\TeV in the NWA scenario.}  
\label{fig:topCR_lvlv}
\end{figure}


\subsection{$WW$ background}\label{sec:ww_bkg}

The $WW$ CR for the quasi-inclusive ggF category ($WW$ CR$_\text{ggF}$) uses the same selection as for the SR except for $|\Delta\eta_{\ell\ell}|$ which is reversed so that the CR and SR are orthogonal. The selection conditions are shown in Table~\ref{tab:selcompback_lvlv}.
The $\mT$ distributions of the $q\bar{q}\to WW$ \textsc{Sherpa} MC sample in the SR$_\text{ggF}$ and $WW$ CR$_\text{ggF}$ are compared at MC generator level with corresponding predictions combining  NNLO QCD calculations~\cite{NNLOQCD} with NLO electroweak (EW) corrections~\cite{NLOEW}. 
While the integrated yields of the distributions agree within 3\% in both the SR$_\text{ggF}$ and the $WW$ CR$_\text{ggF}$, a small $\mT$ shape difference is observed, particularly in the SR. The $\mT$ distributions of the \textsc{Sherpa} samples are thus reweighted to the combined NNLO QCD and NLO EW predictions.
The post-fit normalisation factor obtained from the simultaneous fit for the  $WW$ contributions in the quasi-inclusive ggF categories
is $1.14\pm 0.09$, where the uncertainty quoted corresponds to the combined statistical and systematic uncertainties. The post-fit purity of the $WW$ background in the control region is $51\%$.

In order to select more data events, the $WW$ CR for the $N_\text{jet}=1$ VBF category ($WW$ CR$_\text{VBF1J}$) uses a slightly different selection (shown in Table~\ref{tab:selcompback_lvlv}) from the one in the SR, but still disjoint from the SR. 
The normalisation factor obtained from the same simultaneous fit for the $WW$ contribution in the
$WW$ CR$_\text{VBF1J}$ is $1.0\pm 0.2$, where the uncertainty quoted corresponds to the combined  statistical and systematic uncertainties. The post-fit purity of the $WW$ background in the control region is $44\%$.

The $WW$ contribution in the $N_\text{jet}\geq 2$ VBF 
category is about 20\%, and its prediction is taken from simulation because it is difficult to isolate
a kinematic region with a sufficient number of $WW$ events and with a small 
contamination from the top-quark background.

Figure~\ref{fig:WWCR_lvlv} shows the $\mT$ distributions in the $WW$ CR$_\text{ggF}$ and CR$_\text{VBF1J}$. The different background contributions are scaled according to the event yields obtained from the simultaneous fit. 
For the $WW$ control regions only integrated event yields are used in the fit, like in the fits of the top control regions.

\begin{figure}[tbp]
 \centering
 \includegraphics[width=0.48\textwidth]{fig/paper/emme-InclusiveWWCR-MTWide-log.pdf}
 \includegraphics[width=0.48\textwidth]{fig/paper/emme-VBFWWCR1J-MTWide-log.pdf}
 \caption{Transverse mass distribution in the quasi-inclusive ggF (left) and $N_\text{jet}=1$ VBF $WW$ 
(right) control regions. In each plot, 
the last bin contains the overflow. The hatched band in the upper and lower panels shows the combined statistical, experimental and theoretical uncertainties in the predictions. The top-quark and $WW$ background events are scaled using the indicated normalisation factors obtained from the simultaneous fit to all signal and control regions. The heavy Higgs boson signal event yield, normalised to the expected limits on $\sigma_H\times B(H\to WW)$, is shown for masses of 700\GeV and 2\TeV in the NWA scenario.}
 \label{fig:WWCR_lvlv}
\end{figure}


\subsection{$W+$jets background}\label{sec:wjets_lvlv}

 Events with $W$~bosons produced in association with jets may enter the SR when a jet is misidentified as a lepton. Due to the difficulties in accurately modelling the misidentification process in the simulation,
the $W+$jets background contribution is estimated using the 
data-driven method developed for the SM $h\rightarrow WW$ analysis~\cite{ATLAS:2014aga}.
A sample of events is used which satisfies all event selection criteria, except that one of the two lepton candidates fails to meet the quality criteria for being an identified
lepton but satisfies a less restrictive selection, referred to as ``anti-identified''. Anti-identified muons (electrons) have loosened isolation and impact parameter (likelihood identification) selection criteria as compared to the identified selection.
From this data sample the non-$W+$jets contribution, dominated by top-quark and $WW$ background processes, is subtracted on the basis of MC predictions.
 The $W+$jets purity of the samples is 46\%, 59\% and 22\% for the quasi-inclusive ggF, $N_{\text{jet}}=1$ and $N_{\text{jet}}\geq 2$ VBF categories, respectively.

The $W+$jets contamination in the signal region is then determined by scaling the number of events in the background-subtracted data sample by an extrapolation factor, which is the ratio of the number of 
identified leptons to the number of anti-identified leptons in a data sample of dijet events in bins of lepton $\pt$ and $\eta$.
The dijet sample is collected using prescaled low-$\pT$ single-lepton triggers with thresholds of 12\GeV for electrons and 14\GeV for muons.
Events are selected with exactly one candidate lepton, back-to-back with the leading jet. Electroweak processes in the dijet event sample, dominated by $W$+jets and $Z/\gamma^\ast$ background contributions, are subtracted. 
The dominant systematic uncertainty in the estimation of the $W+$jets background is due to the differences between dijet and $W+$jets sample characteristics. 
All systematic uncertainties associated with this background estimate are listed in Section~\ref{sec:wjetssys}.

