\clearpage
\section{Introduction} \label{introduction}

\pagenumbering{arabic}
\setcounter{page}{1} 

%%The measured properties~\cite{atlas:combined-paper-run1,HIGG-2013-17,cms:comb-paper-run1,HIGG-2015-07}  of the Higgs boson  discovered in 2012 by the ATLAS~\cite{ATLASHiggsDiscovery} 
%%and CMS~\cite{CMSHiggsDiscovery} collaborations at the Large Hadron Collider (LHC) are, within experimental uncertainties, consistent with those predicted for the Standard Model (SM) Higgs boson, $h$. Nevertheless, the SM is thought to be an incomplete theory and many scenarios beyond the SM (BSM) predict an extended Higgs sector~\cite{deFlorian:2016spz,Ivanov:2017dad}. Diboson vector and tensor resonances are also predicted in several other extensions to the SM, such as in composite Higgs models~\cite{Agashe:2004rs,Giudice:2007fh} and models with warped extra dimensions~\cite{Randall:1999ee,Agashe:2007zd,Antipin:2007pi,Antipin:2008hj}.
%%
%%This article reports on the results of a search for heavy neutral resonances decaying into two $W$ bosons, which then decay into the $e\nu\mu\nu$ final state, either directly or via leptonic tau decays with additional neutrinos.
%%The analysis is based on the full $pp$ collision dataset collected by the ATLAS detector in 2015 and 2016 at the centre-of-mass energy of $\sqrt{s}=13$\TeV, corresponding to an integrated luminosity of 36.1\,fb$^{-1}$. 
%%
%%The results are interpreted in terms of different benchmark models. For the case of a scalar resonance
%%produced by gluon--gluon fusion (ggF) or vector-boson fusion (VBF), two scenarios with different intrinsic widths are considered. Constraints on the heavy neutral scalar in two-Higgs-doublet models (2HDM) are also obtained. The neutral member of the fiveplet in the Georgi--Machacek (GM) 
%%model~\cite{Georgi1985463,CHANOWITZ1985105} also serves as a reference model in the VBF production mode.   
%%The parameterisation of heavy vector triplet (HVT) Lagrangians~\cite{deBlas:2012qp,Pappadopulo2014} permits the interpretation of searches for spin-1 resonances in a generic way. 
%%The bulk Randall--Sundrum (RS) model~\cite{Randall:1999ee,Han:1998sg} features a spin-2 Kaluza--Klein (KK) graviton excitation ($G_\text{KK}$) decaying into $WW$, while a tensor resonance signal in the VBF production mode is based on an effective Lagrangian model (ELM)~\cite{Frank:2012wh}.
%%
%%A previous search for a heavy Higgs boson in the $e\nu\mu\nu$ final state was performed by ATLAS~\cite{Aad:2015agg} based on a data sample with an integrated luminosity of 20.3\,fb$^{-1}$ at $\sqrt{s}=8$\TeV. 
%% The CMS Collaboration also published a search for a high-mass scalar decaying into two $W$ bosons in the fully leptonic final state~\cite{Khachatryan:2015cwa},
%% %{cms:ww-paper-run1}, 
%% using datasets at $\sqrt{s}=7$ and 8\TeV with integrated luminosities of 5.1\,fb$^{-1}$ and 19.5\,fb$^{-1}$, respectively.  
%%A search for heavy resonances in the RS models in the leptonic decays of the $WW$ channel,  using a dataset of 4.7\,fb$^{-1}$ at 7\TeV~\cite{EXOT-2012-11}, was reported by the ATLAS Collaboration. The ATLAS and CMS collaborations have obtained constraints on the HVT and bulk RS models, based on other decay modes of the $VV$ channels, with $V$ being either a $W$ or a $Z$ boson~\cite{Khachatryan:2014hpa,Khachatryan:2014gha,Aad:2014pha,Khachatryan:2014xja,Aad:2015ufa,Aad:2015owa,EXOT-2014-18,EXOT-2016-01,Sirunyan:2016cao,Sirunyan:2017nrt,EXOT-2016-19,CMS-B2G-17-001,EXOT-2016-29}. The search in the $e\nu\mu\nu$ decay mode is complementary to searches performed in other decay modes. In particular, the sensitivity to low mass resonances is higher in the fully leptonic final state than in final states that include jets due to background from jet production.
%%
%%The article is organised as follows. Section~\ref{sec:model} presents the various models used in this analysis. Section~\ref{sec:detector} describes the ATLAS detector. The data 
%%and simulated event samples are discussed in Section~\ref{sec:samples}.  The event reconstruction and selection are described in Sections~\ref{sec:objects} and \ref{sec:selection}, respectively, followed by the background estimation techniques in Section~\ref{sec:backgrounds}. Systematic uncertainties are
%%discussed in Section~\ref{sec:systematics} and the results are presented in Section~\ref{sec:results}. Finally, the conclusions are given in Section~\ref{sec:conclusion}.


The Higgs boson, 
as an elementary particle predicted by the Standard Model (SM) in the early 1960s, 
was discovered in 2012 by the ATLAS~\cite{ATLASHiggsDiscovery} and CMS~\cite{CMSHiggsDiscovery} Collaborations at the Large Hadron Collider (LHC) for the first time in the history of particle physics. 
Therefore the Higgs field, which was thought to give a particle mass and produce the Higgs boson through spontaneous symmetry breaking, was confirmed to be existed.
The measured properties~\cite{atlas:combined-paper-run1,HIGG-2013-17,cms:comb-paper-run1,HIGG-2015-07} of the Higgs boson are within experimental uncertainties, consistent with the SM predictions. 
Nevertheless, the SM is thought to be an incomplete theory and many scenarios beyond the SM (BSM) predict an extended Higgs sector~\cite{deFlorian:2016spz,Ivanov:2017dad}. 
Diboson vector and tensor resonances are also predicted in several other extensions to the SM, such as in composite Higgs models~\cite{Agashe:2004rs,Giudice:2007fh} and warped extra dimensions models~\cite{Randall:1999ee,Agashe:2007zd,Antipin:2007pi,Antipin:2008hj}.
In this thesis, a search for a neutral heavy Higgs boson or other heavy resonances is presented, motivated by the statement mentioned above.
The search uses the data collected by the ATLAS detector at the LHC from the $pp$ collisions at the center-of-mass energy $\sqrt{s}=13\TeV$, corresponding to an integrated luminosity of approximately 36.1\,fb$^{-1}$.

%A previous search for a heavy Higgs boson in the $H\rightarrow WW\rightarrow \ell\nu\ell\nu$ ($\ell=e,\mu$, but only $e\nu\mu\nu$ final state was used) decay channel was performed by the ATLAS~\cite{Aad:2015agg} experiment based on a data sample with an integrated luminosity of 20.3\,fb$^{-1}$ at $\sqrt{s}=8$\TeV. 
%No excess of events beyond the SM background prediction was found. Upper limits were set on the product of the production cross section and the $H\to WW$ branching ratio in three different scenarios: a high-mass Higgs boson with a complex-pole scheme (CPS) lineshape and the width predicted for a SM Higgs boson, one with a narrow width approximation (NWA), and one with an intermediate width.
%The search was also performed in the $H\to WW\to \ell\nu qq$ decay channel in the same study~\cite{Aad:2015agg}. A combination of the $\ell\nu\ell\nu$ and $\ell\nu qq$ final states gave stronger upper limits. 
%The CMS experiment also published a search for a high-mass scalar decaying into two $W$ bosons in the fully leptonic final state~\cite{Khachatryan:2015cwa},
%using datasets at $\sqrt{s}=7$ and 8\TeV of 5.1\,fb$^{-1}$ and 19.5\,fb$^{-1}$ of integrated luminosity, respectively.
%The search was performed in the Higgs boson mass range 110--600\GeV. Except for the SM Higgs boson with $m_{H}=125.6\GeV$ which was considered as a background process, addtional Higgs bosons with SM-like properties are excluded in the mass range 114--600\GeV at the $95\%$ confidence level.
%A search for heavy resonances in the (bulk) Randall-Sundrum (RS) models in the fully leptonic decays of the $WW$ channel, using a dataset of 4.7\,fb$^{-1}$ at 7\TeV~\cite{EXOT-2012-11}, was reported by the ATLAS experiment. 
%No significant excess of events was observed and upper limits on the production cross-section times branching ratio were set for two benchmark models: a spin-2 RS graviton $G^{\ast}$ and the bulk RS graviton $G_{\rm bulk}^{\ast}$. 
%The observed $95\%$ confidence level (CL) lower limits on the masses of the two models are: 1.23\TeV for $G^{\ast}$ and 0.84\TeV for $G_{\rm bulk}^{\ast}$, assuming the coupling strength $k/\bar{M}_{\rm Pl}=0.1$ and $k/\bar{M}_{\rm Pl}=1.0$, respectively, 
%where $k$ is the curvature of the warped fifth dimension and $\bar{M}_{\rm Pl}=\frac{M_{\rm Pl}}{\sqrt{8\pi}}$ is the reduced Planck mass.
%The ATLAS and CMS experiments have obtained limits in the heavy vector triplet (HVT), the bulk RS and some other exotic models, based on other decay modes of the $VV$ channels, $V$ being either a $W$ or a $Z$ boson~\cite{Khachatryan:2014hpa,Khachatryan:2014gha,Aad:2014pha,Khachatryan:2014xja,Aad:2015ufa,Aad:2015owa,EXOT-2014-18,EXOT-2016-01,Sirunyan:2016cao,Sirunyan:2017nrt}. 

The results are interpreted in terms of different benchmark models. For the case of a scalar resonance
produced by gluon--gluon fusion (ggF) or vector-boson fusion (VBF), two scenarios with different intrinsic widths are considered. Constraints on the heavy neutral scalar in two-Higgs-doublet models (2HDM) are also obtained. The neutral member of the fiveplet in the Georgi--Machacek (GM) 
model~\cite{Georgi1985463,CHANOWITZ1985105} also serves as a reference model in the VBF production mode.   
The parameterisation of heavy vector triplet (HVT) Lagrangians~\cite{deBlas:2012qp,Pappadopulo2014} permits the interpretation of searches for spin-1 resonances in a generic way. 
The bulk Randall--Sundrum (RS) model~\cite{Randall:1999ee,Han:1998sg} features a spin-2 Kaluza--Klein (KK) graviton excitation ($G_\text{KK}$) decaying into $WW$, while a tensor resonance signal in the VBF production mode is based on an effective Lagrangian model (ELM)~\cite{Frank:2012wh}.

A previous search for a heavy Higgs boson in the $e\nu\mu\nu$ final state was performed by ATLAS~\cite{Aad:2015agg} based on a data sample with an integrated luminosity of 20.3\,fb$^{-1}$ at $\sqrt{s}=8$\TeV. 
 The CMS Collaboration also published a search for a high-mass scalar decaying into two $W$ bosons in the fully leptonic final state~\cite{Khachatryan:2015cwa},
 %{cms:ww-paper-run1}, 
 using datasets at $\sqrt{s}=7$ and 8\TeV with integrated luminosities of 5.1\,fb$^{-1}$ and 19.5\,fb$^{-1}$, respectively.  
A search for heavy resonances in the RS models in the leptonic decays of the $WW$ channel,  using a dataset of 4.7\,fb$^{-1}$ at 7\TeV~\cite{EXOT-2012-11}, was reported by the ATLAS Collaboration. The ATLAS and CMS collaborations have obtained constraints on the HVT and bulk RS models, based on other decay modes of the $VV$ channels, with $V$ being either a $W$ or a $Z$ boson~\cite{Khachatryan:2014hpa,Khachatryan:2014gha,Aad:2014pha,Khachatryan:2014xja,Aad:2015ufa,Aad:2015owa,EXOT-2014-18,EXOT-2016-01,Sirunyan:2016cao,Sirunyan:2017nrt,EXOT-2016-19,CMS-B2G-17-001,EXOT-2016-29}. The search in the $e\nu\mu\nu$ decay mode is complementary to searches performed in other decay modes. In particular, the sensitivity to low mass resonances is higher in the fully leptonic final state than in final states that include jets due to background from jet production.

For the sake of a complete description of the analysis and results that are presented in this thesis, not only my own work but also the work from everyone in the working group are discussed.
My personal contributions to the analysis are mainly listed as follows:
\begin{enumerate}
\item Optimisation of event selection in signal region (SR).
\item Estimation of experimental systematic uncertainty and theoretical systematic uncertainty on top and $WW$ backgrounds.
\item Analysis of data, which includes background estimation and making cutflows, comparison between data and Monte Carlo (MC) prediction.
\item Production of input for the statistical treatment.
\item Optimisation of the binning of transverse mass distributions for the statistical analysis.
\end{enumerate}

The main body of the thesis is organised as follows.
Section~\ref{sec:model} gives an introduction to the signal models that are used in the analysis to interpret the results.
Section~\ref{sec:detector} presents briefly the LHC and the ATLAS detector. %, as well as the event reconstruction.
Section~\ref{sec:samples} describes the data and MC samples that are used in the analysis.
Section~\ref{sec:objects} introduces the event reconstruction and the definitions of objects and observables.
Section~\ref{sec:selection} presents the event selection in the SRs. %, including the optimisation strategy.
Section~\ref{sec:backgrounds} presents the backgrounds estimation.
Section~\ref{sec:systematics} discusses the systematic uncertainties.
Section~\ref{sec:results} presents the results.
Section~\ref{sec:conclusion} provides a conclusion and an outlook of the analysis.
\FloatBarrier
