%-------------------------------------------------------------------------------
\section{Data and simulation samples}
\label{sec:samples}
%-------------------------------------------------------------------------------

The data used in this analysis were collected with a single-electron or single-muon trigger.
These triggers have a transverse energy or momentum threshold, $\ET$ or $\pT$, that depends on the data-taking period, with the lowest threshold varying between 20\GeV and 26\GeV.
The trigger efficiency for $WW$ events passing the offline event selection (Section~\ref{sec:selection}) is greater than 99\%.
Data quality criteria are applied to ensure that events are recorded with stable 
beam conditions and with all relevant subdetector systems operational.

Samples of simulated signal and background events are used to
optimise the event selection and to estimate the signal acceptance and
the background yields from various SM processes.

The sample for the NWA heavy Higgs boson signal was produced with 
\textsc{Powheg-Box}\,2.0~\cite{Nason:2004rx,Frixione:2007vw,Alioli:2010xd} which calculates separately the ggF~\cite{Alioli:2008tz} and VBF~\cite{Nason:2009ai} production mechanisms with matrix elements up to next-to-leading order (NLO) in quantum chromodynamics (QCD).  It uses the CT10 NLO parton distribution function (PDF) set~\cite{Lai:2010vv} and is interfaced with 
\textsc{Pythia\,8.186}~\cite{Sjostrand:2007gs} for the $H\to WW$ decays, for parton showering and hadronisation. A set of tuned parameters called the AZNLO tune~\cite{Aad:2014xaa} is used to describe the underlying event.
The NWA Higgs boson is generated with a width of 4\MeV. This event sample is also used to constrain the 2HDM.
The LWA heavy Higgs boson signal was simulated at NLO using the 
\textsc{MadGraph5\_aMC@NLO}\,2.3.2 event generator~\cite{Alwall:2014hca}
with the NNPDF23LO PDF set~\cite{Carrazza:2013axa}.  The generated 
particles at matrix element level are showered by \textsc{Pythia}\,8.186 with the \textsc{A14} tune~\cite{ATL-PHYS-PUB-2014-021} for the underlying event.
The mass of the heavy Higgs boson signals considered in this analysis spans  
the range between 200\GeV and 4 (3)\TeV for the ggF-induced (VBF-induced) signals. Both NWA and LWA samples were generated in steps of 100\,GeV up to 1\TeV, and in steps of 200\GeV thereafter. 

The \textsc{Powheg-Box} samples describe the production of a ggF-induced heavy Higgs boson in association with one jet at leading-order (LO) precision, while further jets are emulated by the  parton shower generator, \textsc{Pythia}. A more precise calculation of higher jet multiplicities is provided by using \textsc{MadGraph5\_aMC@NLO}\,2.3.2 to simulate $gg\rightarrow H$ events in association with up to two jets at NLO precision. Here, the overlap between identical final states generated at the matrix element (ME) and the parton shower (PS) stage is removed using FxFx merging~\cite{Frederix:2012ps}. The fraction of ggF events passing the event selection requirements of the $N_\text{jet}=1$ and $N_\text{jet}\geq 2$ VBF categories (defined later in Section~\ref{sec:selection}) predicted by the \textsc{Powheg-Box} event generator is reweighted to match that of the \textsc{MadGraph5\_aMC@NLO} FxFx samples. The corresponding scale factors are calculated for several hypothetical heavy Higgs boson masses. It is the largest, 1.14, for the 200\GeV mass point, and decreases with increasing resonance mass to a value of 0.85 for the 4\TeV mass point, for the $N_\text{jet}=1$ VBF category. The corresponding numbers are 0.91 and 0.73 for the $N_\text{jet} \geq 2$ VBF category.

Benchmark samples for the GM, HVT and bulk RS models were generated at LO using \textsc{MadGraph5\_aMC@NLO} 
interfaced to \textsc{Pythia}\,8.186 with the NNPDF23LO PDF set. 
A value of $\sin\theta_H = 0.4$ is chosen for the GM benchmark model.
For the HVT interpretation in the $q\bar{q}$ annihilation mode, samples were generated according to the extended gauge symmetry model $A$~\cite{Pappadopulo2014} with $g_V=1$. In the VBF mode, samples were generated using the same $g_V$ value but setting the couplings to the fermions to zero so that the new vector boson couples only to the SM vector and Higgs bosons.
For the bulk RS model, a curvature scale parameter $k/\bar{M}_\text{Pl}$ of either 0.5 or 1 is considered.
The ELM VBF spin-2 signals were generated at LO with VBFNLO\,3.0.0 beta 2~\cite{Baglio:2014uba} with the NNPDF30LO PDF 
set~\cite{NNPDF30} and using the following parameter setting~\cite{Frank:2012wh}: $\Lambda_{f\!f}=3$\TeV, $n_{f\!f}=4$, 
$\Lambda=1.5$\TeV and $f_1=f_2=f_5=1$. 
The mass range considered is between 200\GeV and 5\TeV for the KK graviton signal, between 250\GeV and 5\TeV for the HVT qqA signal, between 200\GeV and 1\TeV for the GM and ELM VBF signals, and between 300\GeV and 1\TeV for the HVT VBF signal.
 
The main sources of SM background include events from the production of single top quarks, $t\bar{t}$,  
dibosons ($WW$, $WZ$ and $ZZ$), $Z/\gamma^\ast+$jets 
and $W+$jets.
Single-top-quark simulated events were generated with 
\textsc{Powheg-Box}\,2.0~\cite{Alioli:2009je,Re:2010bp} using the CT10 NLO PDF set interfaced 
to \textsc{Pythia}\,6.428~\cite{Sjostrand:2006za} for parton showering and hadronisation, with the \textsc{Perugia}2012 tune~\cite{Skands:2010ak} and CTEQ6L1 PDF~\cite{Pumplin:2002vw} to describe the underlying event. 
The $t\bar{t}$ events were generated with \textsc{Powheg-Box}\,2.0~\cite{Alioli:2011as} using the NNPDF30NLO PDF set~\cite{NNPDF30} 
interfaced to \textsc{Pythia}\,8.186 for parton showering and hadronisation, 
with the \textsc{A14} tune and CTEQ6L1 PDF to describe the underlying 
event. The sample was generated by setting the resummation damping parameter $h_\text{damp}$ to 1.5 times the top-quark mass, $m_\text{top}$, which was set to 172.5 GeV. The $h_\text{damp}$ parameter controls the ME/PS matching and effectively regulates the high-$\pT$ radiation.
The \textsc{EvtGen}\,1.2.0~\cite{Lange:2001uf} package was used to model the properties of 
the bottom and charm hadron decays.  
Diboson samples were generated with \textsc{Sherpa}\,2.1.1~\cite{Gleisberg:2008ta,Gleisberg:2008fv,Cascioli:2011va,Schumann:2007mg,Hoeche:2012yf} for the $gg$ production processes and \textsc{Sherpa}\,2.2.1 for the $q\bar{q}$ production processes, using the CT10 NLO and
NNPDF30NNLO PDF sets, respectively.  The \textsc{Sherpa} event generator for the latter processes produces up to one additional parton at NLO and up to 
three additional partons at LO.
Production of $W$ and $Z$ bosons in association with jets was also 
simulated using \textsc{Sherpa}\,2.1.1 with the CT10 NLO PDF set, where $b$- and 
$c$-quarks are treated as massive particles. 
The $gg\to WW$ production also includes the contribution of the SM Higgs boson at 125\GeV and the 
interference effects between the continuum and Higgs resonance processes. The VBF part of SM Higgs boson production was generated with
\textsc{Powheg-Box}~\cite{Nason:2009ai} interfaced to \textsc{Pythia}\,8.186 for parton showering and hadronisation.

The effect of multiple $pp$ interactions in the same and neighbouring bunch
crossings (pile-up) was included 
by overlaying minimum-bias collisions, simulated 
with \textsc{Pythia}\,8.186, on each generated signal and background event.  
The number of overlaid collisions is such that the distribution of the average 
number of interactions per $pp$ bunch crossing in the simulation matches the pile-up conditions 
observed in the data, which is about 25 interactions per bunch crossing on average. 
The generated samples were processed through a \textsc{Geant4}-based detector 
simulation~\cite{Agostinelli:2002hh,Aad:2010ah}, followed by the standard ATLAS
reconstruction software used for collision data.


