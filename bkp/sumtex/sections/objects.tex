%-------------------------------------------------------------------------------
\section{Event reconstruction}
\label{sec:objects}
%-------------------------------------------------------------------------------

Events used in this analysis are required to have at least one primary vertex with a minimum of 
two associated tracks, each with transverse momentum $\pT > 400$\MeV. 
If there is more than one vertex reconstructed in an event that meets these conditions, the one with
the highest sum of track $\pT^2$ is chosen as the primary
vertex.
% It is subsequently used for the calculation of properties of the physics objects in this analysis.

Electrons are reconstructed from clusters of energy deposits in the
electromagnetic calorimeter that match a track reconstructed in the ID. They are
identified using the likelihood identification criteria described in
Ref.~\cite{ATLAS:2016iqc}. 
The electrons used in this analysis are required to pass the ``MediumLH'' selection for $\pT>25$\GeV~\footnote{For electrons, $\pT$ is defined as the magnitude of the transverse component of the electron momentum as measured using the electromagnetic calorimeter.} or the ``TightLH'' selection for $\pT<25$\GeV 
and be within $|\eta|<2.47$, excluding the transition region between the barrel and
endcaps in the LAr calorimeter ($1.37 < |\eta| < 1.52$). These ``MediumLH'' and ``TightLH'' selection categories have identification efficiencies of  $84\%$ and $74\%$, respectively, for electrons with $\pT$ of 25\GeV.
The corresponding probabilities to misidentify hadrons as electrons are approximately $0.5\%$ and $0.3\%$, respectively. 

Muons are
reconstructed by combining ID and MS tracks that have consistent
trajectories and curvatures~\cite{Aad:2014rra}. 
The muon candidates used in this analysis
are required to have $|\eta|<2.5$ and pass the ``Medium'' selection for $\pT>25$\GeV or the ``Tight'' selection for $\pT<25$\GeV, defined on the basis of the quality of the  reconstruction and identification. These selections have a reconstruction efficiency of approximately $96\%$ and $92\%$, respectively, for muons originating from the decay of $W$ bosons~\cite{Aad:2016jkr}. The corresponding probabilities to misidentify hadrons as muons are approximately $0.2\%$ and $0.1\%$, respectively. 

To ensure that leptons originate from the
interaction point, a requirement of $|d_0|/\sigma_{d_0}<5\,(3)$ is imposed on the electrons (muons) and
$|z_0 \sin\theta|<0.5$\,mm is applied to both lepton types. Here
$d_0$ and $z_0$ are the transverse and longitudinal impact parameters of the
lepton with respect to the primary vertex, respectively, and
$\sigma_{d_0}$ is the uncertainty in the measured value of $d_0$.
In addition, electrons and muons are required to be isolated from other tracks
and calorimetric activities by applying $\pt$- and $\eta$-dependent isolation 
criteria.
For muons, the calorimeter isolation is based on energy deposits in the calorimeter within a cone $\Delta R$ of 0.2 around the muons.
The muon track isolation uses a variable cone size starting at $\Delta R=0.3$ and 
shrinking with increasing $\pT$ of the muon~\cite{isolation}.
The same calorimeter isolation is used for electrons, and the electron track isolation uses a variable cone size starting at $\Delta R = 0.2$.
The efficiency of these isolation requirements is 90\% for both lepton types  
with $\pT$ of 25\GeV, increasing to 99\% at 60\GeV.

Jets are reconstructed from three-dimensional
clusters of energy deposits in the calorimeters using the anti-$k_t$
algorithm~\cite{Cacciari:2008gp} with a radius parameter of
$R=0.4$ implemented in the FastJet package~\cite{Cacciari:2011ma}. The four-momenta of the jets are calculated as the sum of the four-momenta of their
constituents, which are assumed to be massless.
Jets are corrected for energy from pile-up using the pile-up subtraction based on jet areas~\cite{Cacciari:2007fd}.
The jet energy scale is estimated in Ref.~\cite{Aaboud:2017jcu}.
Jets are required to have  $\pT>30\GeV$ and $|\eta| < 4.5$.  

For jets with $\pT<60\GeV$ and |$\eta$| $<$ 2.5, the multivariate ``jet vertex tagger'' 
algorithm~\cite{JVT} is used to suppress jets from pile-up
interactions. To avoid double counting, 
jets of any transverse momentum are discarded if they are within a cone of size
 $\Delta R=0.2$ around an electron
candidate or if they have fewer than three associated tracks and are within a cone of size
 $\Delta R=0.2$ around a muon candidate. 
However, if a jet with three or more associated tracks is within a cone of size $\Delta R<0.4$ of a muon candidate, or the separation between an electron and any jet is within $0.2<\Delta R < 0.4$, the corresponding muon or electron candidate is rejected.

To estimate the number of $b$-tags in the event, jets with $\pT>20$\GeV and within $|\eta| < 2.5$ are considered to contain a $b$-hadron if they yield a $b$-tagging algorithm discriminant value exceeding a reference value. The MV2c10 algorithm~\cite{Aad:2015ydr,ATL-PHYS-PUB-2016-012} is chosen at the 85\% $b$-tagging efficiency benchmark point, estimated from $b$-jets in simulated $t\bar{t}$ events.
The misidentification rate for jets which originate
from a light quark or gluon is less than 1\%, while it is approximately 17\% for $c$-jets.

The missing transverse momentum, with magnitude $\met$, is calculated as the 
negative vectorial sum of the transverse momenta of 
calibrated electrons, muons, and jets originating from the primary vertex, as well as tracks 
with $\pT > 500$\MeV compatible with the primary vertex and not associated with any of these~\cite{met}.
