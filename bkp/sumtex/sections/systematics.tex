%-------------------------------------------------------------------------------
\section{Systematic uncertainties}
\label{sec:systematics}
%-------------------------------------------------------------------------------

In this section, experimental and  theoretical uncertainties in the normalisation and shape of the  $\mT$ distributions of the background and the signal are described.  Except for those explicitly mentioned here, the shape uncertainties are small and thus neglected. Overall, the systematic uncertainty dominates, except  in the tails of the $\mT$ distributions where the statistical uncertainty is larger.

\subsection{Experimental uncertainties}
\label{sec:wjetssys}

The dominant sources of experimental uncertainty in the signal and 
background yields are the jet energy scale and resolution (Jet)~\cite{Aaboud:2017jcu}, the $b$-tagging 
efficiency ($b$-tag)~\cite{Aad:2015ydr}, and the pile-up modelling~\cite{JVT}. 
Other systematic uncertainties such as those associated with trigger efficiencies, 
lepton reconstruction and identification efficiencies, lepton momentum scales and 
resolutions~\cite{ATLAS:2016iqc,Aad:2016jkr}, 
missing transverse momentum reconstruction~\cite{met} and the jet vertex tagger~\cite{JVT} are also considered when evaluating systematic
effects on the shape and normalisation of the background, or the shape and efficiency of the signal yield.
The uncertainty associated with the pile-up modelling is assessed by performing a variation of $\pm 9\%$ in the number of simulated pile-up interactions to cover the uncertainty in the ratio of the predicted and measured cross sections of non-diffractive inelastic events producing a hadronic system of mass $m_{X,\text{had}}>13$\GeV~\cite{STDM-2015-05}.

For the main background from top-quark and $WW$ processes, the impact of the most important experimental systematic uncertainties is summarised in Tables \ref{tab:top_syst_lvlv} and \ref{tab:ww_syst_lvlv} together with dominant theoretical uncertainties. 
The maximum changes in yield for the up and down variations are shown in the various signal and control regions. 
The correlation between the SRs and CRs is taken into account in the simultaneous fit.

Systematic effects due to lepton identification efficiencies, momentum and scale resolutions, are found to be approximately 1\%. They are not shown in the tables. The last column in the tables shows the total uncertainty, including these small uncertainty sources. 

The data-driven $W$+jets background estimate
is subject to several sources of systematic uncertainty.  
The subtraction of the subdominant electroweak processes (Section~\ref{sec:wjets_lvlv}) has a significant impact on the extrapolation factor calculation at high lepton $\pT$. 
The subtraction is varied, as  described in Ref.~\cite{ATLAS:2014aga}, and the 
variation of the event yield in the signal region is taken as the uncertainty.  The method assumes that the extrapolation factors of the dijet and $W$+jets samples are equal.   
Differences in the jet flavour composition between dijet and $W$+jets events introduce an additional 
systematic uncertainty. This is evaluated as the sum in quadrature of two contributions: differences between the extrapolation factors 
calculated with dijet samples and $Z$+jets samples in data, and differences between the extrapolation factors evaluated with 
$W$+jets and $Z$+jets MC samples.
Finally, the statistical uncertainties of the different data and MC samples used to evaluate the extrapolation factors are taken as an additional source of systematic uncertainty.  The overall relative systematic uncertainty of the $W$+jets background is found to be approximately 35\% for each of the three signal event categories, with the dominant uncertainty being associated with the jet flavour composition.

The uncertainty in the total 2015 and 2016 integrated luminosity is 2.1\%. It is derived, following a methodology similar to that detailed in 
Ref.~\cite{lumi2}, from van der Meer scans performed in August 2015 and May 2016, calibrated at high luminosity by various luminosity detectors.


\subsection{Theoretical uncertainties of the background}\label{sec:errth_bkg}

For background sources which are normalised using control regions, theoretical uncertainties are evaluated for the extrapolation from the control region to the signal region.

For the top-quark and $WW$ background, theoretical uncertainties in the
extrapolation are evaluated according to the prescription from the LHC Higgs Cross Section Working Group~\cite{LHCHiggsCrossSectionWorkingGroup:2012vm}. The 
uncertainties include the impact of missing higher-order corrections, PDF variations and other MC modelling.  The dominant theoretical uncertainties are shown in Tables~\ref{tab:top_syst_lvlv} and \ref{tab:ww_syst_lvlv}. 

For the top-quark background, the uncertainty from the event generator and parton shower modelling (ME+PS) is estimated by  comparing the nominal  \textsc{Powheg-Box+Pyhtia8} generated samples with those from an alternative event generator, \textsc{Sherpa}\,2.2.1. The uncertainty named ``Scale'' corresponds to variations of the renormalisation $\mu_\text{R}$ and factorisation $\mu_\text{F}$ scales as well as $h_\text{damp}$. The variations for $\mu_\text{R}$ and $\mu_\text{F}$ are between 0.5 and 2 from their nominal scale of $\sqrt{m^2_\text{top}+\pT^2}$, with $\pT$ being the top-quark transverse momentum. The parameter $h_\text{damp}$ is varied between $m_\text{top}$ and $2\cdot m_\text{top}$ from its nominal scale $h_\text{damp}=1.5\cdot m_\text{top}$. 
In the analysis the single-top-quark and $t\bar{t}$ processes are studied together. An uncertainty of 20\%~\cite{TOPQ-2012-20,TOPQ-2015-16} is assigned to the relative contribution of the single-top-quark processes, corresponding to the source ``Single top'' in Table~\ref{tab:top_syst_lvlv}. 
The PDF uncertainty is obtained by taking the envelope of the uncertainty of the NNPDF30NLO PDF set and its differences in central value with the CT14~\cite{ct14} and MMHT 2014~\cite{mmht2014} PDF sets, following the recommendations of Ref.~\cite{Lai:2010vv}. The PDF uncertainties are $\mT$ dependent and increase from 2\% to 10\% with $\mT$. This $\mT$ dependence is taken into account in the signal regions.
In the ggF quasi-inclusive category, two additional shape systematic uncertainties associated with the scale variations and the $\pT$ reweighting for the leading lepton in the top-quark background are applied, the latter corresponding to $\pm 50\%$ of the reweighting correction. These two uncertainties are comparable and vary from a few percent at low $\mT$ to about 10\% at $\mT\simeq 1$\TeV,
without affecting the integrated event yield of the top-quark background in the category.

For the $WW$ background, 
the ME+PS modelling uncertainty is obtained by comparing the nominal  
\textsc{Sherpa 2.2.1} sample with an alternative sample generated with \textsc{Powheg-Box+Pythia8}. 
The renormalisation, factorisation, and resummation scales are varied separately by factors of 0.5 and 2. The uncertainty corresponding 
to the factorisation scale variation is smaller than the other uncertainties and is not shown. 
The PDF uncertainty for the $WW$ background is obtained and treated in the same way as for the 
top-quark background. In the ggF quasi-inclusive category, an additional shape uncertainty from ME+PS 
is applied. It varies from a few percent at low $\mT$ to about 20\% at $\mT\simeq 1$\TeV. There are no significant shape uncertainties in the $\mT$ distributions in the VBF categories. 

\begin{table}[tbp]
\caption{Relative impact (in \%) of dominant experimental and theoretical uncertainties in the event yields for the top-quark background processes in the three signal regions (SR$_\text{ggF}$, SR$_\text{VBF1J}$ and SR$_\text{VBF2J}$) and the top-quark and $WW$ control regions (Top CR$_\text{ggF/VBF}$ and the $WW$ CR$_\text{ggF/VBF1J}$).
Jet and $b$-tag sources dominate the experimental uncertainty while ME+PS, Scale, Single top and PDF are the dominant theoretical uncertainties. The last column shows the total uncertainty including those not listed here.}
\label{tab:top_syst_lvlv}
\center
\begin{tabular}{|l|cccccc|c|}
\hline
%Source & Jet & $b$-tag & ME+PS & Scale & Single top & PDF & Total \\\hline
%SR$_\text{ggF}$ & 5.2 & 17 & 1.3 & 3.0 & 4.2 & 2.9 & 19\\ 
%$\text{SR}_\text{VBF1J}$  & 9.6 & 7.8 & 1.0 & 1.6 & 5.9 & 3.5 & 15 \\
%$\text{SR}_\text{VBF2J}$  & 9.7 & 14 & 9.5 & 5.0 & 2.1 & 3.6 & 21 \\\hline
%Top CR$_\text{ggF}$ & 2.2 & 4.8 & 0.34 & 0.21 & 2.6 & 4.3 & 7.3 \\
%$WW$ CR$_\text{ggF}$ & 5.3 & 18 & 1.1 & 6.3 & 4.0 & 3.8 & 20 \\
%$\text{Top CR}_\text{VBF}$  & 8.2 & 3.5 & 10 & 1.5 & 1.3 & 3.6 & 14 \\
%${\text{$WW$ CR}}_\text{VBF1J}$  & 9.9 & 8.3 & 9.4 & 3.9 & 5.3 & 3.4 & 18 \\\hline
Source & Jet & $b$-tag & ME+PS & Scale & Single top & PDF & Total \\\hline
SR$_\text{ggF}$ & 5.2 & 17 & 1.3 & 3.0 & 4.2 & 2.5 & 19\\ 
$\text{SR}_\text{VBF1J}$  & 9.6 & 7.8 & 1.0 & 1.6 & 5.9 & 2.6 & 15 \\
$\text{SR}_\text{VBF2J}$  & 9.7 & 14 & 9.5 & 5.0 & 2.1 & 3.4 & 21 \\\hline
Top CR$_\text{ggF}$ & 2.2 & 4.8 & 0.34 & 0.21 & 2.6 & 3.0 & 6.6 \\
$WW$ CR$_\text{ggF}$ & 5.3 & 18 & 1.1 & 6.3 & 4.0 & 3.2 & 20 \\
$\text{Top CR}_\text{VBF}$  & 8.2 & 3.5 & 10 & 1.5 & 1.3 & 3.7 & 14 \\
${\text{$WW$ CR}}_\text{VBF1J}$  & 9.9 & 8.3 & 9.4 & 3.9 & 5.3 & 2.7 & 18 \\\hline
\end{tabular}
\end{table}

\begin{table}[tbp]
\caption{Relative impact (in \%) of dominant experimental and theoretical uncertainties in the event yields for the $WW$ background processes in the three signal regions  (SR$_\text{ggF}$, SR$_\text{VBF1J}$ and SR$_\text{VBF2J}$) and the $WW$ control regions ($WW$ CR$_\text{ggF/VBF1J}$).
Jet and Pile-up sources dominate the experimental uncertainty while ME+PS, $\mu_\text{R}$, Resummation and PDF are the dominant theoretical uncertainties. The last column shows the total uncertainty including those not listed here.}
\label{tab:ww_syst_lvlv}
\center
\begin{tabular}{|l|cccccc|c|}
\hline
%Source & Jet & Pile-up & ME+PS & $\mu_\text{R}$ & Resummation & PDF & Total \\\hline
%SR$_\text{ggF}$ & 1.2 & 1.8 & 2.4 & 1.7 & 3.1 & 1.3 & 5.0 \\ 
%$\text{SR}_\text{VBF1J}$  & 17 & 2.8 & 11 & 7.3 & 5.0 & 1.0 & 23 \\
%$\text{SR}_\text{VBF2J}$  & 18 &  3.1 & 38 & 18 & 1.4 & 1.3 & 47 \\\hline
%$WW$ CR$_\text{ggF}$ & 1.1 & 1.8 & 2.6 & 0.95 & 2.9 & 2.3 & 5.2 \\
%${\text{$WW$ CR}}_\text{VBF1J}$  & 16 & 4.5 & 12 & 11 & 2.3 & 1.6 & 23 \\\hline
Source & Jet & Pile-up & ME+PS & $\mu_\text{R}$ & Resummation & PDF & Total \\\hline
SR$_\text{ggF}$ & 1.2 & 1.8 & 2.4 & 1.7 & 3.1 & 2.7 & 5.5 \\ 
$\text{SR}_\text{VBF1J}$  & 17 & 2.8 & 11 & 7.3 & 5.0 & 2.3 & 23 \\
$\text{SR}_\text{VBF2J}$  & 18 &  3.1 & 38 & 18 & 1.4 & 2.1 & 47 \\\hline
$WW$ CR$_\text{ggF}$ & 1.1 & 1.8 & 2.6 & 0.95 & 2.9 & 3.6 & 5.9 \\
${\text{$WW$ CR}}_\text{VBF1J}$  & 16 & 4.5 & 12 & 11 & 2.3 & 2.8 & 23 \\\hline
\end{tabular}
\end{table}

In addition to the scale uncertainties described above, a relative uncertainty of $\pm 50$\% is assigned to the reweighting corrections of the $q\bar{q}\to WW$ \textsc{Sherpa} sample to the combined NNLO QCD and NLO EW predictions in the ggF SR and $WW$ CR.

The $gg\to (h^\ast)\to WW$ process, where the SM 125\GeV Higgs boson is off-shell, is modelled at leading order with the 
\textsc{Sherpa} event generator with a $K$-factor of 1.7 that is 
used to account for higher-order cross-section corrections 
with an uncertainty of 60\%, following the studies in Refs.~\cite{Melnikov:2015laa,Caola:2015rqy,Bonvini:2013jha,Li:2015jva}. 

Other small background processes, such as $WZ$, $ZZ$, $Z/\gamma^\ast$+jets and $WW$ in the $N_\text{jet}\geq 2$ VBF category, do not have their own control regions. They are normalised to the theoretical predictions. 
The uncertainties in their yields due to the uncertainties in the predictions are evaluated with the same prescription as described above.
The impact of these uncertainties is small (see Tables~\ref{tab:yieldWW_lvlv_ggFincl} and \ref{tab:yieldWW_lvlv_VBF1J2J} in Section~\ref{sec:results}).


\subsection{Theoretical uncertainties in the signal predictions}

Theoretical uncertainties in the signal acceptance include
effects due to the choice of QCD renormalisation and factorisation scales,
the PDF set as well as the underlying-event modelling, the parton shower model and the 
parton shower tune. These uncertainties are evaluated separately in each of the three 
event categories as a function of the resonance mass and independently for ggF- and 
VBF-induced resonances. 

The effect of missing higher-order corrections in QCD on the signal 
acceptance is estimated by varying the renormalisation and factorisation scales 
independently by factors of 0.5 and 2 from the nominal scale of $\sqrt{m_H^2+p^2_{\text{T}, H}}$, with $m_H$ and $p_{\text{T}, H}$ being the mass and the transverse momentum of the heavy Higgs boson, respectively. The acceptance values obtained with 
these modified MC samples are compared to the signal acceptance of the nominal sample. For resonances produced via ggF, these uncertainties are found 
to be negligible in the quasi-inclusive ggF and $N_\mathrm{jet}=1$ VBF categories,
while in the $N_\mathrm{jet}\geq 2$ VBF category they range between $2.5\%$ and 0.2\% for a resonance mass varying from 200\GeV to 4\TeV (unless stated otherwise, the following uncertainties are quoted for the same mass range). 
For resonances produced via vector-boson fusion, these uncertainties range from 
$0.9\%$ to $2.8\%$ in the quasi-inclusive ggF category, from $1.9\%$ to $3.6\%$ in the 
$N_\mathrm{jet}=1$ VBF category and from $1.0\%$ to $7.3\%$ in the $N_\mathrm{jet}\geq 2$ VBF category.

The PDF-induced uncertainties in the signal acceptance are determined in the same way as for the top-quark and $WW$ background processes.
For the ggF-induced (VBF-induced) signal, these uncertainties reach  
 $0.4\%$ ($1.7\%$), $1.5\%$ ($1.2\%$) and $1.6\%$ ($1.5\%$) for the quasi-inclusive ggF, 
$N_{\text{jet}}=1$ and $N_{\text{jet}}\geq 2$ VBF event categories, respectively.

The uncertainties corresponding to the parton shower tune and the underlying event are derived by 
moving independently, up or down, the \textsc{Pythia} internal parameters that are associated with
final-state radiation or the multiple parton interactions to study separately their 
influence on the signal acceptance of the various signal mass points. These uncertainties are
compared for each event category and mass point to the uncertainties from the choice of parton shower model, which 
are estimated by comparing the results obtained for the nominal parton shower generator
to those obtained using \textsc{Herwig++}~\cite{Bahr:2008pv,Bellm:2013hwb}.  
The tune uncertainties 
are found to be smaller than the shower uncertainties for all mass points. Thus only the 
latter uncertainties are considered in the final results. The corresponding uncertainties for 
ggF-induced signals increase from $1.3\%$ to $3.1\%$, from $13\%$ to 
$28\%$, and from $2.3\%$ to $15\%$ for increasing 
resonance masses in the quasi-inclusive ggF, $N_{\text{jet}}=1$ and $N_{\text{jet}}\geq 2$ VBF 
categories, respectively.
The uncertainties for VBF-induced signals increase from $4.3\%$ to $19\%$, from $5.1\%$ to $9.0\%$, and from $3.3\%$ to $8.0\%$ in the three categories.

In addition, uncertainties due to missing higher-order corrections in QCD are evaluated for 
ggF-induced processes for each event category, considering also event migration
effects between different event categories. This follows the method proposed by Stewart and Tackmann~\cite{Stewart:2011cf}.  
The corresponding uncertainties range from $3\%$ to $10\%$ for the quasi-inclusive ggF category
and from $4\%$ to $30\%$ ($30\%-60\%$) for the $N_{\text{jet}}=1$ ($N_{\text{jet}}\geq 2$) VBF event 
categories.
