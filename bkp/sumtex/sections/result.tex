%-------------------------------------------------------------------------------
\section{Results}
\label{sec:results}
%-------------------------------------------------------------------------------

The statistical method used to interpret the results of the search is described 
in Ref.~\cite{Aad:2012an}. A likelihood function ${\cal L}$ is defined 
as the product of Poisson probabilities associated with the number of events in bins of the $\mT$ distributions in the signal regions
and of the total yields in the control regions. Each source of systematic uncertainty is parameterised by 
a corresponding nuisance parameter $\theta$ constrained by a Gaussian function.

The $\mT$ distributions in the
signal regions are divided into 18 (8) bins for the ggF quasi-inclusive 
($N_{\text{jet}}=1$  and $\geq 2$ VBF) categories.
The bins are of variable size to reflect the increasing width of the $\mT$ distribution of the expected signal with increasing mass, while keeping the statistical precision of the background contributions in each bin sufficiently high. 

The numbers of events predicted and observed in the signal and control regions are shown for the quasi-inclusive ggF categories in Table~\ref{tab:yieldWW_lvlv_ggFincl} and for the VBF $N_{\text{jet}}=1$ and $\geq 2$ categories in Table~\ref{tab:yieldWW_lvlv_VBF1J2J}. 
These yields are obtained from a simultaneous fit to the data in all the SRs and the CRs. 
The fitted signal event yield is consistent with zero. 
The background compositions depend strongly on the event categories: the top-quark and $WW$ processes are comparable in SR$_\text{ggF}$ and SR$_\text{VBF1J}$ while the top-quark events dominate in SR$_\text{VBF2J}$. The large reduction of the total background uncertainty is due to strong anti-correlations between some of the uncertainty sources of the top-quark and $WW$ background.
The $\mT$ distributions in SR$_\text{ggF}$, SR$_\text{VBF1J}$ and SR$_\text{VBF2J}$ are shown in Figure~\ref{fig:SRMT}. As no excess over the background prediction is observed, upper limits at 95\% confidence level (CL) are set on the production cross section times the branching fraction, $\sigma_X\times B(X\to WW)$,  for signals in each benchmark model.

\begin{table}[btp]
        \caption{Event yields in the signal and control regions for the quasi-inclusive ggF category.
        The predicted background yields and uncertainties are calculated after the simultaneous fit to the
        data in all the SRs and the CRs including those from Table~\ref{tab:yieldWW_lvlv_VBF1J2J}. The statistical and systematic uncertainties are combined. The notation ``$VV$'' represents non-$WW$ diboson background.}
        \label{tab:yieldWW_lvlv_ggFincl}
        \center
\begin{tabular}{|l|r@{ $\pm$ }l|r@{ $\pm$ }l|r@{ $\pm$ }l|}
\hline
& \multicolumn{2}{c|}{SR$_\text{ggF}$}  &  \multicolumn{2}{c|}{Top CR$_\text{ggF}$} &  \multicolumn{2}{c|}{$WW$ CR$_\text{ggF}$}\\
\hline
$WW$&11\,500&800&820&120&3\,360&220 \\
Top quark&11\,800&600&52\,550&330&2\,610&180 \\
$Z$/$\gamma$*&1\,420&110&111&20&20.9&2.0 \\
$W$+jets&1\,180&320&710&190&280&70 \\
$VV$&866&34&101&12&250&11 \\
\hline
Background &26\,740&170&54\,290&250&6\,510&80 \\
\hline
Data&\multicolumn{2}{l|}{26\,739}&\multicolumn{2}{l|}{54\,295}&\multicolumn{2}{l|}{6\,515} \\
\hline
\end{tabular}
\end{table}    


\begin{table}[tbp]
        \caption{Event yields in the signal and control regions for the $N_{\text{jet}}=1$  and $\geq 2$ VBF categories.
        The predicted background yields and uncertainties are calculated after the same simultaneous fit to the data in all the event categories as in Table~\ref{tab:yieldWW_lvlv_ggFincl}. The statistical and systematic uncertainties are combined. The notation ``$VV$'' represents non-$WW$ diboson background.}
        \label{tab:yieldWW_lvlv_VBF1J2J}
        \begin{center}
\begin{tabular}{|l|r@{ $\pm$ }l|r@{ $\pm$ }l|r@{ $\pm$ }l|r@{ $\pm$ }l|}
\hline
& \multicolumn{2}{c|}{SR$_\text{VBF1J}$} & \multicolumn{2}{c|}{SR$_\text{VBF2J}$}  &  \multicolumn{2}{c|}{Top CR$_\text{VBF}$} &  \multicolumn{2}{c|}{$WW$ CR$_\text{VBF1J}$}\\
\hline
$WW$&390&50&120&26&61&11&\hspace{0.3cm}265&32 \\
Top quark&450&50&391&24&5\,650&90&\hspace{0.3cm}167&18 \\
$Z$/$\gamma$*&45&11&24&6&68&19&\hspace{0.3cm}74&12 \\
$W$+jets&52&13&8.9&2.5&91&24&\hspace{0.3cm}43&11 \\
$VV$&32&7&16.6&1.9&20&9&\hspace{0.3cm}38&4 \\
\hline
Background &972&29&563&22&5\,890&80&\hspace{0.3cm}596&22 \\
\hline
Data&\multicolumn{2}{l|}{978}&\multicolumn{2}{l|}{\hspace{0.1cm}560}&\multicolumn{2}{l|}{5\,889}&\multicolumn{2}{l|}{\hspace{0.3cm}594} \\
\hline
\end{tabular}
        \end{center}
        \end{table} 

\begin{figure}[htbp]
 \centering
 \includegraphics[width=0.49\textwidth]{fig/paper/emme-InclusiveSR-MTWide-log.pdf}
 \includegraphics[width=0.49\textwidth]{fig/paper/emme-VBFSR1J-MTWide-log.pdf}
 \includegraphics[width=0.49\textwidth]{fig/paper/emme-VBFSR2J-MTWide-log.pdf}
 \caption{Post-fit distributions of the transverse mass  $\mT$ in the SR$_\text{ggF}$ (top left), SR$_\text{VBF1J}$ (top right) and SR$_\text{VBF2J}$  (bottom) categories. In each plot, the last bin contains the overflow. The hatched band in the upper and lower panels shows the total uncertainty of the fit. The top-quark and $WW$ background event yields are scaled using the indicated normalisation factors obtained from the simultaneous fit to all signal and control regions. The heavy Higgs boson signal event yield is normalised to the expected limits on $\sigma_H\times B(H\to WW)$ and
is shown for masses of 700\GeV and 2\TeV in the NWA scenario.}
 \label{fig:SRMT}
\end{figure}

The 95\% CL upper limits are computed using the modified frequentist method known as CL$_\text{s}$~\cite{CLs_2002}, using the asymptotic approximation of the distribution of a test statistic~\cite{asymptotics},
$q_\mu$, a function of the signal strength $\mu$, defined as the ratio of the measured $\sigma_X\times B(X\to WW)$ to 
that of the prediction: 
\begin{equation}
q_\mu=-2\ln\left(\frac{{\cal L}(\mu; \hat{\boldsymbol{\theta}}_\mu)}{{\cal L}(\hat{\mu};\hat{\boldsymbol{\theta}})}\right)\,. \nonumber
\end{equation}
 The quantities $\hat{\mu}$ and 
$\hat{\boldsymbol{\theta}}$ are those values of $\mu$ and ${\boldsymbol \theta}$, respectively, that
unconditionally maximise ${\cal L}$. The numerator depends on the values 
$\hat{\boldsymbol \theta}_\mu$ that maximise ${\cal L}$ for a given value of $\mu$.

Limits are obtained separately for
ggF and VBF production for the NWA and LWA signal hypotheses.
To derive the expected limits
on the ggF (VBF) production modes, the VBF (ggF) production cross section is set to zero
so that the expected limits correspond to the background-only hypothesis.
To derive the observed limits on the ggF (VBF) production mode, the VBF (ggF)
production cross section is treated as a nuisance parameter in the fit and 
profiled,
in the same way as dealing with the normalisation factors of the different background processes.
This approach avoids making any assumption about the presence or 
absence of the signal in any of these production modes.

Figure~\ref{fig:lvlv-limit-ggf-vbf} shows the 95\% CL upper limits on
$\sigma_H\times B(H\to WW)$ as a function of $m_H$ for a Higgs boson in the NWA scenario in the mass range $200\GeV\leq m_H\leq 4 (3)$\TeV for the ggF (VBF) production. Values above 6.4\,pb (1.3\,pb) at $m_H=200$\GeV and  above 0.008\,pb (0.006\,pb) at 4 (3)\TeV are excluded at 95\% CL by the quasi-inclusive ggF (VBF) NWA analysis. The main systematic uncertainties affecting the limits 
are the $\pT$ correction for the leading lepton in the top-quark background, scale variations for the top-quark background, the parton shower modelling of the $WW$ MC generator, and the jet energy scale and resolution uncertainties.
Limits are consistent with those expected in the absence of a signal over the investigated mass range.
The fact that the  observed limits are more stringent than the expected ones for mass values beyond 2\TeV is explained by the deficit in data at the high $\mT$ tail in Figure~\ref{fig:SRMT}.
These limits are extracted using the asymptotic approximation and their accuracy is verified to be consistent within about 5\% at 800\GeV and better than 20\% at 2\TeV and beyond using pseudo-experiments.

\begin{figure}[tbp]
 \centering
\includegraphics[width=0.48\linewidth]{fig/paper/Limit_NWA_ggf.pdf}
\includegraphics[width=0.48\linewidth]{fig/paper/Limit_NWA_vbf.pdf}
 \caption{Upper limits at 95\% CL on the Higgs boson production cross section times branching fraction 
 $\sigma_H\times B(H\to WW)$ in the $e\nu\mu\nu$ channel, for ggF (left) and VBF (right) signals with narrow-width lineshape as a function of the signal mass. The inner and outer bands show the $\pm 1\sigma$ and $\pm 2\sigma$ ranges around the expected limit.} 
 \label{fig:lvlv-limit-ggf-vbf}
\end{figure}

The analysis can be extended to a more general case where the relative fraction of the ggF production cross section varies over the total ggF and VBF production cross section. The corresponding 95\% CL upper exclusion limits for a signal at 800\GeV are shown in Figure~\ref{fig:limit-ggffra}. The dependence of the limits on the ggF fraction for other masses is similar but becomes slightly stronger (weaker) for lower (higher) mass values. The limit values for a ggF fraction of 0 and 1 are comparable with the VBF and ggF limits shown in Figure~\ref{fig:lvlv-limit-ggf-vbf} at the same mass value. The VBF limits are tighter than the ggF limits since the VBF $N_\text{jet}\geq 2$ signal region has the smallest background contribution and thus is the most sensitive.
\begin{figure}[tbp]
 \centering
\includegraphics[width=0.6\linewidth]{fig/paper/Limit_ggffra_800.pdf}
 \caption{Upper limits at 95\% CL on the total ggF and VBF Higgs boson production cross section times branching fraction 
 $\sigma_H\times B(H\to WW)$ in the $e\nu\mu\nu$ channel, for a signal at 800\GeV as a function of the ggF cross section divided by the combined ggF and VBF production cross section. The inner and outer bands show the $\pm 1\sigma$ and $\pm 2\sigma$ ranges around the expected limit.} 
 \label{fig:limit-ggffra}
\end{figure}

The NWA exclusion limit shown above can be further translated to exclusion contours in the 2HDM for the phase space where the narrow-width approximation is valid. The 95\% CL exclusion contours for Type\,I and Type\,II in the plane of $\tan\beta$ and $\cos(\beta-\alpha)$ for three mass values of 200\GeV, 300\GeV and 500\GeV are shown in Figure~\ref{fig:limit-2hdm}. For a fixed value of $\cos(\beta-\alpha)=-0.1$, 95\% CL exclusion limits on $\tan\beta$ as a function of the heavy Higgs boson mass are shown in Figure~\ref{fig:limit-2hdm-mh}. The coupling of the heaviest CP-even Higgs boson to vector bosons is proportional to $\cos(\beta-\alpha)$ and in the decoupling limit $\cos(\beta-\alpha)\to 0$, the light CP-even Higgs boson is indistinguishable from a SM Higgs boson with the same mass. The range of $\cos(\beta-\alpha)$ and $\tan\beta$ explored is limited to the region where the assumption of a heavy narrow-width Higgs boson with negligible interference is valid. When calculating the limits at a given choice of $\cos(\beta-\alpha)$ and $\tan\beta$, the relative rate of ggF and VBF production in the fit is set to the prediction of the 2HDM for that parameter choice. The white regions in the exclusion plots indicate regions of parameter space which are not excluded by the present analysis. 
\begin{figure}[htbp]
 \centering
 \includegraphics[width=0.38\linewidth]{fig/paper/Limit_2HDM1_200.pdf}
 \includegraphics[width=0.38\linewidth]{fig/paper/Limit_2HDM2_200.pdf}
 \includegraphics[width=0.38\linewidth]{fig/paper/Limit_2HDM1_300.pdf}
 \includegraphics[width=0.38\linewidth]{fig/paper/Limit_2HDM2_300.pdf}
 \includegraphics[width=0.38\linewidth]{fig/paper/Limit_2HDM1_500.pdf}
 \includegraphics[width=0.38\linewidth]{fig/paper/Limit_2HDM2_500.pdf}
 \caption{Exclusion contours at 95\% CL in the plane of $\tan\beta$ and $\cos(\beta-\alpha)$ for Type\,I (left) and Type\,II (right) 2HDM signals with three mass values of 200\GeV (top), 300\GeV (middle) and 500\GeV (bottom). The inner and outer bands show the $\pm 1\sigma$
and $\pm 2\sigma$ ranges around the expected limit and the hatched regions are excluded.} 
 \label{fig:limit-2hdm}
\end{figure}
\begin{figure}[tbp]
 \centering
 \includegraphics[width=0.48\linewidth]{fig/paper/Limit_2HDM1_mh.pdf}
 \includegraphics[width=0.48\linewidth]{fig/paper/Limit_2HDM2_mh.pdf}
 \caption{Exclusion contours at 95\% CL in the plane of $\tan\beta$ and $m_H$ for Type\,I (left) and Type\,II (right) 2HDM signals with $\cos(\beta-\alpha)=-0.1$. The inner and outer bands show the $\pm 1\sigma$ and $\pm 2\sigma$ ranges around the expected limit and the hatched regions are excluded. The other heavy Higgs boson states $A$ and $H^\pm$ are assumed to have the same mass as $H$.} 
 \label{fig:limit-2hdm-mh}
\end{figure}

For the LWA scenario, the interference effects among the heavy boson, the light Higgs boson at 125\GeV and the SM $WW$ continuum background were studied and found to have negligible impact on the exclusion limits. The 95\% CL upper limits are shown in Figure~\ref{fig:limit-lwa}. The limits for signal widths of 5\%, 10\% and 15\% are comparable with those from the NWA scenario for the VBF signals while for the ggF signals, the limits weaken slightly at high masses as the width increases.
For the LWA 15\% case, the upper exclusion limit ranges between 5.2\,pb (1.3\,pb) at $m_H=200$\GeV and 0.02\,pb (0.006\,pb) at 4 (3)\TeV for the ggF (VBF) signals.
\begin{figure}[tbp]
 \centering
\includegraphics[width=0.48\linewidth]{fig/paper/Limit_LWA15_ggf.pdf}
\includegraphics[width=0.48\linewidth]{fig/paper/Limit_LWA15_vbf.pdf}
\includegraphics[width=0.48\linewidth]{fig/paper/Limit_LWAcomp_ggf.pdf}
\includegraphics[width=0.48\linewidth]{fig/paper/Limit_LWAcomp_vbf.pdf}
 \caption{Upper limits at 95\% CL on the Higgs boson production cross section times branching fraction 
 $\sigma_H\times B(H\to WW)$ in the $e\nu\mu\nu$ channel, for a signal with  
a width of 15\% of the mass (top) and the comparison of three different widths (bottom) for the ggF (left) and VBF (right) production. The inner and outer bands show the $\pm 1\sigma$
and $\pm 2\sigma$ ranges around the expected limit.} 
\label{fig:limit-lwa}
\end{figure}

Figure~\ref{fig:limit-gm} shows the limits on the resonance production cross section times branching fraction $\sigma_X\times B(X\to WW)$ and $\sin\theta_H$ for a scalar GM signal with  masses between 200\GeV and 1\TeV. At the observed limit, the width is narrower than the experimental resolution~\cite{Zaro:2015ika}. The current sensitivity is not sufficient to exclude the benchmark model with $\sin\theta_H=0.4$.
\begin{figure}[tbp]
 \centering
 \includegraphics[width=0.48\linewidth]{fig/paper/Limit_GM.pdf}
 \includegraphics[width=0.48\linewidth]{fig/paper/Limit_GM_sinth.pdf}
 \caption{Upper limits at 95\% CL on the resonance production cross section times branching fraction 
 $\sigma_X\times B(X\to WW)$ (left) and on $\sin\theta_H$ (right) in the $e\nu\mu\nu$ channel, for a GM  signal. The inner and outer bands show the $\pm 1\sigma$
and $\pm 2\sigma$ ranges around the expected limit. The full curves without dots correspond to the predicted theoretical cross section and the model parameter used in the benchmark model, respectively.} 
 \label{fig:limit-gm}
\end{figure}
  
Limits are derived in the mass range from 250\GeV to 5\TeV and from 300\GeV to 1\TeV for a qqA and VBF HVT signal, respectively, as shown in Figure~\ref{fig:limit-hvt}. For the qqA production, signals below about 1.3\TeV are excluded at 95\% CL. 
No limit can be set for the VBF production in the benchmark model that assumes a coupling strength to gauge bosons $g_V =1$ and a coupling to fermions $c_F =0$. The model has an intrinsic width much narrower than the detector resolution. 
\begin{figure}[tbp]
 \centering
 \includegraphics[width=0.48\linewidth]{fig/paper/Limit_HVT.pdf}
 \includegraphics[width=0.48\linewidth]{fig/paper/Limit_HVT_VBF.pdf}
 \caption{Upper limits at 95\% CL on the resonance production cross section times branching faction 
 $\sigma_X\times B(X\to WW)$ in the $e\nu\mu\nu$ channel, for HVT qqA (left) and VBF (right) signals. The inner and outer bands show the $\pm 1\sigma$
and $\pm 2\sigma$ ranges around the expected limit. The full curves without dots correspond to the predicted theoretical cross sections.} 
 \label{fig:limit-hvt}
\end{figure}

Figure~\ref{fig:limit-spin2} shows the limits on a $G_\text{KK} \to WW$ signal for two different couplings: $k/\bar{M}_\text{Pl}=1$ and $k/\bar{M}_\text{Pl}=0.5$, for masses between 200\GeV and 5\TeV, and for an ELM spin-2 VBF signal for masses between 200\GeV and 1\TeV. 
The observed limits exclude a KK graviton signal lighter than 1.1\TeV (750\GeV) with the higher (lower) coupling, while the current sensitivity is not sufficient to exclude the ELM spin-2 VBF signal. 
\begin{figure}[tbp]
 \centering
 \includegraphics[width=0.48\linewidth]{fig/paper/Limit_RS.pdf}
 \includegraphics[width=0.48\linewidth]{fig/paper/Limit_RS_c050.pdf}
 \includegraphics[width=0.48\linewidth]{fig/paper/Limit_Spin2.pdf}
 \caption{Upper limits at 95\% CL on the resonance production cross section times branching fraction 
 $\sigma_X\times B(X\to WW)$ in the $e\nu\mu\nu$ channel, for a graviton signal with two different couplings of $k/\bar{M}_\text{Pl}=1$ (left) and $k/\bar{M}_\text{Pl}=0.5$ (right), and for an ELM spin-2 VBF signal (bottom). The inner and outer bands show the $\pm 1\sigma$
and $\pm 2\sigma$ ranges around the expected limit. The full curves without dots correspond to the predicted theoretical cross sections.} 
 \label{fig:limit-spin2}
\end{figure}
\FloatBarrier