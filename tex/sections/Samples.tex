\chapter{Data and MC samples}
\label{chap:sam}

The data used in this analysis were collected by the ATLAS detector at the LHC from the $pp$ collisions at the center-of-mass energy $\sqrt{s}=13\TeV$ in 2015 and 2016. The total integrated luminosity of the 2015 and 2016 datasets is 36.1\,fb$^{-1}$. Single-electron and single-muon triggers, as listed in Table~\ref{tab:trigger}, are used.
These triggers have transverse momentum (\pT) thresholds that depend on the data-taking periods for the 2015 and 2016 datasets.
\begin{table}[!htbp]
  \caption{\small The minimum \pT requirements used at the different levels of the trigger in different data taking periods. Letters ``$m$'',``$l$'' and ``$t$'' next to the threshold value stand for the medium, loose and tight electron identification requirement, respectively. Letter ``$i$'' indicates an isolation requirement, that is less restrictive than the isolation requirement used in the offline selection. The single-lepton trigger with higher-\pT thresholds are more efficient at high lepton \pT than the lower-\pT triggers because of this isolation requirement.}
  \label{tab:trigger}
  \vspace{0.4cm}
  \centering
\resizebox{\textwidth}{!}{
  \begin{tabular}{c|c|c|c}
   \hline
Lepton & Period & Level-1 trigger & High-level trigger \\\hline
\multirow{4}{*}{$e$}  & 2015 & \multirow{4}{*}{20\GeV} & 24m OR 60m OR 120l\GeV \\
  & 2016 up to $0.5\times 10^{34}\,{\text{cm}}^{-2}s^{-1}$ & & 24m\_i OR 60m OR 140l\GeV \\
  & 2016 up to $1.0\times 10^{34}\,{\text{cm}}^{-2}s^{-1}$ & & 24t\_i OR 60m OR 140l\GeV\\
  & 2016 up to $1.2\times 10^{34}\,{\text{cm}}^{-2}s^{-1}$ & & 26t\_i OR 26m\_i OR 60m OR 140l\GeV\\\hline
 \multirow{4}{*}{$\mu$} & 2015 & 15\GeV & 20i OR 50\GeV \\ \cline{2-4}
  & 2016 up to $0.5\times 10^{34}\,{\text{cm}}^{-2}s^{-1}$ & \multirow{3}{*}{20\GeV}& 24i OR 40 OR 50\GeV \\
  & 2016 up to $1.0\times 10^{34}\,{\text{cm}}^{-2}s^{-1}$ & & 24i OR 50\GeV\\
  & 2016 up to $1.2\times 10^{34}\,{\text{cm}}^{-2}s^{-1}$ & & 26i OR 50\GeV\\
 \hline
\end{tabular}
}
\end{table}

For the 2016 datasets, the un-prescaled triggers with the lowest \pT thresholds depend on the maximum instantaneous luminosity:
up to $0.5\times 10^{34}\,{\text{cm}}^{-2}s^{-1}$, they are {\ttfamily e24\_lhmedium\_ivarloose (nod0)} and {\ttfamily mu24\_ivarloose/iloose} for single electrons and muons, respectively; 
up to $1.0\times 10^{34}\,{\text{cm}}^{-2}s^{-1}$, {\ttfamily e24\_lhtight\_ivarloose (nod0)} and {\ttfamily mu24\_ivarmedium/imedium}; 
and up to $1.2\times 10^{34}\,{\text{cm}}^{-2}s^{-1}$, {\ttfamily e26\_lhtight\_ivarloose (nod0)}, \\
{\ttfamily e26\_lhtight\_smooth\_ivarloose} and {\ttfamily mu26\_ivarmedium/imedium}.
Each event is required to pass at least one of the single-lepton triggers. %in the pre-selection, which is the trigger cut.
A so-called ``trigger matching'' cut is also applied just after the trigger application, in order to verify that the two selected leptons have an electron that fired a single-electron trigger, or a muon that fired a single-muon trigger.

The menus postfixed with {\ttfamily\_ivarloose}, {\ttfamily\_ivarloose}, {\ttfamily\_ivarmedium} or {\ttfamily\_imedium} contain online isolation cuts which are usually looser than those used in the offline selection. There is no isolation cut applied for the trigger menus used for 2015 data analysis.

The single-lepton trigger efficiencies were measured using $Z$ boson candidates as a function of lepton \pT and $\eta$~\cite{MAaboud:ATLASTrigger2015}. A tag-and-probe method was used to determine the L1 and HLT lepton efficiencies. The efficiency is calculated as the ratio of the number of probe leptons ($e$ or $\mu$) passing the trigger selection to the number of probe leptons. 
%The overall (L1 and HLT) single-lepton efficiencies are approximately 70\% for muons with $|\eta|<1.05$, 90\% for muons in the range $1.05<|\eta|<2.40$, and $\geq 90$\% for electrons in the range $|\eta|<2.40$. 
The trigger efficiencies for electrons are approximately 100\% and 90\% for L1 and HLT, respectively, in the range of $|\eta|<2.40$.
The L1 muon trigger efficiencies are approximately 70\% and 90\% in the barrel ($|\eta|<1.05$) and the end-caps ($1.05<|\eta|<2.40$), respectively. The difference is due to the different geometrical acceptance of the barrel and end-cap trigger systems and inefficiencies of the local detectors. The relative efficiency of HLT with regard to L1 is close to 100\% in both the barrel and the end-caps.
The trigger efficiency for $WW$ events passing the event selection (described in Chapter~\ref{chap:sel}) has also been studied and described in detail in Appendix~\ref{sec:app:trigeff}. This includes two sets of efficiencies: the trigger efficiency and the trigger and trigger matching efficiency. Both are found to be greater than 99\%.
Data quality criteria were applied to ensure that events are recorded with stable beam conditions and with all relevant subdetector systems operational.

Samples of simulated signal and background events are used to optimise the event selection and to estimate the signal acceptance and the background yields from various SM processes.

The sample for the NWA heavy Higgs boson signal was generated with 
\textsc{Powheg-Box}\,2.0~\cite{Nason:2004rx,Frixione:2007vw,Alioli:2010xd} which calculates separately the ggF~\cite{Alioli:2008tz} and VBF~\cite{Nason:2009ai} production mechanisms with matrix elements up to next-to-leading order (NLO) in QCD.  It uses the CT10 NLO PDF set~\cite{Lai:2010vv} and is interfaced with 
\textsc{Pythia\,8.186}~\cite{Sjostrand:2007gs} for the $H\to WW$ decays, for parton showering and for hadronisation. The AZNLO tune~\cite{Aad:2014xaa} is used for the underlying event description.
The NWA Higgs boson was generated with a width of 4\MeV. This same event sample is used to derive the exclusion regions for 2HDM.

The \textsc{Powheg-Box} samples only describe the production of a ggF induced Higgs-like resonance in association with one jet at leading-order (LO) precision, while further jets were emulated by the  parton shower generator, \textsc{Pythia}. A more precise calculation of higher jet multiplicities is provided by using the \textsc{MadGraph5\_aMC@NLO} generator~\cite{Alwall:2014hca} to simulate $gg\rightarrow H$ events in association with up to two jets at NLO precision. Here, the overlap between identical final states generated at the matrix element (ME) and the parton shower (PS) stage is removed using FxFx merging~\cite{Frederix:2012ps}. The fraction of ggF events passing the event selection requirements of the $N_\text{jet}=1$ and $\geq 2$ VBF categories (defined in Section~\ref{sec:sel:sr}) predicted by the \textsc{Powheg-Box} generator is reweighted to match that of the \textsc{MadGraph5\_aMC@NLO} FxFx samples, with a detailed study described in Appendix~\ref{sec:app:powrew}. The corresponding scale factors are calculated for several hypothetical heavy Higgs masses. They are the largest, $1.14$ ($0.91$), for the 200\GeV mass point, and decrease with increasing resonance mass to a value of $0.85$ ($0.73$) for the 4\TeV mass point, for the $N_{\rm jet}=1$ ($N_{\rm jet} \geq 2$) VBF category.

The LWA heavy Higgs boson signal is simulated at NLO using 
\textsc{MadGraph5\_aMC@NLO}
with the NNPDF23LO PDF set~\cite{Carrazza:2013axa}.  The generated 
particles at matrix element level were showered by \textsc{Pythia}\,8.186 with the \textsc{A14} setting~\cite{ATL-PHYS-PUB-2014-021} for the tunable parameters of the underlying event.
The mass of the heavy Higgs boson signals considered in this analysis spans  
the range between 200\GeV and 4\TeV. Both NWA and LWA samples have been generated in steps of 100\GeV
up to 1\TeV, and in steps of 200\GeV thereafter. 

Samples for the GM, HVT and bulk RS graviton benchmark signal models were generated at LO using \textsc{MadGraph5\_aMC@NLO},
which is interfaced to \textsc{Pythia}\,8.186 with the NNPDF23LO PDF set. 
For the GM benchmark model, a value of $\sin\theta_H = 0.4$ was chosen. %\textcolor{red}{check!}.
Samples for the HVT interpretation in the qqA production mode were generated according to the extended gauge symmetry model A~\cite{Pappadopulo2014} with $g_V=1$, while for the VBF production mode, samples were generated using the same $g_V$ value but the couplings to the fermions are set to zero, such that the new vector boson couples only to the SM vector and Higgs bosons.
For the RS graviton signal model, a curvature scale parameter $k/\bar{M}_\text{Pl}$ being either 0.5 or 1 is considered.
The branching ratio for the decays $G_\text{KK}\to WW$ is larger than 30\%. 
The VBF spin-2 signals were produced at LO with VBFNLO v3.0.0 beta 2~\cite{Baglio:2014uba} with the NNPDF30LO PDF set~\cite{NNPDF30} and the following parameter setting~\cite{Frank:2012wh} was used: $\Lambda_{f\!f}=3$\TeV, $n_{f\!f}=4$, 
$\Lambda=1.5$\TeV and $f_1=f_2=f_5=1$. 
The mass range considered in the analysis is between 200\GeV and 5\TeV for the KK graviton signal, and between 250\GeV and 5\TeV for the HVT qqA signal.
For the GM and ELM VBF signals, the mass range is between 200\GeV and 1\TeV, while for the HVT VBF signal, the range is between 300\GeV and 1\TeV.

Events from the production of single-top quark, $t\bar{t}$, dibosons ($WW$, $WZ$ and $ZZ$), $Z/\gamma^\ast+$jets %($\gamma^\ast$ being a virtual photon) 
and $W+$jets are the main sources of SM backgrounds.
Samples for single-top quark were generated with \textsc{Powheg-Box}\,2.0~\cite{Alioli:2009je,Re:2010bp} using the CT10 NLO PDF set, which is interfaced to \textsc{Pythia}\,6.428~\cite{Sjostrand:2006za} for the parton showering, and \textsc{Perugia}2012~\cite{Skands:2010ak} tune and CTEQ6L1 PDF~\cite{Pumplin:2002vw} were used for the underlying event description. 
The simulated $t\bar{t}$ events were generated with \textsc{Powheg-Box}\,2.0~\cite{Alioli:2011as} using the NNPDF30NLO PDF set~\cite{NNPDF30}, that is interfaced to \textsc{Pythia}\,8.186 for the parton showering, with the \textsc{A14} tune and CTEQ6L1 PDF used for the description of the underlying events.
The resummation damping parameter, $h_\text{damp}$, was set to be one and a half times of the top-quark mass, $m_\text{top}$, which was set at 172.5\GeV. 
The $h_\text{damp}$ parameter is used to control the ME/PS matching and effectively regulate the high-$\pT$ radiation.
The properties of the bottom and charm hadron decays were modelled by the \textsc{EvtGen}\,1.2.0~\cite{Lange:2001uf} package.
For the diboson backgrounds, simulated events were generated using \textsc{Sherpa}\,2.1.1~\cite{Gleisberg:2008ta,Gleisberg:2008fv,Cascioli:2011va,Schumann:2007mg,Hoeche:2012yf}, for the $gg$ production processes, and \textsc{Sherpa}\,2.2.1, for the $q\bar{q}$ production processes. CT10 NLO and NNPDF30NNLO PDF sets were used for the two different processes, respectively. 
The \textsc{Sherpa} generator for the $q\bar{q}$ production processes produced up to one additional parton at NLO and up to three additional partons at LO.
The $W$ and $Z$ bosons in association with jets were also generated with \textsc{Sherpa}\,2.1.1, with the CT10 NLO PDF set, where $b$- and $c$-quarks are treated to be massive particles. 
In the $gg\to WW$ production, the contribution of the SM Higgs boson at 125\GeV and the interference effects between the two were also included. 
The VBF part of the SM Higgs boson was generated with \textsc{Powheg-Box}~\cite{Nason:2009ai}, that is interfaced to \textsc{Pythia}\,8.186 for the parton showering.

The impact of multiple $pp$ interactions occured in the same and neighbouring bunch crossings (pile-up) is also included by overlaying minimum-bias collisions, that are generated with \textsc{Pythia}\,8.186, and considered for each generated signal and background event.  
The number of overlaid collisions is configured such that the distribution of the average number of interactions per $pp$ bunch crossing, $\langle \mu \rangle$, in the simulation is matched to the pile-up conditions observed in the data, which is approximately 25 interactions per bunch crossing on average.
Figure~\ref{fig:mu1516} shows the mean number of interactions per crossing for the 2015 and 2016 $pp$ collision data at 13\TeV centre-of-mass energy.
The generated samples were processed through a \textsc{Geant4}-based detector 
simulation~\cite{Agostinelli:2002hh,Aad:2010ah}, followed by the standard ATLAS
reconstruction software used for collision data.

\begin{figure}[htbp]
 \centering
 \includegraphics[width=0.8\textwidth]{fig/other/mu_2015_2016.pdf}
 \caption{Mean number of interactions per crossing for the 2015 and 2016 $pp$ collision data at 13\TeV. All data delivered to ATLAS during stable beams is shown. The integrated luminosity and $\langle \mu \rangle$ are given in the figure.}
 \label{fig:mu1516}
\end{figure}
\FloatBarrier
