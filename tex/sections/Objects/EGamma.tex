\section{Electrons and photons}
\label{sec:obj:eg}

The reconstruction of electrons and photons~\cite{Aad:2014nim} in the pseudorapidity region $|\eta|<2.47$ is started from clusters with deposited energy in the EM calorimeters.
First the calorimeters are divided into a grid of towers ($N_{\eta}\times N_{\phi}$) with a constant size of $\Delta\eta\times\Delta\phi = 0.025\times 0.025$.
The energy deposition in the towers is calculated by an integration over all the cells in all longitudinal layers.
A sliding-window algorithm~\cite{Lampl:2008zz} with a window size of $3\times 5$ towers is then applied to search for clusters that have a total transverse energy above 25\GeV. 
Such a cluster will be chosen as a seed, and the reconstruction is continued around this seed.
The energy of these clusters is calibrated to the original energy using multivariate techniques~\cite{Aad:2014nim} based on simulated MC samples.
A match is performed from the seed clusters to the tracks that are well-reconstructed and originate from 
the primary vertex
%a vertex 
in the inner detector.
Matched clusters are considered as electron candidates or converted photons if the track is consistent with originating from a photon conversion. Clusters without matching any tracks are considered as unconverted photons.
The clusters are then rebuilt using a set of calorimeter cells\footnote{For electrons, this correspondes to $3\times 7$ cells in the EMB and $5\times 5$ cells in the EMEC in the second layer.}.
This choice gives an optimised balance between the conflicting requirements of collecting all the energies even in the case of hard bremsstrahlung and of preserving the energy resolution by minimizing the contributions from the noise and pile-up.
Electrons and photons reconstructed near regions of the calorimeter affected by read-out or HV failures are rejected.
%The cluster energy reconstructed in the EM calorimeters is corrected from a calibration based on the full detector simulation.

The relative energy resolution can be parameterised as follows:
\begin{equation}
\frac{\sigma}{E} = \frac{a}{\sqrt{E}} \oplus \frac{b}{E} \oplus c\,,
\end{equation}
where $a$, $b$ and $c$ are $\eta$-dependent parameters: $a$ is the sampling term, which contributes mostly at low energy; $b$ is the noise term, which is dominated by the pile-up noise at high $\eta$; $c$ is the constant term. At high energy, the resolution tends asymptotically to the constant term, $c$, which has a design value of $0.7\%$.

However, what we are interested in in our analysis are prompt electrons decayed from the $W$ boson, instead of other background objects that can also be built as electron candidates by reconstruction algorithms, including hadronic jets and electrons from photon conversions, Dalitz decays and semi-leptonic heavy-flavour hadron decays.
Therefore, the identification algorithms are also necessary in the reconstruction of electrons.
This is usually done in two different ways. One is called the cut-based method and the other is the likelihood (LH) method~\cite{Aaboud:2016vfy}.
Both make use of variables that can describe the longitudinal and lateral shapes of the EM showers in the calorimeters, the properties of the tracks in the ID and the matching between tracks and energy clusters. 
Electrons used in this analysis are identified using the LH identification method.
In the LH identification, multivariate analysis (MVA) techniques~\cite{Hocker:2007ht} are used and the final discriminant variable can be expressed as:
\begin{equation}
d_{\mathcal{L}} = \frac{\mathcal{L}_{\rm S}}{\mathcal{L}_{\rm S} + \mathcal{L}_{\rm B}},\,\,\mathcal{L}_{\rm S(B)}(\vec{x})=\prod^{n}_{i=1}P_{{\rm S(B)},i}(x_i)
\end{equation}
where $\vec{x}$ is the vector of variable values, while $P_{{\rm S},i}(x_i)$ and $P_{{\rm B},i}(x_i)$ are the values of the probability density functions of the $i$-th variable evaluated from data at $x_i$ for the prompt electrons and other background objects, respectively.

Three sets of requirement on the discriminant variable have been developed so far in order to reject light-flavour jets and conversions:
\begin{description}
\item[LooseLH selection:] features the most powerful variables for discrimination against ligh-flavour jets. In addition, a requirement on the number of hits on the track in the B-layer is applied to better reject photon conversions.
\item[MediumLH selection:] uses tighter requirements on the discriminant variable than the ``LooseLH'' selection. Besides, the transverse impact parameter $d_0$ and its significance $\sigma_{d_0}$ defined as the ratio of the magnitude of $d_0$ to its uncertainty are also used to construct the MVA discriminant variable.
\item[TightLH selection:] has the tightest requirements and the smallest selection efficiency. In addition to the variables used for the ``MediumLH'' selection, electron candidates that are matched to reconstructed photon conversions are vetoed.
\end{description}
The ``LooseLH'', ``MediumLH'' and ``TightLH'' regimes are defined such that the samples selected by them are subsets of one another, i.e. electrons selected by ``MediumLH'' are all selected by ``LooseLH'', and ``TightLH'' electrons are all selected by ``MediumLH''.

The electron identification performance may be affected by the parasitic collisions in the same beam crossing (called in-time pile-up) or a consecutive bunch crossing (called out-of-time pile-up) as the hard $pp$ collision producing the electron candidates~\cite{ATLAS-CONF-2016-024}.
Some shower shape distributions depend on the number of pile-up collisions per bunch crossing, therefore, the requirements on the discriminant variable are loosened as a function of the number of primary vertices, which makes the LH identification still efficient at high pile-up without background increased dramatically.


In addition to the reconstruction and identification criteria described above, electrons are usually required to fulfil isolation requirements~\cite{ATLAS-CONF-2016-024} to further disentangle prompt electrons from the background objects.
Two discriminant variables are designed for this purpose:
\begin{itemize}
\item calorimeter isolation variable, $E_{\rm T}^{{\rm cone}0.2}$, defined as the sum of transverse energies of the EM topological clusters within a cone of $\Delta R=0.2$ around the candidate electron cluster.
\item track isolation variable, $p_{\rm T}^{{\rm varcone}0.2}$, defined as the sum of transverse momenta of all tracks that satisfies quality requirements~\cite{ATLAS-CONF-2016-024} within a cone of $\Delta R={\rm min}(0.2, 10\GeV/E_{\rm T})$ around the candidate electron track.
\end{itemize}
Depending on the requirements on the two variables mentioned above, two different kinds of working points are developed: 
fixed requirement working points where the upper thresholds on the isolation variables are constant, 
and efficiency targeted working points where requirements are varied to obtain a given isolation efficiency, which is estimated typically from simulated $Z\to ee$ events.
Table~\ref{tab:isolation} shows the definition of the various efficiency targeted working points for electron isolation.
\begin{table}[!htbp]
\center
\caption{Efficiency targeted isolation working points for electrons.
For the Gradient and GradientLoose working points, $E_{\rm T}$ is in \GeV.
The calorimeter and track isolations refer 
%The calorimeter and track isolation refer 
to the selection based on $E_{\rm T}^{{\rm cone}0.2}/E_{\rm T}$ and $p_{\rm T}^{{\rm varcone}0.2}/E_{\rm T}$, respectively.}
\label{tab:isolation}
    \vspace{2mm}
\resizebox{\textwidth}{!}{
\begin{tabular}{|c||c|c|c|}\hline
& \multicolumn{3}{c|}{Efficiency} \\
\hline
Working points & calorimeter isolation & track isolation & total efficiency \\
\hline\hline
LooseTrackOnly & - & $99\%$ & $99\%$ \\
Loose & $99\%$ & $99\%$ & $\sim 98\%$ \\
Tight & $96\%$ & $99\%$ & $\sim 95\%$ \\
Gradient & $0.1143\%\times E_{\rm T}+92.14\%$ & $0.1143\%\times E_{\rm T}+92.14\%$ & $90/99\%$ at 25/60\GeV \\
GradientLoose & $0.057\%\times E_{\rm T}+95.57\%$ & $0.057\%\times E_{\rm T}+95.57\%$ & $95/99\%$ at 25/60\GeV \\
\hline
\end{tabular}   
}
\end{table}

In this analysis, for the electron identification, ``MediumLH'' and ``TightLH'' selections are used for the leading two leptons ($\pT > 25\GeV$) and vetoing additional leptons ($15\GeV < \pT < 25\GeV$), respectively, corresponding to efficiencies of 
%$\sim 89\%$ and $\sim 80\%$ at $\pT = 25\GeV$,
$\sim 84\%$ and $\sim 74\%$ at $\pT = 25\GeV$.
The corresponding probabilities of misidentification of electrons from hadrons are 
$\sim 0.5\%$ and $\sim 0.3\%$, respectively.
While for the electron isolation, the ``Gradient'' and ``GradientLoose'' working points are used for the leading two leptons and vetoing additional leptons, respectively, corresponding to efficiencies of $90\%$ and $95\%$ at $\pT = 25\GeV$ ($99\%$ at $\pT = 60\GeV$ for both).


%Electrons are selected from clusters of energy deposits in the electromagnetic calorimeter that match a track reconstructed in the inner detector. They are identified using the likelihood identification criteria and Ref.~\cite{ATLAS:2016iqc}.
%The electrons used in this analysis are required to pass the ``MediumLH'' (when $\pT > 25\GeV$) or the ``TightLH'' (when $\pT < 25\GeV$) selection and be within $|\eta|<2.47$, excluding the transition region between the barrel and endcaps in the LAr calorimeter ($1.37 < |\eta| < 1.52$). 
%Here, $\pT$ refers to the transverse momentum of the electron that is measured by the electromagnetic calorimeter.
%These ``MediumLH'' and ``TightLH'' selection categories have identification efficiencies of $94\%$ and $88\%$ for electrons with transverse energy $\pT$ of 100\,GeV, respectively. 
%The corresponding efficiency to identify hadrons as electrons is approximately $0.5\%$ and $0.3\%$, respectively. 
%
%
%The reconstruction of electrons and photons is seeded using a sliding-window algorithm with the window size corresponding to 5 $\times$ 5 cells in the middle layer of the electromagnetic calorimeter cluster of fixed size, and then is reconstructed around this seed. 
%For electrons, the energy in the barrel electromagnetic calorimeter is collected over an area corresponding to 3 $\times$ 7 cells in the middle layer or 0.075 $\times$ 0.175 in $\Delta\eta \times \Delta\phi$. This choice gives an optimised balance between the conflicting requirements of collecting all the energies even in the case of hard bremsstrahlung and of preserving the energy resolution by minimizing the contributions from the noise and pile-up. 
%For unconverted photons, adequate performance is obtained by limiting the area to 3 $\times$ 5 cells in the middle layer, whereas converted photons are treated like electrons. 
%Finally, for the end-cap electromagnetic calorimeters, an optimal area of 5 $\times$ 5 cells in layer 2 has been chosen for both electrons and photons.
%
%In addition to the calorimeter-seeded electron and photon reconstruction, a second electron reconstruction and identification algorithm uses good-quality tracks as a seed and constructs a cluster around the extrapolated impact point in the calorimeter. 
%This algorithm relies more on the electron identification capabilities of the inner detector and has been developed to improve the efficiency for low-\pT electrons as well as for electrons close to jets. 
%The algorithm matches good-quality inner detector tracks to small clusters of electromagnetic energy. For a given track, only the energy contained in a small window along the track extrapolation is used and the contribution of neighbouring hadronic showers is therefore reduced. 
%The identification procedure takes full advantage of the tracking and electron-identification capabilities of the TRT in the inner detector (over $|\eta| < 2.0$), as well as of the granularity of the electromagnetic calorimeter. A likelihood ratio combines inner detector information (measured track momentum and transition-radiation hits) with shower-shape variables from the calorimeter.
%%%In the following, unless specified otherwise, only the results of the calorimeter-seeded algorithm will be discussed.
%
%%%For the standard reconstruction of electrons and photons, a seed cluster is taken from the electromagnetic calorimeter and a loosely matching track is searched for among all reconstructed tracks. Additionally, the candidate is flagged if it matches a photon conversion reconstructed in the inner detector. Electron and photon candidates are thus separated reasonably cleanly, by requiring the electrons to have an associated track but no associated conversion. In contrast, the photons are defined as having no matched track, or as having been matched to a reconstructed conversion.
%
%The standard identification for isolated high-\pT electrons is based on cuts on the shower shapes, on information from the reconstructed track and on the combined reconstruction. 
%Jet rejections are computed with respect to truth-particle jets reconstructed using particle four-momenta within a cone of size $\Delta R = 0.4$. 
%Three sets of cuts have been studied depending on the signal efficiency and jet rejection requirements:
%\begin{itemize}
%\item ``loose cuts'', which consist of simple shower-shape cuts (longitudinal leakage, shower shape in the middle layer of the electromagnetic calorimeter) and very loose matching cuts between reconstructed track and calorimeter cluster; 
%\item ``medium cuts'', which add shower-shape cuts using the important information contained in the first layer of the electromagnetic calorimeter and track-quality cuts similar to the standard reconstruction cuts;
%\item ``tight cuts'', which tighten the track-matching criteria and the cut on the energy-to-momentum ratio. These cuts also explicitly require the presence of a vertexing-layer hit on the track (to further reject photon conversions) and a high ratio between high-threshold and low-threshold hits in the TRT detector (to further reject the background from charged hadrons). Additionally, further isolation of the electron may be required by using calorimeter energy isolation beyond the cluster itself. Two sets of tight cuts are used in this section to illustrate the overall performance of the electron identification. They are labelled as ``tight (TRT)'', in the case where a TRT cut with approximately 90\% efficiency for electrons is applied, and as ``tight (isol.)'', in the case where a TRT cut with approximately 95\% efficiency is applied in combination with a calorimeter isolation cut. 
%\end{itemize}
%
%Photons are much harder to extract as a signal from the jet background than certain specific isolated electron signals, such as those expected from $Z\to ee$ or $W\to e\nu$ decays. A single set of photon identification cuts, equivalent to the ``tight cuts'' defined for electrons, has been optimised based on the shower shapes in the calorimeter with special emphasis on separating single $\pi^{0}$ from photons using the very fine granularity in $\eta$ of the strip layer. In addition, a simple track-isolation criterion has been added to further improve the rejection while preserving the vast majority of converted photons.
