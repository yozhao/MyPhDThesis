\section{Muons}
\label{sec:obj:mu}

%The collisions at the LHC produce a broad spectrum of final-state muons, ranging from low momentum non-isolated muons in $b$-jets to high-momentum isolated muons from $W/Z$-boson decays or from possible new physics. The experiment detects and measures muons in the MS and also exploits the measurements in the ID and the calorimeters to improve the muon identification efficiency and momentum resolution. Muon measurements are a combination of accurate measurements in the MS and in the ID. The MS also efficiently triggers on muons over a wide range of energies and over $|\eta| < 2.4$. The ID provides the best measurement at low to intermediate momenta, whereas the MS takes over above 30\GeV. The toroidal field guarantees excellent momentum resolution even at the highest values of $\eta$.

%Muons with momenta ranging from approximately 3\GeV to 3\TeV are identified and measured with optimal acceptance and efficiency through the use of a combination of three track reconstruction strategies:
%\begin{description}
%\item [Stand-alone:] muon track reconstruction based solely on the MS data over the range $|\eta| < 2.7$ (defined by the spectrometer acceptance);
%\item [Combined:] combination of a muon-spectrometer track with an inner-detector track over the range $|\eta| < 2.5$ (defined by the inner-detector acceptance);
%\item [Segment tag:] combination of an inner-detector track with a muon-spectrometer segment, i.e. a straight-line track, in an inner muon station.
%\end{description}

%Track reconstruction in the MS is logically sub-divided into the following stages: pre-processing of raw data to form drift-circles in the MDTs or clusters in the CSCs and the trigger chambers (RPCs and TGCs), pattern-finding and segment-making, segmentcombining, and finally track-fitting. Track segments are defined as straight lines in a single MDT or CSC station. The search for segments is seeded by a reconstructed pattern of drift-circles or clusters or by drift-circles or clusters lying in a region of activity, which is defined by the trigger chambers and has a size of the order of $0.4 \times 0.4$ in $\eta$---$\phi$ space.


Muon reconstruction is usually first performed in the ID and MS independently.
Then, the information from each of them is combined to reconstruct tracks that are finally used in physics analyses.
The reconstruction of muons in the ID is same as any other charged particles, as described in Section~\ref{sec:obj:tv}.

There are four types of muons depending on the subdetectors used in the reconstruction~\cite{Aad:2016jkr}:
\begin{description}
\item[Combined (CB) muon:] the combined track is formed with a global fit using the hits from the ID and the MS. During the fit, MS hits can be added or removed from the track to improve the fit quality. Most muons are reconstructed following an outside-in pattern recognition, where muons are first reconstructed in the MS and then extrapolated inward to be matched to an ID track. As a complementary approach, an inside-out reconstruction is used, in which ID tracks are extrapolated outwards and matched to MS tracks.
\item[Segment-tagged (ST) muons:] if a track in the ID can be extrapolated to the MS and is associated with at least one local track segment in the MDT or CSC chambers, it will be classified as a muon. 
Such kind of muons is
%Such kind of muons are 
usually used when muons cross only one layer of MS chambers (either because of their low $\pT$ or because they fall in regions with reduced MS acceptance).
\item[Calorimeter-tagged (CT) muons:] if a track in the ID can be matched to an energy deposit in the calorimeters compatible with a minimum-ionizing particle, it will also be identified as a muon. ``CT muons'' have the lowest purity of all the types of muons, but they recover acceptance in the region where the MS is only partially instrumented (to allow for cabling and services for the ID and calorimeters).
\item[Extrapolated (ME) muons:] the muon trajectory is reconstructed based only on the MS track and a loose requirement on compatibility with originating from the interaction point. Energy loss of the muons in the calorimeters is also estimated and taken into account. Generally, muons are required to traverse at least two layers of the MS chambers (three layers in the forward region) to provide a track measurement. 
This type of muons is
%This type of muons are 
mainly used to extend the acceptance in the region $2.5<|\eta|<2.7$, which can not be covered by the ID.
\end{description}

Overlaps between different muon types are also considered and resolved for physics analyses.
When two different types share the same ID track, the priority is in order of: ``CB muons'', ``ST muons'' and ``CT muons''.
The overlap with ``ME muons'' is resolved by analysing the hits of the track and selecting the track with better fit quality and larger number of hits.

Muon momentum calibration is also performed to identify the corrections to the simulated muon transverse momentum reconstructed in the ID and MS subdetectors, in order to precisely describe the measurement of the same quantities in data. Only ``CB muons'' are used to extract the calibration parameters. The ID (MS) track reconstruction uses hits from the corresponding detector and is extrapolated to the interaction point. Corrections are also applied to the momentum resolution according to the following formula:
\begin{equation}
\frac{\sigma(\pT)}{\pT} = r_0/\pT \oplus r_1 \oplus r_2 \pT\,,
\end{equation}
where, $\oplus$ denotes a sum in quadrature, and the first term accounts mainly for fluctuations of the energy loss in the transverse material, the second term for multiple scattering, local magnetic field inhomogeneities and local radial displacements of the hits, and the third term describes intrinsic resolution effects that are caused by spatial resolution of the hit measurements and residual misalignment of the muon spectrometer.

Muon identification is generally performed by applying quality requirements that suppress backgrounds, mainly from pion and kaon decays.
This should be aimed at selecting prompt muons with high efficiency (usually simulated $t\bar{t}$ events are used with muons from $W$ decays categorised as signal muons and muon candidates from light-hadron decays categorised as background muons) with a robust momentum measurement.
Variables providing good discrimination between prompt muons and background muons are used.
To ensure a robust momentum measuremtn, specific requirements on the number of hits in the ID and MS are used.

Four muon identification four selections~\cite{Aad:2016jkr} have been developed for physics analyses in ATLAS:
\begin{description}
\item[Loose muons:] developed to maximise the reconstruction efficiency but also providing good-quality muon tracks. They are specifically optimised for Higgs boson reconstruction with a four-lepton final state. All types of muon reconstruction are used.
\item[Medium muons:] designed to be used as a default selection for muons in ATLAS, while minimising the systematic uncertainties that are associated with the reconstruction and calibration of muons. Only ``CB muons'' and ``ME muons'' are used. Besides, a loose selection on the compatibility between the momentum measurement in the ID and that in the MS is applied to suppress the misidentification of hadrons as muons.
\item[Tight muons:] selected to maximise the purity of muons at the cost of losing some efficiency. Only ``CB muons'' with hits in at least two stations of the MS but satisfying the ``Medium'' selection criteria are considered.
\item[High-$\pT$ muons:] aimed to maximise the momentum resolution for tracks with $\pT > 100\GeV$. And this type is mainly developed and optimised for searches for high-mass $Z^{\prime}$ and $W^{\prime}$ resonances. ``CB muons'' passing the ``Medium'' selection are selected but additionally they are required to have at least three hits in three stations of the MS.
\end{description}
As with the electron identification, these selections are inclusive categories such that muons identified with tighter selections are also included in the looser categories.
The misidentification is usually estimated using MC simulation and validated using data by measuring the probability that pions are reconstructed as muons.


In this analysis, ``CB muons'' are used and 
%reconstructed by combining the ID and the MS tracks that have consistent trajectories and curvatures~\cite{Aad:2014rra}.
the muon candidates 
%used in this analysis 
are required to have $|\eta|<2.5$ and pass the ``Medium'' (when $\pT > 25\GeV$) or the ``Tight'' (when $\pT < 25\GeV$) selection.
%, defined based on the quality of the reconstruction and identification. 
These selections have a reconstruction efficiency of approximately $96\%$ and $92\%$, respectively, for muons originating from the decay of $W$ bosons~\cite{Aad:2016jkr}. 
The corresponding probabilities to identify hadrons as muons are approximately $0.2\%$ and $0.1\%$, respectively. 

Although the ``High-$\pT$ muons'' regime is not directly used in this analysis, it is thought to be mostly included by the ``Medium'' working point. Anyway, study was performed to check this, as described in Appendix~\ref{sec:app:highptmu}, and there is no indication that this would have a significant impact on the results, taking also the limited statistics of data into account.
%
The isolation and isolation efficiency for muons are the same as that for electrons, except that the variable cone size starting at $\Delta R = 0.3$ for the track isolation.
%in calorimeter is based on energy deposits within a cone $\Delta R$ of 0.2 around the muons, while the track isolation uses a variable cone size that starts at $\Delta R = 0.3$, and shrinks as muon $\pT$ increases~\cite{isolation}.
