\section{Definition of signal regions}
\label{sec:sel:sr}

The event selections in the SRs include basically two parts: the pre-selection aimed at selecting $WW$ candidate events with very general and initial cuts, and the additional cuts aimed at defining specific SRs.

The event pre-selection includes requirements on the leading and subleading lepton ($e$ or $\mu$) transverse momentum $\pT^\ell$: $\pT^{\ell,{\text{\rm lead}}}>25$\GeV and $\pT^{\ell,{\text{sublead}}}>25$\GeV.
Each event is required to have two oppositely charged leptons with different flavours and no additional lepton with $\pT^{\ell, \text{other}}>15$\GeV in order to suppress diboson backgrounds. The identification requirement for the addtional leptons are same with the two leading leptons when $\pT>25$\GeV, satisfying the minimal quality criteria (``MediumLH'' for electrons and ``Medium'' for muons) as discussed in Chapter~\ref{chap:obj}, but more strict (``TightLH'' for electrons and ``Tight'' for muons) when $15\GeV < \pT < 25\GeV$. The isolation requirement for the additional leptons is looser (``GradientLoose'' in stead of ``Gradient'' for the two leading leptons).
Backgrounds from low-mass resonances decaying to different flavour final states via $\tau^+\tau^-$  are rejected by requiring $m_{\ell\ell}>10$\GeV, with $m_{\ell\ell}$ being the invariant mass of the leading and subleading leptons.

Additional cuts, that are used in the SRs, are optimised according to the optimisation strategy described in Section~\ref{sec:sel:opt}. For this optimisation, the NWA signal samples are used. Nevertheless, the same event selections are applied to all signal models and mass points.

Since the background rate and composition, as well as the signal topology are significantly dependent on the jet multiplicity, three orthogonal event categories are defined, two of them with one and at least two jets optimised for the VBF production and one quasi-inclusive category (excluding the VBF phase space) optimised for the ggF production. 
This is due to the fact that the VBF process is mostly associated with two or more jets, while in contrast, signal without accompanying forward jets is dominated by the ggF process.
The resulting selections that define the three event categories or SRs, namely the quasi-inclusive ggF (${\rm SR_{ggF}}$), the $N_{\text{jet}}=1$ VBF (${\rm SR_{VBF1J}}$) and the $N_{\text{jet}}\geq 2$ VBF (${\rm SR_{VBF2J}}$) are summarised in Table~\ref{tab:selcomp_lvlv}.
For the VBF $N_{\rm jet}=1$ category, two discriminating variables are used to minimise the contribution 
of the ggF signal: the (absolute) pseudorapidity of the jet, $|\eta_j|$, and the minimum value of the pseudorapidity distance 
between the jet and either of the leptons, $\min(|\Delta\eta_{j\ell}|)$.  
For the VBF $N_{\rm jet}\geq 2$ category, the invariant mass, $m_{jj}$, and the rapidity difference, $|\Delta y_{jj}|$, of the two leading jets are used to select the VBF signal. 
%\begin{table}[htbp]
%\begin{table}[!htbp]
%\center
%\caption{\small Selection conditions used in the ggF and VBF signal regions (SR). The VBF1J phase space corresponds to $N_{\rm jet}=1$,  $|\eta_j|>2.4$ and $\min(|\Delta\eta_{j\ell}|)>1.75$, while the VBF2J phase space corresponds to $N_{\rm jet}\geq 2$, $m_{jj}>500$\GeV and $|\Delta y_{jj}|>4$. These sets of selections ensure the orthogonality with respect to the ${\rm SR_{ggF}}$ category.}
%\label{tab:selcomp_lvlv}
%    \vspace{2mm}
%\resizebox{\textwidth}{!}{
%\begin{tabular}{|c|c|c|}\hline
% ${\rm SR_{ggF}}$ & ${\rm SR_{VBF1J}}$ & ${\rm SR_{VBF2J}}$ \\\hline
%\multicolumn{3}{|c|}{Pre-selection: $\pt^{\ell,\text{lead}}>25$\GeV, $\pt^{\ell,\text{sublead}}>25$\GeV, $m_{\ell\ell}>10$\GeV, veto if $\pt^{\ell,\text{other}}>15$\GeV }\\\hline
%\multicolumn{3}{|c|}{    $N_{\text{$b$-jet}}=0$ }\\
%\multicolumn{3}{|c|}{    $|\Delta \eta_{\ell\ell}|<1.8$} \\
%\multicolumn{3}{|c|}{    $m_{\ell\ell}>55$\GeV}\\
%\multicolumn{3}{|c|}{    $\pt^{\ell,\text{lead}}>45$\GeV}\\
%\multicolumn{3}{|c|}{    $\pt^{\ell,\text{sublead}}>30$\GeV}\\
%\multicolumn{3}{|c|}{    $\max(\mT^W)>50$\GeV}\\\hline
%Inclusive in $N_{\text{jet}}$ but excluding  & $N_{\text{jet}}=1$ & $N_{\text{jet}}\geq 2$ \\
%VBF1J and VBF2J phase space & $|\eta_j|>2.4$, $\min(|\Delta\eta_{j\ell}|)>1.75$ & $m_{jj}>500$\GeV, $|\Delta y_{jj}|>4$ \\\hline
%\end{tabular}   
%}
%\end{table}
\begin{table}[!htbp]
\center
\caption{Selection conditions and phase space definitions used in the ggF and VBF signal regions. 
}
\label{tab:selcomp_lvlv}
    \vspace{2mm}
\resizebox{\columnwidth}{!}{
\begin{tabular}{|c|c|c|}\hline
 SR$_\text{ggF}$ & SR$_\text{VBF1J}$ & SR$_\text{VBF2J}$ \\\hline
\multicolumn{3}{|c|}{Common selections}\\\hline
\multicolumn{3}{|c|}{    $N_{\text{$b$-tag}}=0$ }\\
\multicolumn{3}{|c|}{    $|\Delta \eta_{\ell\ell}|<1.8$} \\
\multicolumn{3}{|c|}{    $m_{\ell\ell}>55$\GeV}\\
\multicolumn{3}{|c|}{    $\pt^{\ell,\text{lead}}>45$\GeV}\\
\multicolumn{3}{|c|}{    $\pt^{\ell,\text{sublead}}>30$\GeV}\\
\multicolumn{3}{|c|}{veto if $\pt^{\ell,\text{other}}>15$\GeV}\\
\multicolumn{3}{|c|}{    $\max(\mT^W)>50$\GeV}\\\hline
ggF phase space & VBF1J phase space & VBF2J phase space \\\hline
Inclusive in $N_{\text{jet}}$ but excluding  & $N_{\text{jet}}=1$ and & $N_{\text{jet}}\geq 2$ and \\
VBF1J and VBF2J phase space & $|\eta_j|>2.4$, $\min(|\Delta\eta_{j\ell}|)>1.75$ & $m_{jj}>500$\GeV, $|\Delta y_{jj}|>4$ \\\hline
\end{tabular}   
}
\end{table}

The selected variables for discriminating signals from the large SM backgrounds are $\pt^{\ell,\text{lead}}$, $m_{\ell\ell}$ and $|\Delta\eta_{\ell\ell}|$, the latter being the pseudorapidity difference between the leading and subleading leptons. 
The signals tend to be at high $m_{\ell\ell}$ for high $m_{\rm H}$. 
The cut applied on $|\Delta\eta_{\ell\ell}|$ is actually due to the special kinematics of the Higgs boson (or other scalars) decaying in the $WW$ channel. 
The $W$ pairs originating from the decay of a scalar have to have opposite spin orientation, and due to the $V$-$A$ structure in the $W$ decay, the left-handed $e^{-}$ (right-handed $\mu^{+}$) is emitted along the $W^{-}$ ($W^{+}$) spin, assuming the final state is $e^{-}\bar{\nu_{e}}\mu^{+}\nu_{\mu}$.
Consequently, one of the two charged leptons is emitted along the momentum direction of the two $W$'s while the other one is emitted in the opposite direction.
In addition, to suppress the top quark background, events with number of $b$-jets ($N_{\text{$b$-jet}}$) greater than 0 are rejected from the signal regions. 
To reduce the $Z+$jets and $W+$jets contributions, two other variables are used: the transverse momentum of the subleading lepton ($\pt^{\ell,\text{sublead}}$) and the maximum value of the transverse mass calculated with either of the two leptons and the missing transverse momentum:
\begin{equation}
m_{\rm T}^W=\sqrt{2\pt^\ell\et^{\rm miss}(1-\cos(\phi^\ell-\phi^{\et^{\rm miss}}))}\,,
\end{equation} 
where $\pT^\ell$ and $\phi^\ell$ are the transverse momentum and azimuthal angle of a given lepton and $\phi^{\eT^{\rm miss}}$ is the azimuthal angle of the missing transverse momentum vector.

The discriminating variable used in this search is the transverse mass, \mT, defined as
\begin{equation}
 \mT = \sqrt{(\ET^{\ell\ell} + \met)^2 - |\vpTll + \vmet|^2}\,,
\end{equation}
  where
\begin{equation}
  \ET^{\ell\ell} = \sqrt{|\vpTll|^2 + m_{\ell\ell}^2}\,,
\end{equation}
and $\vpTll$ is the transverse momentum vector of the leading and subleading leptons.

The relative efficiencies of the additional cuts used to define the signal regions except for the pre-selection for the Higgs bosons with the NWA lineshape with mass values between 200\GeV and 4\TeV (3\TeV for VBF signal) are displayed in Figure~\ref{fig:EffNWA}.
The corresponding plots for the other signal models are shown in Figures~\ref{fig:EffLWA} and \ref{fig:EffOther}.
%\begin{figure}[htbp]
\begin{figure}[!htbp]
 \centering
 \includegraphics[width=0.48\linewidth]{fig/acceff/Eff-NWA-ggF.pdf}
 \includegraphics[width=0.48\linewidth]{fig/acceff/Eff-NWA-VBF.pdf}
 \caption{\small Efficiencies relative to the pre-selection efficiencies of those signal region additional cuts, defined on top of the pre-selection, as a function of heavy Higgs boson mass for ggF (left) and VBF (right) signals with the NWA lineshape. The legend e.g.\ ``ggF @ VBF 1J SR'' means the efficiency of the ggF signals in the VBF $N_{\text{jet}}=1$ category.}
 \label{fig:EffNWA}
\end{figure}

%\begin{figure}[htbp]
\begin{figure}[!htbp]
 \centering
 \includegraphics[width=0.49\linewidth]{fig/acceff/Eff-LWA-ggF.pdf}
 \includegraphics[width=0.49\linewidth]{fig/acceff/Eff-LWA-VBF.pdf}
 \caption{\small Efficiencies relative to the pre-selection efficiencies of those signal region additional cuts, defined on top of the pre-selection, as a function of heavy Higgs boson mass for the LWA signal model (width: 15\% of $m_{H}$) with both ggF (left) and VBF (right) production modes. The legend e.g.\ ``ggF @ VBF 1J SR'' means the efficiency of the ggF signals in the VBF $N_{\text{jet}}=1$ category.}
 \label{fig:EffLWA}
\end{figure}
\FloatBarrier

%\begin{figure}[htbp]
\begin{figure}[!htbp]
 \centering
 \includegraphics[width=0.49\linewidth]{fig/acceff/Eff-RS.pdf}
 \includegraphics[width=0.49\linewidth]{fig/acceff/Eff-HVT-qqA.pdf}
 \includegraphics[width=0.49\linewidth]{fig/acceff/Eff-HVT-VBF.pdf}
 \includegraphics[width=0.49\linewidth]{fig/acceff/Eff-GM-VBF.pdf}
 \includegraphics[width=0.49\linewidth]{fig/acceff/Eff-Spin2-VBF.pdf}
 \caption{\small Efficiencies relative to the pre-selection efficiencies of those signal region additional cuts, defined on top of the pre-selection, as a function of heavy resonance mass for the exotic signal models: RS with $k/\bar{M}_\text{Pl}=1$ (top left), HVT qqA (top right) and VBF (middle left), GM VBF (middle right) and Spin-2 VBF (bottom). The legend e.g.\ ``qqA @ VBF 1J SR'' means the efficiency of the qqA signals in the VBF $N_{\text{jet}}=1$ category.}
 \label{fig:EffOther}
\end{figure}

The geometrical acceptance times event selection efficiency, namely the overall selection efficiency including the pre-selection and additional cuts in the SRs for the NWA signal model is displayed in Figure~\ref{fig:AccNWA}.
The corresponding plots for the other signal models are shown in Figures~\ref{fig:AccLWA} and \ref{fig:AccOther}.
For samples with lower resonance masses, the acceptance times efficiency is lower because the leptons are softer (smaller \pT and $m_{\ell\ell}$).
This is due to the fact that the same selection is applied to all signal models and mass points in this analysis.
This is also the reason why the search is limited to signal mass values greater than about 200GeV. 
Therefore, the different selection efficiencies between the models are as expected which is mainly due to different $|\Delta\eta_{\ell\ell}|$ distributions for the different spin states.
%\begin{figure}[htbp]
\begin{figure}[!htbp]
 \centering
 \includegraphics[width=0.48\linewidth]{fig/acceff/Acc-NWA-ggF.pdf}
 \includegraphics[width=0.48\linewidth]{fig/acceff/Acc-NWA-VBF.pdf}
 \caption{\small Geometrical acceptance times selection efficiency (shortened as ``Signal acceptance'' in the plots) as a function of heavy Higgs boson mass for ggF (left) and VBF (right) signals with the NWA lineshape. The legend e.g.\ ``ggF @ VBF 1J SR'' means the efficiency of the ggF signals in the VBF $N_{\text{jet}}=1$ category, and ``overall'' means that the efficiencies of a signal sample to all the three signal event categories are combined.}
 \label{fig:AccNWA}
\end{figure}

%\begin{figure}[htbp]
\begin{figure}[!htbp]
 \centering
 \includegraphics[width=0.48\linewidth]{fig/acceff/Acc-LWA-ggF.pdf}
 \includegraphics[width=0.48\linewidth]{fig/acceff/Acc-LWA-VBF.pdf}
 \caption{\small Geometrical acceptance times selection efficiency (shortened as ``Signal acceptance'' in the plots) as a function of heavy Higgs boson mass for the LWA signal model (width: 15\% of $m_{H}$) with both ggF (left) and VBF (right) production modes. The legend e.g.\ ``ggF @ VBF 1J SR'' means the efficiency of the ggF signals in the VBF $N_{\text{jet}}=1$ category, and ``overall'' means that the efficiencies of a signal sample to all the three signal event categories are combined.}
 \label{fig:AccLWA}
\end{figure}

%\begin{figure}[htbp]
\begin{figure}[!htbp]
 \centering
 \includegraphics[width=0.48\linewidth]{fig/acceff/Acc-RS.pdf}
 \includegraphics[width=0.48\linewidth]{fig/acceff/Acc-HVT-qqA.pdf}
 \includegraphics[width=0.48\linewidth]{fig/acceff/Acc-HVT-VBF.pdf}
 \includegraphics[width=0.48\linewidth]{fig/acceff/Acc-GM-VBF.pdf}
 \includegraphics[width=0.48\linewidth]{fig/acceff/Acc-Spin2-VBF.pdf}
 \caption{\small Geometrical acceptance times selection efficiency (shortened as ``Signal acceptance'' in the plots) as a function of heavy resonance mass for the exotic signal models: RS with $k/\bar{M}_\text{Pl}=1$ (top left), HVT qqA (top right) and VBF (middle left), GM VBF (middle right) and Spin-2 VBF (bottom). The legend e.g.\ ``qqA @ VBF 1J SR'' means the efficiency of the qqA signals in the VBF $N_{\text{jet}}=1$ category, and ``overall'' means that the efficiencies of a signal sample to all the three signal event categories are combined.}
 \label{fig:AccOther}
\end{figure}
\FloatBarrier
