%% Event selection optimisation strategy

\section{Optimisation Strategy}
\label{sec:sel:opt}

A simple strategy was developed to optimise event selections to improve the sensitivity for signals for this analysis. 
However, the strategy is general and can be easily applied to all other analyses both for new physics searches and precision measurements.
In this section, the strategy will be discussed in detail, followed by two examples showing how it can be applied successfully to analysis.

%% Introduction

\subsection{Introduction}
\label{sec:sel:opt:intro}

Optimisation of event selection is an important step in all data analyses aiming for precision measurements or searching for new physics. One uses often different strategies in performing the optimisation. Two main categories are cut-based selections and MVA. Both have their own advantages but one usually lacks a means to tell if one uses more variables than one needs or if the cuts are really optimal or not. More variables one uses, larger systematic uncertainties one may introduce.

In this section, a combined strategy of MVA-based variable selections and cut-based scans is proposed. It has two main steps:
\begin{enumerate}
\item In the first step, a few discriminant variables from both a signal sample and a background sample containing all background processes are selected as inputs to a multivariate algorithm. Two output informations are used: the ranking of the variables and correlation matrices of the signal and background samples. Variables having high ranking values are selected. In the examples that are shown in this section, variables having a relative ranking value below 40\% of the highest one are removed. Among the selected high ranking variables, the correlation is then checked. If two variables are highly correlated (e.g.\ greater than 80\%) for both the signal and the background, the lower ranking variable is further removed. The remaining selected variables are kept for the next step.
\item In the second step, a signal significance is defined. Depending on the analysis, e.g.\ for an event counting experiment, one could define a global significance based on~\cite{asymptotics}
\begin{equation}
s=\sqrt{2\left[(n_{\rm S}+n_{\rm B})\ln\left(1+\frac{n_{\rm S}}{n_{\rm B}}\right)-n_{\rm S}\right]}
\label{eq:sgn}
\end{equation}
where $n_{\rm S}$ and $n_{\rm B}$ are the number of signal and background events, respectively. If the background sample is large or if the signal is widely distributed, one could extend the significance definition more differentially over a distribution which has different shapes between the signal and the background as
\begin{equation}
s_{\text{1-d}}=\sqrt{\sum_i s_i^2}
\label{eq:sgn1d}
\end{equation}
where subscript 1-d refers to the one-dimensional distribution and $s_i$ stands for the significance value of the $i$-th bin calculated with the same formula (\ref{eq:sgn}). Extension in $n$ dimensions with $n$ different distributions is simply:
\begin{equation}
s_{\text{n-d}}=\sqrt{\sum_{j=1}^n \sum_i \left(s_i^j\right)^2}\,.
\label{eq:sgnnd}
\end{equation}
Then a significance scan with different cuts for each of the selected variables is performed. The cut value corresponding to the maximum significance is chosen as an initial cut value of the variable. If the significance has a flat plateau, the cut value which gives a larger signal efficiency (or a smaller background efficiency) is favourably selected. These initial cut values are then checked in a new scan by applying all the other cut values except for the one on the variable under consideration. If the new cut values are different from those of the initial scans, the iteration continues. However given the fact that the highly correlated variables are removed in the first step, only one iteration is usually sufficient to define the optimal cuts.  For a given variable, if the significance maximum corresponds to the location where no cut is applied, then this variable is further removed from the list.
\end{enumerate}

In Sections~\ref{sec:sel:opt:hm} and \ref{sec:sel:opt:sm}, two example applications are shown, followed by a summary in Section~\ref{sec:sel:opt:sum}.

%% Application to HM

\subsection{Application to heavy Higgs boson search in the $H\rightarrow WW\rightarrow \ell\nu\ell\nu$ channel}
\label{sec:sel:opt:hm}

Following \cite{Aad:2015agg}, the following nine variables are used as inputs to an MVA training: $p_{\rm T}^{\rm lead}$, $p_{\rm T}^{\rm sublead}$, $p_{\rm T}^{\ell\ell}$, $m_{\ell\ell}$,  $\Delta\eta_{\ell\ell}$, $p_{\rm T}^{\rm miss}$, $p_{{\rm T},{\rm rel}}^{\rm miss}$, $E_{\rm T}^{\rm miss}$, and $E_{{\rm T},{\rm rel}}^{\rm miss}$ corresponding to the transverse momenta of the leading and subleading leptons and of the dilepton system, its invariant mass, the pseudorapidity difference between the two leptons,  as well as various track or calorimeter based missing transverse momenta or energies. 
The same MC samples as the Run-1 publication~\cite{Aad:2015agg} are used. We take the $e\nu\mu\nu$ channel and the zero-jet category as an example.

The ranking of the variables based on a Boosted Decision Tree (BDT) training is shown in Figure~\ref{fig:rank_hm} as a function of heavy Higgs boson mass.
Some variation from one mass point to another is observed due to small difference between some of the variables. To avoid such a variation, an average is performed over all the mass points and the resulting absolute and relative ranking values are shown in Table~\ref{tab:rank_hm}. Among the nine variables, three of them in the last three columns have a relative ranking value below 40\% and are therefore removed.
%\begin{figure}[htb]
\begin{figure}[!htbp]
\centering
\includegraphics[width=.9\textwidth]{fig/selopt/fig1_rank_hm}
\caption{Ranking of selected discriminant variables as function of heavy Higgs boson mass in the $H\to WW\to \ell\nu\ell\nu$ search channel.} \label{fig:rank_hm}
\end{figure}

%\begin{table}[htbp]
\begin{table}[!htbp]
  \caption{Absolute and relative ranking order averaged over heavy Higgs boson mass points between 600 and 1\,000\GeV in the $H\to WW\to \ell\nu\ell\nu$ channel.}\label{tab:rank_hm}
  \vspace{0.4cm}
  \centering
  \resizebox{\textwidth}{!}{
  \begin{tabular}{c|cccccc|ccc}
   \hline
Variable & $m_{\ell\ell}$ & $\Delta\eta_{\ell\ell}$ & $p_{\rm T}^{\rm lead}$ & $p_{\rm T}^{\rm sublead}$ & $p_{{\rm T},{\rm rel}}^{\rm miss}$ & $p_{\rm T}^{\rm miss}$ & $E_{\rm T}^{\rm miss}$ & $p_{\rm T}^{\ell\ell}$ & $E_{{\rm T},{\rm rel}}^{\rm miss}$ \\\hline
Absolute & 0.225 & 0.118 & 0.110 & 0.107 & 0.107 & 0.096 & 0.084 & 0.079 & 0.074 \\
Relative (\%) & 100 & 53 & 49 & 48 & 47 & 43 & 37 & 35 & 33 \\\hline
  \end{tabular}%
  }
\end{table}

Checking now the correlation between the variables shown in Figure~\ref{fig:corr_hm} which have also been averaged over the mass points, among the six remaining variables, variable $p_{\rm T}^{\rm sublead}$ has a correlation coefficient of 83\% for the signal and 80\% for the background with $m_{\ell\ell}$ which meets the 80\% rejection requirement, the lower ranking variable $p_{\rm T}^{\rm sublead}$ is thus removed. 
The other correlated variables have already been removed with the lower ranking requirement (40\%). This concludes the first step of the strategy with five selected variables:  
$m_{\ell\ell}$, $\Delta\eta_{\ell\ell}$, $p_{\rm T}^{\rm lead}$, $p_{{\rm T},{\rm rel}}^{\rm miss}$ and $p_{\rm T}^{\rm miss}$.
%\begin{figure}[htb]
\begin{figure}[!htbp]
\centering
\includegraphics[width=.49\textwidth]{fig/selopt/fig2_corr_sig_hm}
\includegraphics[width=.49\textwidth]{fig/selopt/fig2_corr_bkg_hm}
\caption{Correlation matrices between variables for signal (left) and background (right). For the signal, averaged over the mass values between 600\GeV and 1\,000\GeV.}\label{fig:corr_hm}
\end{figure}

A significance scan is then performed. The results for a heavy Higgs boson at 600\GeV are shown in Figure~\ref{fig:scan_hm}, where the four curves in each panel correspond to the signal efficiency (blue), the background efficiency (black), the global significance $s$ (magenta) and the 
$m_{\rm T}$-based 1-d significance $s_{\text{1-d}}[m_{\rm T}]$ (red).  For an easy display, the significance curves have been scaled with a scale factor indicated on the panel. It is interesting to note that the 1-d curve $s_{\text{1-d}}[m_{\rm T}]$ is  significantly higher than the global one $s$. 
The gain using more differential bins is expected. 
Based on $s_{\text{1-d}}[m_{\rm T}]$, the following cuts are chosen as an initial set of the cuts: $m_{\ell\ell}>100$\GeV\footnote{For a heavy Higgs boson of 600\GeV and up to 1\,000\GeV, a more stringent cut could be selected. On the other hand, Higgs bosons at lower masses need a lower cut on $m_{\ell\ell}$. So this cut is a kind of compromise.}, $\Delta\eta_{\ell\ell}<1.8$, $p_{\rm T}^{\rm lead}>120$\GeV and $p_{\rm T}^{\rm miss}>40$\GeV. 
These cuts correspond to 
$s_{\text{1-d}}[m_{\rm T}]$ being on the plateau. Within a same plateau, the cut value corresponding to higher signal efficiency is preferably selected. 
The background rejection is of course another consideration.
No cut is selected for $p_{{\rm T},{\rm rel}}^{\rm miss}$ as any cut would decrease the significance value of $s_{\text{1-d}}[m_{\rm T}]$. 

A new scan is repeated by applying all these initial cuts except for the one on the variable under scan (see Figure~\ref{fig:scan_it2_hm}). It is found that no change is needed for any of these initial cut values. 
This implies that a simultaneous scan in multi-dimensions is not needed. In fact the latter scan is not preferable as it either can be time consuming or may not lead to a unique set of cuts given that the variation of the significance on some of the variable is fairly weak as one can see from Figure~\ref{fig:scan_hm}. The individual scan on the other hand allows one to better appreciate the dependence of the significance which differs from one variable to another. 

%\begin{figure}[htbp]
\begin{figure}[!htbp]
\centering
\includegraphics[width=.49\textwidth]{fig/selopt/fig3a_hm}
\includegraphics[width=.49\textwidth]{fig/selopt/fig3b_hm}
\includegraphics[width=.49\textwidth]{fig/selopt/fig3c_hm}
\includegraphics[width=.49\textwidth]{fig/selopt/fig3e_hm}
\includegraphics[width=.49\textwidth]{fig/selopt/fig3d_hm}
\caption{\small Significance scan for five variables, $m_{\ell\ell}$ (Mll), $\Delta\eta_{\ell\ell}$ (DEtall), $p_{\rm T}^{\rm lead}$ (LeadPt), $p_{\rm T}^{\rm miss}$ 
(MPT) and 
$p_{{\rm T},{\rm rel}}^{\rm miss}$ 
(MPTRel), selected from the first step for a heavy Higgs boson at 600\GeV in the $H\to WW\to e\mu + \mu e$ channel in the zero-jet category. The four curves correspond to the efficiency of the signal (Eff H600), the efficiency of the background (Eff bkg), 
the $m_{\rm T}$-based significance (RSS H600) and the global significance (Sgn H600). 
The number in the brackets corresponds to the scale factor applied to the significance curves for an easy display.}\label{fig:scan_hm}
\end{figure}

%\begin{figure}[htbp]
\begin{figure}[!htbp]
\centering
\includegraphics[width=.49\textwidth]{fig/selopt/fig_ap0_mll_it2_hm}
\includegraphics[width=.49\textwidth]{fig/selopt/fig_ap0_detall_it2_hm}
\includegraphics[width=.49\textwidth]{fig/selopt/fig_ap0_lead_it2_hm}
\includegraphics[width=.49\textwidth]{fig/selopt/fig_ap0_mpt_it2_hm}
\includegraphics[width=.49\textwidth]{fig/selopt/fig_ap0_mptrel_it2_hm}
\caption{Same as Figure~\ref{fig:scan_hm} except that here all initial cuts have been applied except for that one on the variable under consideration.} \label{fig:scan_it2_hm}
\end{figure}

When selecting these cuts, it has been checked (by comparing Figures~\ref{fig:scan_1tev} and \ref{fig:scan_1j} with Figure~\ref{fig:scan_hm}) that they apply also to a high mass signal up to 1\,000\GeV as well as to the 1-jet channel. 

%\begin{figure}[htbp]
\begin{figure}[!htbp]
\centering
\includegraphics[width=.49\textwidth]{fig/selopt/fig_ap1a_1tev}
\includegraphics[width=.49\textwidth]{fig/selopt/fig_ap1b_1tev}
\includegraphics[width=.49\textwidth]{fig/selopt/fig_ap1c_1tev}
\includegraphics[width=.49\textwidth]{fig/selopt/fig_ap1e_1tev}
\includegraphics[width=.49\textwidth]{fig/selopt/fig_ap1d_1tev}
\caption{Same as Figure~\ref{fig:scan_hm} except that here the signal is at 1\,TeV instead of 600\,GeV.}\label{fig:scan_1tev}
\end{figure}

%\begin{figure}[htbp]
\begin{figure}[!htbp]
\centering
\includegraphics[width=.49\textwidth]{fig/selopt/fig_ap2a_1j}
\includegraphics[width=.49\textwidth]{fig/selopt/fig_ap2b_1j}
\includegraphics[width=.49\textwidth]{fig/selopt/fig_ap2c_1j}
\includegraphics[width=.49\textwidth]{fig/selopt/fig_ap2d_1j}
\caption{Same as Figure~\ref{fig:scan_hm} except that this is for 1-jet channel instead of zero-jet channel.}\label{fig:scan_1j}
\end{figure}

These new cuts are finally compared with those used in the Run-1 publication~\cite{Aad:2015agg} in Table~\ref{tab:cuts_hm}. Both the signal significance value 
($s_{\text{1-d}}[m_{\rm T}]$) and the expected limit at 95\% CL on the signal strength are improved with the newly optimised cuts for a heavy Higgs boson at 600\GeV. One also observes that the $p_{\rm T}^{\ell\ell}$ cut and a strict cut on $p_{\rm T}^{\rm sublead}$ are not needed with this optimisation strategy.
%\begin{table}[htb]
\begin{table}[!htbp]
  \caption{Comparison of the new optimal cuts with those used in the Run-1 publication~\cite{Aad:2015agg}. The comparison for the significance values and expected 95\% CL limits on the signal strength $\mu$ is given for $m_H=600$\GeV.}\label{tab:cuts_hm}
  \vspace{0.4cm}
  \centering
  \begin{tabular}{c|c|c}
   \hline
Comparison & New & Publication \\\hline
\multirow{2}{*}{Common pre-selection} & \multicolumn{2}{c}{$p_{\rm T}^{\rm lead}>22$\GeV}\\
  & \multicolumn{2}{c}{$p_{\rm T}^{\rm sublead}>15$\GeV}\\\hline
\multirow{6}{*}{Additional cuts} & $p_{\rm T}^{\rm lead}>120$\GeV & $p_{\rm T}^{\rm lead}>60$\GeV\\
 &  & $p_{\rm T}^{\rm sublead}>30$\GeV \\
 & $p_{\rm T}^{\rm miss}>40$\GeV & $p_{\rm T}^{\rm miss}>45$\GeV \\
 &  & $p_{\rm T}^{\ell\ell}>60$\GeV \\
 & $m_{\ell\ell}>100$\GeV & $m_{\ell\ell}>60$\GeV\\
 & $\Delta\eta_{\ell\ell}<1.8$ & $\Delta\eta_{\ell\ell}<1.35$ \\\hline
 Significance ($s_{\text{1-d}}[m_{\rm T}]$) & $1.37\pm 0.02$ & $1.24\pm 0.03$ \\\hline
 Limit on $\mu$ (95\% CL) & 1.54 & 1.70 \\\hline
  \end{tabular}%
\end{table}

A similar comparison as a function of heavy Higgs boson mass $m_H$ is shown in Figure~\ref{fig:s1d_mh}. The improvement is observed over all the mass points considered.
%\begin{figure}[htbp]
\begin{figure}[!htbp]
\centering
\includegraphics[width=0.8\textwidth]{fig/selopt/RSS_emme_0j}
\caption{Comparison of the signal significance $s_{\text{1-d}}[m_{\rm T}]$ (RSS) between the new and published selection is shown as a function of heavy Higgs boson mass $m_H$.}
\label{fig:s1d_mh}
\end{figure}

%% Application to SM

\subsection{Application to SM Higgs boson discovery analysis in the $H\rightarrow WW^{\ast}\rightarrow \ell\nu\ell\nu$ channel}
\label{sec:sel:opt:sm}

The same strategy is also applied to and compared with the low-mass analysis which has previously allowed us to observe the Higgs boson in the $h\to WW^\ast$ channel alone~\cite{ATLAS:2014aga}. For the 0 and 1-jet channels of this analysis, the following variables have been used in the event selection: 
$p_{\rm T}^{\rm lead}$, $p_{\rm T}^{\rm sublead}$, $m_{\ell\ell}$, $\Delta\phi_{\ell\ell}$, $p_{\rm T}^{\rm miss}$, $p_{\rm T}^{\ell\ell}$ and $\Delta\phi_{\ell\ell, {\rm MET}}$. 

In the first step, the ranking order obtained from a BDT training is shown in Table~\ref{tab:rank_lm}. 
Among the seven variables, $\Delta\phi_{\ell\ell, {\rm MET}}$ has a low ranking value below 40\% and is removed from the list. 
In addition, $p_{\rm T}^{\ell\ell}$ is also removed as it has a strong correlation with $p_{\rm T}^{\rm miss}$ (Figure~\ref{fig:corr_lm}) but a lower ranking value ({\rm T}able~\ref{tab:rank_lm}). {\rm T}herefore after the first step, only five variables are selected: $m_{\ell\ell}$, $\Delta\phi_{\ell\ell}$, $p_{\rm T}^{\rm sublead}$, $p_{\rm T}^{\rm lead}$ and $p_{\rm T}^{\rm miss}$.
%\begin{table}[htb]
\begin{table}[!htbp]
  \caption{Absolute and relative ranking order for the SM Higgs boson at 125\GeV in the $H\to WW^\ast\to \ell\nu\ell\nu$ channel.}\label{tab:rank_lm}
  \vspace{0.4cm}
  \centering
  %\resizebox{\textwidth}{!}{
  \begin{tabular}{c|cccccc|c}
   \hline
Variable & $m_{\ell\ell}$ & $\Delta\phi_{\ell\ell}$ & $p_{\rm T}^{\rm sublead}$ & $p_{\rm T}^{\rm lead}$ & $p_{\rm T}^{\rm miss}$ & $p_{\rm T}^{\ell\ell}$ & $\Delta\phi_{\ell\ell,{\rm MET}}$ \\\hline
Absolute & 0.217 & 0.197 & 0.153 & 0.146 & 0.128 & 0.102 & 0.0058\\
Relative (\%) & 100 & 91 & 70 & 67 & 59 & 47 & 27 \\\hline
  \end{tabular}%
  %}
\end{table}

%\begin{figure}[htbp]
\begin{figure}[!htbp]
\centering
\includegraphics[width=.49\textwidth]{fig/selopt/fig4_sig_lm}
\includegraphics[width=.49\textwidth]{fig/selopt/fig4_bkg_lm}
\caption{Correlation matrices between variables for signal (left) and background (right) in the SM Higgs boson analysis.}\label{fig:corr_lm}
\end{figure}

In the second step, a significance scan is performed. Three significance quantities are defined, the global one $s$, 
the $m_{\rm T}$-based one $s_{\text{1-d}}[m_{\rm T}]$ and the two-dimensional one in $p_{\rm T}^{\rm sublead}$ and $m_{\rm T}$ $s_{\text{2-d}}[p_{\rm T}^{\rm sublead}, m_{\rm T}]$. 
The significance results together with the signal and background efficiencies are shown in Figure~\ref{fig:scan_lm}. 
As expected, the 2-d significance curve $s_{\text{2-d}}[p_{\rm T}^{\rm sublead}, m_{\rm T}]$ is systematically higher than the 1-d curve $s_{\text{1-d}}[m_{\rm T}]$ but the gain is smaller from 1-d to 2-d than from the global one to 1-d. Based on $s_{\text{2-d}}[p_{\rm T}^{\rm sublead}, m_{\rm T}]$, the initial cuts are defined as: $m_{\ell\ell}<45$\GeV, $\Delta\phi_{\ell\ell}<2$ and $p_{\rm T}^{\rm miss}>35$\GeV. No strict cuts on $p_{\rm T}^{\rm lead}$ and $p_{\rm T}^{\rm sublead}$ are needed, so the cuts at the pre-selection level corresponding to the starting point of the scan are kept.
%\begin{figure}[htbp]
\begin{figure}[!htbp]
\centering
\includegraphics[width=.49\textwidth]{fig/selopt/fig5_mll_lm}
\includegraphics[width=.49\textwidth]{fig/selopt/fig5_dphill_lm}
\includegraphics[width=.49\textwidth]{fig/selopt/fig5_mpt_lm}
\includegraphics[width=.49\textwidth]{fig/selopt/fig5_lead_lm}
\includegraphics[width=.49\textwidth]{fig/selopt/fig5_sublead_lm}
\caption{\small Significance scan for five variables, $m_{\ell\ell}$ (Mll), $\Delta\phi_{\ell\ell}$ (DPhill), $p_{\rm T}^{\rm miss}$ (MPT), $p_{\rm T}^{\rm lead}$ (LeadPt) and $p_{\rm T}^{\rm sublead}$ (SubPt), selected from the first step for the SM Higgs boson at 125\GeV in the $h\to WW^\ast\to e\mu + \mu e$ channel in the zero-jet category. The four curves correspond to the efficiency of the signal (Eff H125), the efficiency of the background (Eff bkg), the $p_{\rm T}^{\rm sublead}$ and $m_{\rm T}$-based significance (RSS(SubPt, MT) H125), the $m_{\rm T}$-based significance (RSS(MT) H125) and the global significance (Sgn H125). The number in the brackets corresponds to the scale factor applied to the significance curves for an easy display.}\label{fig:scan_lm}
\end{figure}

A new scan is then performed by applying all the other cuts except for the one on the variable under the new scan (the results are shown in Figure~\ref{fig:scan_it2_lm}). It turns out that a minor adjustment is needed for $m_{\ell\ell}$ from 45\GeV to 50 or 55\GeV and for $p_{\rm T}^{\rm miss}$ from 35\GeV to 30\GeV. Further iteration shows that these cuts are stable. 
The final cuts are compared~\footnote{It should be noted that in the Run-1 publication~\cite{ATLAS:2014aga} a 3-d based likelihood analysis was used, it is expected that the corresponding signal significance is slightly better than the 2-d significance shown here. But the cuts that are proposed here should be fairly independent of the 3-d and 2-d significance scan since the difference between the 1-d and 2-d curves is small, which can be found in Figure~\ref{fig:scan_lm}.} with those used in the Run-1 publication~\cite{ATLAS:2014aga} in Table~\ref{tab:cuts_lm}.
%\begin{figure}[htbp]
\begin{figure}[!htbp]
\centering
\includegraphics[width=.49\textwidth]{fig/selopt/fig_ap3_mll_lm}
\includegraphics[width=.49\textwidth]{fig/selopt/fig_ap3_dphill_lm}
\includegraphics[width=.49\textwidth]{fig/selopt/fig_ap3_mpt_lm}
\caption{Same as Figure~\ref{fig:scan_lm} except that here all initial cuts have been applied except for that one on the variable under consideration.}\label{fig:scan_it2_lm}
\end{figure}

%\begin{table}[htb]
\begin{table}[!htbp]
  \caption{Comparison of the new optimal cuts with those used in the Run-1 publication~\cite{ATLAS:2014aga}. The comparison for the significance values 
  is given for $m_h=125$\GeV.}\label{tab:cuts_lm}
  \vspace{0.4cm}
  \centering
  \begin{tabular}{c|c|c}
   \hline
Comparison & New & Publication \\\hline
\multirow{2}{*}{Common pre-selection} & \multicolumn{2}{c}{$p_{\rm T}^{\rm lead}>22$\GeV}\\
  & \multicolumn{2}{c}{$p_{\rm T}^{\rm sublead}>10$\GeV}\\\hline
\multirow{5}{*}{Additional cuts} & $m_{\ell\ell}<55$\GeV & $m_{\ell\ell}<55$\GeV \\
 & $\Delta\phi_{\ell\ell}<2$ & $\Delta\phi_{\ell\ell}<1.8$ \\
 & $p_{\rm T}^{\rm miss}>30$\GeV & $p_{\rm T}^{\rm miss}>20$\GeV \\
 & & $p_{\rm T}^{\ell\ell}>30$\GeV \\
 & & $\Delta\phi_{\ell\ell, {\rm MET}}>1.55$ \\\hline
 Significance ($s_{\text{2-d}}[p_{\rm T}^{\rm sublead},m_{\rm T}]$) & $4.682\pm 0.011$ & $4.662\pm 0.012$ \\\hline
  \end{tabular}%
\end{table}

%% Summary

\subsection{Summary}
\label{sec:sel:opt:sum}

A simple strategy combining a selection of variables from the ranking order and the correlation matrices provided by a BDT training and a significance scan of the BDT selected variables is proposed to define optimal event selection for a data analysis. Two examples have been shown by applying the strategy. In both cases, improvement in signal significance is achieved with fewer variables than previously used. 

In comparison with a pure MVA analysis, the strategy allows one to use fewer discriminant variables to performed cut-based like analysis so that the data and MC agreement of the selected variables can be checked in detailed in particular around the cut value.

In comparison with a pure cut-based analysis, the use of the MVA training allows one to select only the most discriminant and less correlated variables.

This strategy can be easily extended to other analyses both for hunting for new physics and for performing precision measurements.
\FloatBarrier
