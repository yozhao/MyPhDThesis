\subsection {Calorimetry}
\label{sec:det:calo}

Calorimeters measure the energy a particle loses as it passes through the detector. They are usually designed to stop entire or ``absorb'' most of the particles coming from a collision, forcing them to deposit all of their energy within the detector. 
Typically calorimeters consist of layers of ``passive'' or ``absorbing'' high-density material --- for example, lead --- interleaved with layers of an ``active'' medium such as solid lead-glass or liquid argon.
In this sense, the ATLAS calorimetry system includes two types of calorimeters: the Liquid Argon (LAr) calorimeters and the Tile Hadronic calorimeters. 

In terms of functionality and performance, there are two basic calorimeter systems: an inner electromagnetic (EM) calorimeter and an outer hadron calorimeter. Both are sampling calorimeters\footnote{That is, they absorb energy in high-density metal and periodically sample the shape of the resulting particle shower, inferring the energy of the original particle from this measurement.}.
The EM calorimeter measures the energy of electrons and photons as they interact with matter electromagnetically. It has high precision in the measurement of energy deposition. The hadron calorimeter absorbs energy from  particles that pass through the EM calorimeter but interact via strong force. These particles are primarily hadrons. It is less precise than the EM calorimeter.

The LAr EM calorimeter with high granularity covers the pseudorapidity range $|\eta| < 3.2$. The hadron calorimetry in the range $|\eta| < 1.7$ is provided by a scintillator-tile calorimeter, which is separated into a large central barrel and two smaller extended barrels. In the end-caps ($|\eta| > 1.5$), LAr technology is also used for the hadron calorimeter, matching the outer $|\eta|$ limits of the EM calorimeter end-caps. The LAr forward calorimeters provide both electromagnetic and hadronic energy measurements, and extend the pseudorapidity coverage to $|\eta| = 4.9$.
An overall view of the calorimeters is depicted in Figure~\ref{fig:cal}. 

\begin{figure}[htbp]
 \centering
 \includegraphics[width=0.9\textwidth]{fig/lhc/calo.jpg}
 \caption{Cut-away view of the calorimetry system.}
 \label{fig:cal}
\end{figure}

%% EM calo

\subsubsection {Electromagnetic calorimeter}
\label{sec:det:calo:em}

The EM calorimeter~\cite{Aad:2014nim} is divided into a barrel section (EMB) with a coverage of $|\eta| < 1.475$ and two end-cap sections (EMEC) covering the pseudorapidity region $1.375 < |\eta| < 3.2$. 
Each section is housed in its own cryostat.
The barrel and end-cap parts are divided into 16 and 8 modules, respectively, in $\phi$.
The region between the EMB and the EMEC, covering $1.37 < |\eta| < 1.52$, is called the transition region. 
%The transition region contains a large amount of material in front of the first active calorimeter layer ranging from 5 to almost 10 radiation lengths ($X_0$).
A high voltage (HV) system is used to generate an electric field of approximately 1\,kV/mm, allowing ionisation electrons to drift in the LAr gap.
In the EMB, the value of the HV is a constant along $\eta$, and in the EMEC, the gap varies continuously with radius, thus it is adjusted in steps along $\eta$.
%The barrel consists of two identical half-barrels, separated by a small gap (4\,mm) at $z$ = 0. Each end-cap is mechanically divided into two coaxial wheels: an outer wheel covering the region $1.375 < |\eta| < 2.5$, and an inner wheel covering the region $2.5 < |\eta| < 3.2$. 

The barrel has accordion-shaped electrodes and energy-absorbing materials are lead and stainless steel, with liquid argon as the sampling materials, and a cryostat is required around the calorimeter to keep it sufficiently cool.
The accordion geometry (see Figure~\ref{fig:accord}) provides complete $\phi$ symmetry without azimuthal cracks and fast extraction of the signal at the rear or at the front of the electrodes. 

\begin{figure}[htbp]
 \centering
 \includegraphics[width=1.0\textwidth]{fig/lhc/accord.png}
 \caption{Sketch of a barrel module where the different layers are clearly visible with the ganging of electrodes in $\phi$. The granularity in $\eta$ and $\phi$ of the cells of each of the three layers and of the trigger towers is also shown.}
 \label{fig:accord}
\end{figure}

Both the barrel and end-cap calorimeters are segmented longitudinally into three layers in depth of shower within $|\eta| < 2.5$.
The first layer (L1), covering $|\eta| < 1.4$ and $1.5 < |\eta| < 2.4$, is composed of high-granularity strips, which have typically, e.g. $0.003\times 0.1$ in $\Delta\eta\times\Delta\phi$ in EMB. Such a design makes it sufficient to provide an event-by-event discrimination between single photon showers and overlapped showers from the decay of neutral hadrons from jets.
The second layer (L2), being used to collect most of the deposited energy in the calorimeters from electron and photon showers, has a granularity of $0.025\times 0.025$ in $\Delta\eta\times\Delta\phi$. 
The third layer (L3), with a granularity of $0.05\times 0.025$ in $\Delta\eta\times\Delta\phi$, is designed mainly to correct the leakage beyond the EM calorimeters for high-energy showers.
There is also a thin pre-sampler layer(PS), which has a coverage of $|\eta| < 1.8$ and granularity of $0.025\times 0.1$ in $\Delta\eta\times\Delta\phi$ and is used to correct the energy loss.


Electrons and photons that enter the LAr calorimeters will interact with the lead absorbers and build EM showers which then ionise the LAr in the gaps between the absorbers.
%>
The ionisation electrons drift and induce an electrical signal on the electrodes that is proportional to the energy that is deposited in the active volume of the calorimeters.
The signal is brought through cables to the read-out Front End Boards, where it is first amplified by a current-sensitive pre-amplifier.

The total energy deposited in an EM calorimeter cell can be reconstructed as follows:
\begin{equation}
\begin{aligned}
E_{cell} = F_{\rm \mu A\to MeV} {}& \times F_{\rm DAC\to\mu A} \\
& \times \frac{1}{\frac{M_{\rm phys}}{M_{\rm cali}}} \times G \times \sum\limits_{j=1}^{\rm N_{samples}}a_{j}(s_{j}-p) \,,
\end{aligned}
\end{equation}
where $s_j$ are the samples of the shaped ionisation signal digitised in the second electronic gain;
$p$ is the read-out electronic pedestal, measured for each gain in dedicated calibration runs;
the $a_j$ weights are the optimal filtering coefficients (OFC) derived from the predicted shape of the ionisation pulse and the noise auto-correction, accounting for both the electronic and the pile-up components.
The cell gain $G$ is computed by injecting a known calibration signal and reconstructing the corresponding cell response.
The factor $\frac{M_{\rm phys}}{M_{\rm cali}}$, which quantifies the ratio of the maxima of the physical and calibration pulses corresponding to the same input current, corrects the gain factor $G$ that is obtained with the calibration pulses to adapt it to physics-induced signals;
the factor $F_{\rm DAC\to\mu A}$ converts digital-to-analog converter (DAC) counts set on the calibration board to a current in $\mu A$;
the factor $F_{\rm \mu A\to MeV}$ converts the ionisation current to the total deposited energy at the EM scale and is determined from test-beam studies.
%<


%Over the region devoted to precision physics ($|\eta| < 2.5$), the EM calorimeter is segmented in three section in depth. For the end-cap inner wheel, the calorimeter is segmented in two section in depth and has a coarser lateral granularity than for the rest of the acceptance.
%In the region of $|\eta| < 1.8$, a presampler detector is used to correct for the energy lost by electrons and photons upstream of the calorimeter. The presampler consists of an active LAr layer in the barrel and end-cap region.

%The position of the inner solenoid in front of the EM calorimeter requires optimisation of the material in order to achieve the desired calorimeter performance. As a consequence, the inner solenoid and the LAr calorimeter share a common vaccum vessel, thereby eliminating two vacuum walls. 

%In the barrel, the accordion waves are axial and run in $\phi$, and the folding angles of the waves vary with radius to keep the liquid-argon gap constant. In the end-caps, the waves are parallel to the radial direction and run axially. Since the liquid-argon gap increases with radius in the end-caps, the wave amplitude and the folding angle of the absorbers and electrodes vary with radius. All these features of the accordion geometry lead to a very uniform performance in terms of linearity and resolution as a function of $\phi$.

%% Hadronic calo

\subsubsection{Hadron calorimeter}

The energy-absorbing material is steel for the hadron calorimeter, while scintillator tiles are used to sample the deposited energy.
%Many of the features of the calorimeter are chosen for their cost-effectiveness. 
The main part of the hadron calorimeter --- the tile calorimeter --- is placed directly outside the EM calorimeter envelope and is composed of three parts: 
one central barrel ($|\eta| < 1.0$) and two extended barrels ($0.8 < |\eta| < 1.7$).
The hadron calorimeter is extended to larger pseudorapidities ($|\eta| < 4.9$) by the Hadron End-cap Calorimeter (HEC), a copper/liquid-argon detector and the Forward Calorimeter (FCal), a copper-tungsten/liquid-argon detector.

%The Hadronic End-cap Calorimeter (HEC) consists of two independent wheels per end-cap, located directly behind the end-cap electromagnetic calorimeter and sharing the same LAr cryostats. To reduce the drop in material density at the transition between the end-cap and the forward calorimeter (around $|\eta| = 3.1$), the HEC extends out to $|\eta| = 3.2$, thereby overlapping with the forward calorimeter. Similarly, the HEC $\eta$ range also slightly overlaps that of the tile calorimeter ($|\eta| < 1.7$) by extending to $|\eta| = 1.5$.
%
%The Forward Calorimeter (FCal) is integrated into the end-cap cryostats, as this provides clear benefits in terms of uniformity of the calorimetric coverage as well as reduced radiation background levels in the muon spectrometer. In order to reduce the amount of neutron albedo in the inner detector cavity, the front face of the FCal is recessed by about 1.2\,m with respect to the EM calorimeter front face. This severely limits the depth of the calorimeter and therefore calls for a high-density design. The FCal is approximately 10 interaction lengths deep, and consists of three modules in each end-cap.
