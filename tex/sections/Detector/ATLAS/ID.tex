\subsection {Inner detector}
\label{sec:det:id}

%Aaboud:2017pjd

The ATLAS inner detector (ID) system, immersed in a 2\,T axial magnetic field, is used to provide measurements of trajectory reconstruction for charged particles, with a pseudorapidity coverage of $|\eta|<2.5$. A cut-away view of it is displayed in Figure~\ref{fig:id-trans}, showing mainly its geometric construction.
It consists of three different but complementary subsystems: a silicon pixel detector (pixel), a silicon micro-strip detector (SCT) and a transition-radiation straw-tube tracker (TRT). 
\begin{figure}[htbp]
 \centering
 \includegraphics[width=0.8\textwidth]{fig/lhc/id-trans.jpg}
 \caption{Cut-away view of the inner detector.}
 \label{fig:id-trans}
\end{figure}

The reconstruction of interaction vertices helps to identify the locations where particles interact with the material of the ID.
The largest source of secondary particles is from nulear interaction of primary particles with the ID material, where primary particles refer to particles that are promptly produced in the $pp$ collision, while secondary particles refer to those produced in the decays of primary particles or their interaction with detector material.
The ID is designed such that its material has a minimum effect on the particles traversing its volume.
The layout of the ID for Run 2 is shown in Figure~\ref{fig:id-long}.
\begin{figure}[htbp]
 \centering
 \includegraphics[width=1.0\textwidth]{fig/lhc/id-long.png}
 \caption{Plan view of a quarter-section of the inner detector showing each of the major detector elements with its active dimensions and envelopes. 
 The labels PP1, PPB1 and PPF1 indicate the patch-panels for the inner detector services.
 }
 \label{fig:id-long}
\end{figure}
During the LHC long shutdown in 2013--2014, between Run~\footnote{Run 1 refers to the period of the LHC data-taking in 2008--2012, while Run 2 refers to the period since 2015.} 1 and Run 2, the ID was upgraded with installing a new pixel-detector layer, referred to as the insertable $B$-layer (IBL), together with a new, thinner beam pipe which is used to minimise the distance of the IBL from the beam line.
In addition, the pixel detector has been extracted and renovated in the meanwhile.
The IBL helps to improve the track reconstruction performance, e.g. the resolution of the transverse and longitudinal impact parameters, $d_0$ and $z_0$, are improved by more than $40\%$ in the best case of tracks with $\pT$ around 0.5\GeV. The transverse impact parameter, $d_0$, is defined as the shortest distance between a track and the beam axis in the transverse plane. The longitudinal impact parameter, $z_0$, is defined as the distance in $z$-aixs between the primary vertex and the point on the track that is used to estimate $d_0$.

In addition to the IBL, the pixel detector has three other barrel layers (referred to as PIX1, PIX2 and PIX3 inside-out) and two end-caps each made of three disks.
It hosts 1744 pixel-sensor modules, each module containing 46080 pixels (there are over 80 million pixels in the ID in total).
An octagonal prism structure, referred to as the pixel support frame (PSF) is inserted inside the pixel support tube (PST) to support the barrel and end-cap layers of the pixel detector.
In the pixel detectors, the time over threshold (ToT) is used to measure the charge of tracks that is collected by each individual pixel~\cite{Aaboud:2017all}. ToT is defined to be the time when the pulse exceeds a given threshold and it is proportional to the deposited energy.
The SCT, shortened from a Semiconductor Tracker, is installed outside of the pixel volume and consists of 4088 silicon micro-strip modules, distributed in four barrel layers (referred to as SCT1, SCT2, SCT3 and SCT4 inside-out) and two end-caps, each having nine wheels.
Every module is made of two layers of silicon micro-strip detector sensors that are glued back-to-back.
The SCT sensors are not used to collect charge of the tracks.
The TRT is the outermost subsystem of the ID and it consists of more than 350,000 gas-filled straw tubes.
The reconstruction of tracks is properly extended radially up to a radius of 1082\,mm by the TRT.
The TRT straw tubes provide the raw timing information, which can be translated into calibrated drift circles that are used to match track candidates that are reconstructed from the SCT.
The structures of the SCT and TRT are the same with that used for Run 1.

%Three disks are also installed on each side of the end-cap region to improve the coverage of the pixel detectors.
%
%The barrel part of the SCT is installed outside of the pixel volume, and it is composed of four double strip layers with nine disks on each side of the end-caps. 
%On each layer, the strips are parallel to the beam direction on one side, and information from two sides of each layer is combined in order to provide an average of four three-dimensional measurements for every track.
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%The inner detector consists of three different but complementary subsystems of sensors parallel to the beam axis: Pixel Detector, Semiconductor Tracker (SCT), Transition Radiation Tracker (TRT).
%All the subsystems are immersed in a 2\,T magnetic field generated by the inner soleniod.
%The layout of the inner detector, as well as the coverage of the three subdetectors, is illustrated in Figure~\ref{fig:id-trans} and~\ref{fig:id-long}, as the former shows the geometric construction and the later reflects the performance requirements.
%
%The inner detector measures the direction, momentum, and charge of electrically-charged particles produced in each proton-proton collision.
%It is designed to provide hermetic and robust pattern recognition, excellent momentum resolution and both primary and secondary vertex measurements for charged tracks above a given \pT threshold 
%(nominally 0.5\GeV, but as low as 0.1\GeV in some ongoing studies of initial measurements with minimum-bias events) 
%and within the pseudorapidity range $|\eta| < 2.5$. 
%It also provides electron identification over $|\eta| < 2.0$ and a wide range of energies 
%(between 0.5\GeV and 150\GeV).

%The Insertable B-Layer (IBL)~\cite{ARosa:IBL}, as the innermost layer of pixel detectors, was installed in 2014 during the long shutdown between the LHC Run 1 and Run 2 data taking, and between the existing pixel detectors and a new smaller radius beam-pipe.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%The Pixel Detector has 80 million channels (1744 modules, 46080 readout channels per module).
%The SCT is a silicon microstrip tracker consisting of 4,088 two-sided modules and over 6 million implanted readout strips (6 million channels).
%The TRT has totally 350,000 readout channels, with straw tube being the basic detector element.
%The end-cap part is placed in the cryostat. The instrumentation housed inside the inner end-cap must be kept cool to avoid thermal noise. This cooling is achieved on ATLAS by placing the end-cap inside a liquid argon cryostat. The end-cap measures particles that are produced close to the direction of the beam pipe and would otherwise be missed.
%
%The magnetic field is generated by the central soleniod, which extends over a length of 5.3\,m with a diameter of 2.5\,m. The precision tracking detectors (pixel and SCT) cover the region $|\eta| < 2.5$. In the barrel region, they are arranged on concentric cylinders around the beam axis while in the end-cap regions they are located on disks perpendicular to the beam axis. The highest granularity is achieved around the vertex region using silicon pixel detectors. The pixel layers are segmented in $R - \phi$ and $z$ with typically three pixel layers crossed by each track. 
%All pixel sensors are identical. The pixel detector has approximately 80.4 million readout channels. 
%
%For the SCT, eight strip layers (four space points) are crossed by each track. In the barrel region, this detector uses small-angle (40\,mrad) stereo strips to measure both coordinates, with one set of strips in each layer parallel to the beam direction, measuring $R - \phi$. The total number of readout channels in the SCT is approximately 6.3 million.
%
%A large number of hits (typically 36 per track) is provided by the 4\,mm diameter straw tubes of the TRT, which enables track-following up to $|\eta| = 2.0$. The TRT only provides $R - \phi$ information, for which it has an intrinsic accuracy of 130\,$\mu$m per straw. In the barrel region, the straws are parallel to the beam axis and are 144\,cm long, with their wires divided into two halves, approximately at $\eta = 0$. In the end-cap region, the 37\,cm long straws are arranged radially in wheels.
