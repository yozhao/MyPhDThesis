\section{Overview}
\label{sec:sys:ov}

Both the experimental and theoretical uncertainties include two kinds of uncertainties that are studied and applied in the analysis: 
the normalisation uncertainty and the shape uncertainty of the $\mT$ distribution.
As for the shape uncertainty, unless explicitly discussed in this section, most of the shape uncertainties are very small and therefore neglected in the analysis.
The normalisation uncertainty is considered as the relative difference in the integrated event yields between the nominal and alternative MC samples.
The experimental uncertainty is treated by varying the parameters of one source of uncertainty at a time, and then re-running the full analysis.
The theoretical uncertainty includes generally uncertainties due to the choice of generator and parton shower modelling, QCD renormalisation and factorisation scales, PDF model used to evaluate the cross section and acceptance, etc. 
%More details will be given later in this section.

In the analysis, the variation of ${\mu}_R$ and ${\mu}_F$ is used to estimate the uncertainties due to missing higher order corrections.
The mostly used method is called 7-point scale variations, i.e. pairwise variations of
\begin{equation}
\{{\mu}_R,{\mu}_F\} \times \{0.5,0.5\},\{1,0.5\},\{0.5,1\},\{1,1\},\{2,1\},\{1,2\},\{2,2\}.
\end{equation}
If a 7-point variation is not available, a 3-point variation with the scales varied simultaneously can be used:
\begin{equation}
\{{\mu}_R,{\mu}_F\} \times \{0.5,0.5\},\{1,1\},\{2,2\}\,,
\end{equation}
which should in general lead to larger variations than the independent variation of ${\mu}_R$ and ${\mu}_F$.

The estimation of PDF uncertainty can usually be done in three different ways depending on what PDF set is used in the nominal samples:
\begin{description}
\item [Symmetric Hessian:] e.g. CTEQ66. The idea is that each PDF has $n$ (uncorrelated) 
eigenvalues
%parameters 
and hence each 
eigenvalue
%parameter 
can be varied independently by $+/-1 \sigma$ to create a new PDF. The uncertainty is given by 
\begin{equation}
\Delta X = \frac{1}{2}\sqrt{\sum\limits_i (X_{i+}-X_{i-})^2} 
\end{equation}
if variations are provided as pairs, or 
\begin{equation}
\Delta X = \sqrt{\sum\limits_i (X_i-X_0)^2}
\end{equation}
if provided as single values, where $X_0$ is the central value and $X_i$ corresponds to the variation of the $i$-th 
eigenvalue,
%parameter, 
with $X_{i+}$ and $X_{i-}$ corresponding to the $+/-1 \sigma$ variation.
\item [Asymmetric Hessian:] e.g. CT10 and MSTW. The idea is similar to the Symmetric Hessian method, but if the $+/-1 \sigma$ variations are in the same direction, the largest is used:
\begin{equation}
\Delta X_{+}=\sqrt{\sum\limits_i \max(0,X_{i+}-X_0,X_{i-}-X_0)^2}
\end{equation}
and
\begin{equation}
\Delta X_{-}=\sqrt{\sum\limits_i \max(0,X_0-X_{i+},X_0-X_{i-})^2}
\end{equation}
\item [Standard deviation:] e.g. NNPDF. The NNPDF set does not provide a set of error PDFs. Instead of a central value and some error PDFs with 
eigenvalues
%parameters 
varied they provide an ensemble of PDFs, which is made from fits to the ensemble test on the input data. In that way, the best value would be the mean value of all the ensembles and the uncertainty is the standard deviation:
\begin{equation}
\Delta X=\sqrt{\frac{1}{N}\sum\limits_i (X_i-X_0)^2}
\end{equation}
where $X_0$ is the central value or the mean value, and $N$, the number of ensembles, is usually 100 for NNPDF.
\end{description}

The uncertainty on the integrated luminosity is 2.1\% for the 2015 dataset, 2.2\% for the 2016 dataset, and 2.1\% for the $2015+2016$ combined dataset. The estimation of the luminosity uncertainties follows a methodology similar to that described in Ref.~\cite{lumi2}, from a calibration of the luminosity scale using $x-y$ beam-separation scans performed in August 2016 and May 2016.


For both signals and backgrounds, 
the dominant experimental uncertainties are found to arise from
the jet energy scale and resolution (Jet)~\cite{Aaboud:2017jcu},
the $b$-tagging efficiency ($b$-tag)~\cite{Aad:2015ydr}, 
and the pile-up modelling~\cite{JVT}.
Sources of experimental uncertainties, such as trigger efficiency, lepton reconstruction and identification efficiencies, lepton momentum scale and resolution~\cite{ATLAS:2016iqc,Aad:2016jkr}, missing transverse momentum reconstruction~\cite{met} and jet vertex tagger~\cite{JVT}, are also considered in the analysis (see Sections~\ref{sec:sys:exp-top},~\ref{sec:sys:exp-ww},~\ref{sec:sys:exp-sig} and \ref{sec:sys:wjsys} for the experimental uncertainties for top, $WW$, signal and $W+$jets, respectively).
The uncertainty that is associated with pile-up modelling is estimated by performing a variation of $\pm 9\%$ in the number of simulated pile-up interactions which covers the uncertainty in the ratio of the predicted and measured cross sections of non-diffractive inelastic events that produce a hadronic system of mass $m_{X,\text{had}}>13$\GeV~\cite{STDM-2015-05}.

The dominant uncertainties, including both experimental and theoretical uncertainties, for the top and $WW$ backgrounds are summarised in Tables~\ref{tab:top_syst_lvlv} and \ref{tab:ww_syst_lvlv}.
%The maximum changes in yield for the up and down variations are shown in the various signal and control regions. 
Systematic uncertainties from lepton identification efficiencies, momentum and scale resolutions, are found to be approximately 1\%, and thus not shown in the tables. But they, as well as the other sources with very small uncertainties, are all included in the total uncertainty as shown in the last column in the tables.
The correlation between the SRs and CRs is also taken into account in the simultaneous fit.
%\begin{table}[tbp]
\begin{table}[!htbp]
\caption{\small Relative impact (in \%) of the dominant experimental and theoretical uncertainties in the event yields for the top-quark background in the three SRs (SR$_\text{ggF}$, SR$_\text{VBF1J}$ and SR$_\text{VBF2J}$) and the top-quark and $WW$ CRs (Top CR$_\text{ggF/VBF}$ and the $WW$ CR$_\text{ggF/VBF1J}$).
Jet and $b$-tag are the dominant sources of the experimental uncertainty, while ME+PS, Scale, Single top and PDF are the dominant theoretical uncertainties. The last column corresponds to the total uncertainty including those not listed here.}
\label{tab:top_syst_lvlv}
\center
%\resizebox{0.6\textwidth}{!}{
\begin{tabular}{|l|cccccc|c|}
\hline
Source & Jet & $b$-tag & ME+PS & Scale & Single top & PDF & Total \\\hline
SR$_\text{ggF}$ & 5.2 & 17 & 1.3 & 3.0 & 4.2 & 2.5 & 19\\ 
$\text{SR}_\text{VBF1J}$  & 9.6 & 7.8 & 1.0 & 1.6 & 5.9 & 2.6 & 15 \\
$\text{SR}_\text{VBF2J}$  & 9.7 & 14 & 9.5 & 5.0 & 2.1 & 3.4 & 21 \\\hline
Top CR$_\text{ggF}$ & 2.2 & 4.8 & 0.34 & 0.21 & 2.6 & 3.0 & 6.6 \\
$WW$ CR$_\text{ggF}$ & 5.3 & 18 & 1.1 & 6.3 & 4.0 & 3.2 & 20 \\
$\text{Top CR}_\text{VBF}$  & 8.2 & 3.5 & 10 & 1.5 & 1.3 & 3.7 & 14 \\
${\text{$WW$ CR}}_\text{VBF1J}$  & 9.9 & 8.3 & 9.4 & 3.9 & 5.3 & 2.7 & 18 \\\hline
\end{tabular}
%}
\end{table}

%\begin{table}[tbp]
\begin{table}[!htbp]
\caption{\small Relative impact (in \%) of the dominant experimental and theoretical uncertainties in the event yields for the $WW$ background in the three SRs (SR$_\text{ggF}$, SR$_\text{VBF1J}$ and SR$_\text{VBF2J}$) and the top-quark and $WW$ CRs (Top CR$_\text{ggF/VBF}$ and the $WW$ CR$_\text{ggF/VBF1J}$).
Jet and Pile-up are the dominant sources of the experimental uncertainty, while ME+PS, $\mu_\text{R}$, Resummation and PDF are the dominant theoretical uncertainties. The last column corresponds to the total uncertainty including those not listed here.}
\label{tab:top_syst_lvlv}
\label{tab:ww_syst_lvlv}
\center
%\resizebox{0.6\textwidth}{!}{
\begin{tabular}{|l|cccccc|c|}
\hline
Source & Jet & Pile-up & ME+PS & $\mu_\text{R}$ & Resummation & PDF & Total \\\hline
SR$_\text{ggF}$ & 1.2 & 1.8 & 2.4 & 1.7 & 3.1 & 2.7 & 5.5 \\ 
$\text{SR}_\text{VBF1J}$  & 17 & 2.8 & 11 & 7.3 & 5.0 & 2.3 & 23 \\
$\text{SR}_\text{VBF2J}$  & 18 &  3.1 & 38 & 18 & 1.4 & 2.1 & 47 \\\hline
$WW$ CR$_\text{ggF}$ & 1.1 & 1.8 & 2.6 & 0.95 & 2.9 & 3.6 & 5.9 \\
${\text{$WW$ CR}}_\text{VBF1J}$  & 16 & 4.5 & 12 & 11 & 2.3 & 2.8 & 23 \\\hline
\end{tabular}
%}
\end{table}

For the top-quark background, 
the uncertainties arised from the event generator and parton shower modelling (ME+PS) are estimated by comparing the nominal \textsc{Powheg-Box+Pyhtia8} generated samples with the samples generated by an alternative event generator, \textsc{Sherpa}\,2.2.1. 
The uncertainty which is named ``Scale'' in Table~\ref{tab:top_syst_lvlv}, corresponds to the variations of the renormalisation $\mu_\text{R}$ and factorisation $\mu_\text{F}$ scales, as well as the variations of $h_\text{damp}$. 
The variations for $\mu_\text{R}$ and $\mu_\text{F}$ are from 0.5 to 2 with regard to the nominal scale of $\sqrt{m^2_\text{top}+\pT^2}$, where $\pT$ is the transverse momentum of the top quark. 
The parameter $h_\text{damp}$ is varied between $m_\text{top}$ and $2\cdot m_\text{top}$ from the nominal scale $h_\text{damp}=1.5\cdot m_\text{top}$. 
Since the single-top-quark and $t\bar{t}$ processes are studied together in the analysis, an uncertainty of 20\%~\cite{TOPQ-2012-20,TOPQ-2015-16} is assigned to the relative contribution of the single-top-quark processes, which corresponds to the source ``Single top'' in the table.
The PDF uncertainty for the top-quark background is estimated by taking the envelope of the uncertainty of the NNPDF30NLO PDF set and the differences of its central value with the CT14~\cite{ct14} and MMHT 2014~\cite{mmht2014} PDF sets, following the recommendations of Ref.~\cite{Lai:2010vv}. 
The PDF uncertainties are $\mT$ dependent, increased from 2\% to 10\% as a function of $\mT$ (see Figure~\ref{fig:pdf-mt-top} in Section~\ref{sec:sys:the-top}). This $\mT$ dependence is taken into account in the signal regions.
In the ggF quasi-inclusive category, two additional shape systematic uncertainties (see Section~\ref{sec:sys:the-top}) that are associated with the scale variations and the leading lepton $\pT$ reweighting for the top-quark background are applied, the latter corresponding to $\pm 50\%$ of the reweighting correction. 
These two uncertainties are comparable, varying from a few percent at low $\mT$ to about 10\% at $\mT\simeq 1$\TeV,
without affecting the integrated event yield of the top-quark background in the category.

For the $WW$ background, 
the ME+PS modelling uncertainty is obtained by comparing the nominal sample generated by \textsc{Sherpa 2.2.1} with the alternative sample generated by {\sc Powheg-Box}+{\sc Pythia8}. 
The renormalisation, factorisation and resummation scales are varied separately by factors of 0.5 and 2.0. 
The factorisation scale uncertainty is very small, compared to the other uncertainties and thus not shown in the table.
The PDF uncertainty is obtained and treated in the same way as for the top-quark background (see Figure~\ref{fig:pdf-mt-ww} in Section~\ref{sec:sys:the-ww}).
An additional shape uncertainty in the $\mT$ distribution from ME+PS is applied in the ggF quasi-inclusive category, varied from a few percent at low $\mT$ to about 20\% at $\mT\simeq 1$\TeV. 
There is no significant shape uncertainty observed in the VBF categories. 
In addition to the scale uncertainties described above, a relative uncertainty of $\pm 50$\% is assigned to the correction of the $q\bar{q}\to WW$ \textsc{Sherpa} sample to the combined NNLO QCD and NLO EW predictions in the ggF SR and $WW$ CR.
Both the scale and the $q\bar{q}\to WW$ reweighting uncertainties are further discussed in Section~\ref{sec:sys:the-ww}. 
Besides, as discussed in Section~\ref{sec:bkg:ww}, an uncertainty of 60\%~\cite{Melnikov:2015laa,Caola:2015rqy,Bonvini:2013jha,Li:2015jva} is assigned to the high order cross section $k$-factor (1.7) for the $gg\to (h^\ast)\to WW$ process.


For the other background processes that have small contributions to the yields, such as $WZ$, $ZZ$, $Z/\gamma^\ast$+jets and $WW$ in the $N_\text{jet}\geq 2$ VBF category, 
%since they are normalised to the theoretical predictions,
the uncertainties in their yields due to the uncertainties in the predictions are evaluated with the same prescription as described above.
The impact of these uncertainties is small (see Tables~\ref{tab:yieldWW_lvlv_ggFincl} and \ref{tab:yieldWW_lvlv_VBF1J2J} in Chapter~\ref{chap:res}).


For the theoretical uncertainties in the signal acceptance,
effects due to the choice of QCD renormalisation and factorisation scales and the PDF set, as well as the underlying-event modelling, parton shower model and parton shower tune are all considered.
These uncertainties are estimated separately in three event categories, as a function of the resonance mass, independently for ggF- and VBF-induced resonances. 
To estimate the uncertainty associated to the missing high-order corrections in QCD, the renormalisation and factorisation scales are varied independently by factors of 0.5 and 2.0 from the nominal scale, $\sqrt{m_H^2+p^2_{\text{T}, H}}$, where $m_H$ is the mass and $p_{\text{T}, H}$ is the transverse momentum of the heavy Higgs boson.
The signal acceptance obtained using the modified MC samples are then compared to the nominal sample. 
The uncertainties for resonances produced via ggF are found to be negligible in the quasi-inclusive ggF and $N_\mathrm{jet}=1$ VBF categories, whilst in the $N_\mathrm{jet}\geq 2$ VBF category the uncertainties range between $2.5\%$ and $0.2\%$ with the resonance mass varied from 200\GeV to 4\TeV (unless stated otherwise, the uncertainties shown in the following are quoted for the same mass range).
The uncertainties for resonances produced via vector-boson fusion are found to be ranged from $0.9\%$ to $2.8\%$ in the quasi-inclusive ggF category, from $1.9\%$ to $3.6\%$ in the $N_\mathrm{jet}=1$ VBF category, and from $1.0\%$ to $7.3\%$ in the $N_\mathrm{jet}\geq 2$ VBF category.
The 
PDF-induced uncertainties in the signal acceptance are estimated in the same way as that for the top-quark and $WW$ background.
The corresponding uncertainties for the ggF-induced (VBF-induced) signal, are found to be up to $0.4\%$ ($1.7\%$), $1.5\%$ ($1.2\%$) and $1.6\%$ ($1.5\%$) for the quasi-inclusive ggF, $N_{\text{jet}}=1$ and $N_{\text{jet}}\geq 2$ VBF event categories, respectively.
To estimate the uncertainties associated to the parton shower tune and underlying event, 
the internal parameters that are associated with final-state radiation or the multiple parton interactions in the \textsc{Pythia} generator are varied independently, up or down, such that their influence on the signal acceptance of the various signal mass points can be studied separately.
In each event category, and for every mass point, the uncertainties are compared with that due to the choice of the parton shower model, which are estimated by comparing the results obtained for the nominal parton shower generator with those obtained using the alternative one with \textsc{Herwig++}~\cite{Bahr:2008pv,Bellm:2013hwb}.
The tune uncertainties are not considered in the final results, due to the fact that they are found to be very small compared with the shower uncertainties for all mass points.
The shower uncertainties for ggF-induced signals are found to increase from $1.3\%$ to $3.1\%$ for increasing resonance masses in the quasi-inclusive ggF category, from $13\%$ to $28\%$ in the $N_{\text{jet}}=1$ category, and from $2.3\%$ to $15\%$ in the $N_{\text{jet}}\geq 2$ VBF category. 
The uncertainties for VBF-induced signals increase from $4.3\%$ to $19\%$, from $5.1\%$ to $9.0\%$, and from $3.3\%$ to $8.0\%$ in the three categories, respectively.
The uncertainties due to missing high-order corrections in QCD are also evaluated for ggF-induced processes in each event category, with the event migration effects between different event categories considered.
The method that is used for the estimation of the corresponding uncertainties is proposed by Stewart and Tackmann~\cite{Stewart:2011cf}.
The resulting uncertainties associated to this are found to be ranged from $3\%$ to $10\%$ for the quasi-inclusive ggF category and from $4\%$ to $30\%$ ($30\%-60\%$) for the $N_{\text{jet}}=1$ ($N_{\text{jet}}\geq 2$) VBF event categories.
More details about the signal theoretical uncertainty are presented in Section~\ref{sec:sys:the-sig}.

%%paper plagiarism check up to here
For the estimation of the data-driven $W$+jets background, there are several sources of systematic uncertainty.  
The subtraction of the subdominant electroweak processes (Section~\ref{sec:bkg:wj}) is found to have a significant impact on the calculation of the extrapolation factor at high lepton $\pT$. 
As  described in Ref.~\cite{ATLAS:2014aga}, the subtraction is varied. The variation of the event yields in the SR is taken as the uncertainty, assuming that the extrapolation factors of the dijet and $W$+jets samples are the same.   
An additional systematic uncertainty source is introduced due to the differences in the jet flavour composition between dijet and $W$+jets events. 
This uncertainty is taken to be the sum of two contributions in quadrature: one corresponds to the differences between the extrapolation factors that are calculated with dijet samples and $Z$+jets samples in data, while the other corresponds to the differences between the extrapolation factors estimated with $W$+jets and $Z$+jets MC samples.
The statistical uncertainties of the different data and MC samples that are used to estimate the extrapolation factors are taken as another source of systematic uncertainty.  
The overall relative systematic uncertainty of the $W$+jets background is finally found to be approximately 35\% in each categorie. The dominant uncertainty is found to be associated to the jet flavour composition.
\FloatBarrier
