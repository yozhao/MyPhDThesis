\section{Event category uncertainty}
\label{sec:app:mig}

As this analysis is carried out in several orthogonal event categories, whose definition partially 
depend on the number of jets in the final state, the perturbative uncertainties on the theory 
predictions have to be calculated separately in each of these event categories. In addition, the 
correlation between the predicted cross sections and their corresponding uncertainties have to be 
taken into account, when combining the results of these exclusive event categories. \par

One of the approaches used in ATLAS to take into account the requirements that were stated above 
was developed by Stewart and Tackmann~\cite{Stewart:2011cf}. The main idea of this method is to 
first determine the uncertainties on inclusive $N$-jet cross sections $\sigma_{\geq N}$ and use them 
to calculate uncertainties on the exclusive $N$-jet cross section $\sigma_{N}$ using the relation 
\begin{equation}
\sigma_{N}=\sigma_{\geq N} - \sigma_{\geq N+1} \,. \nonumber
\end{equation}
Here, the inclusive jet-bin cross sections $\sigma_{\geq N}$ and $\sigma_{\geq N+1}$ are assumed 
to be uncorrelated. Thus the uncertaintiies on the exclusive jet-bin cross section $\sigma_{N}$ can 
simply be determined via
\begin{equation}
\Delta\sigma_{N}=\sqrt{\Delta\sigma_{\geq N}^2 + \Delta\sigma_{\geq N+1}^2}\,. \nonumber
\end{equation}
Following the example given in Ref.~\cite{ATL-PHYS-PUB-2011-011}, nuisance parameters are defined (in 
Table~\ref{STNuisanceParameters}) corresponding to the average upward and downward variation $\kappa_{i}$ of the 
calculated exclusive cross section uncertainties for a log-normal distribution. These up and down
variations are calculated by varying the QCD factorisation and normalisation scales in the ranges 
\begin{equation}
\frac{1}{2} \leq \frac{\mu_{F}}{m_{H}/2} \leq 2 \,\,\, \mathrm{and} \,\,\, \frac{1}{2} \leq \frac{\mu_{R}}{m_{H}/2} \leq 2 \,. \nonumber
\end{equation}

For the definition of the nuisance parameters, the exclusive jet-bin fractions 
$f_{0}=\sigma_{0}/\sigma_{\geq 0}$, $f_{1}=\sigma_{1}/\sigma_{\geq 0}$, and 
$f_{2}=\sigma_{2}/\sigma_{\geq 0}$ are used as well. The total inclusive production 
cross section $\sigma_{\geq 0}$ and their corresponding scale uncertainties
are calculated at next-to-next-to-leading order QCD using the parton-level
fixed-order program HNNLO (v1.3), while the inclusive 1- and 2-jet bin cross sections 
$\sigma_{\geq 1}$ and $\sigma_{\geq 2}$ as well as their corresponding QCD scale 
uncertainties are calculated at NLO and LO precision respectively using 
the program MCFM (v8.0). Jets are defined in these calculations as objects 
having a transverse momentum of at least $30\GeV$ and an absolute pseudo-rapidity 
value of at least $2.4$. This of course leads to the problem that the definition of 
the $N_{\text{jet}}\ge 2$ category used in this approach to calculate the jet bin migration uncertainties 
is different from the VBF $N_{\text{jet}}\ge 2$ category used in this analysis. Thus the standard 
procedure of the Stewart-Tackman method is slightly adjusted in the following: 
the cross section $\sigma_{\geq 2}$ (calculated considering only jets with $|\eta|>2.4$)
will be used to calculate the migration uncertainties related to the VBF $N_{\text{jet}}=1$  category
but not for those of the VBF $N_{\text{jet}}\ge 2$ category. The corresponding uncertainties for the 
VBF $N_{\text{jet}}\ge 2$ category are then simply determined separately using the largest of the two considered 
QCD scale variations on the production cross section predicted by the NLO generator 
\textsc{MG5\_aMC@NLO} (simulating the gluon-fusion induced production of Higgs bosons in association with 
up to two jets in the matrix element). As the overlap between the definition of the VBF $N_{\text{jet}}\ge 2$ 
phase space and the definition of the phase space used to calculate $\sigma_{\geq 2}$ is 
relative small, potential correlations between the nuisance parameter of the VBF $N_{\text{jet}}\ge 2$ category
$S_{\text{VBF 2-jet}}$ and those for the inclusive ggF $S^0_{\text{ggF}}$ and $S^1_{\text{ggF}}$ as well as the 
VBF $N_{\text{jet}}=1$  region $S^1_{\text{VBF 1-jet}}$ and $S^2_{\text{VBF 1-jet}}$ are neglected in the following.
%\begin{table}[!h]
\begin{table}[!htbp]
\centering
\caption{Definition of nuisance parameters used to describe the uncertainties based on the migration between event categories~\cite{Stewart:2011cf}.}
\label{STNuisanceParameters}
\renewcommand{\arraystretch}{1.6}
\begin{tabular}{ c | c | c }
\hline
  ggF inclusive category  &  VBF $N_{\text{jet}}=1$ category &  VBF $N_{\text{jet}}\geq 2$ category  \\
\hline
 $S^0_{\text{ggF}} = \left(\kappa_{\geq 0}\right)^{\frac{1}{f_{0}}}$         &                      --                            &           --          \\
 $S^1_{\text{ggF}} = \left(\kappa_{\geq 1}\right)^{-\frac{f_1+f_2}{f_{0}}}$  & $S^1_{\text{VBF 1-jet}} = \left(\kappa_{\geq 1}\right)^{\frac{f_1+f_2}{f_{1}}}$   &           --          \\
                   --                                                 & $S^2_{\text{VBF 1-jet}} = \left(\kappa_{\geq 2}\right)^{-\frac{f_2}{f_{1}}}$        &     $S_{\text{VBF 2-jet}} = \kappa_{\geq 2}$ \\
\hline
\end{tabular}
\end{table}


The distributions of the four nuisance parameters obtained via the Stewart-Tackmann method are shown as a function of the 
resonance mass in Figure~\ref{fig:EventCategoryUncertainties}. All four parameters show a small mass dependence. 
The parameter $S^0_{\text{ggF}}$ increases from $1.027$ for the lowest resonance mass up to a value of $1.064$ for a resonance 
mass of $3\,\TeV$, while $S^1_{\text{ggF}}$ varies between $0.958$ and $0.919$. The nuisance parameters for the VBF $N_{\text{jet}}=1$ category 
$S^1_{\text{VBF 1-jet}}$ and $S^2_{\text{VBF 1-jet}}$ are $1.299$ and $0.968$ for a resonance mass of $200\GeV$ and decrease
to values of $1.260$ and $0.963$ respectively for a resonance mass of $3\,\TeV$. In the VBF $N_{\text{jet}}\ge 2$
category the normalisation uncertainties due to the QCD scale choice have been calculated to be 
$+30.2\%$ ($-26.8\%$) for a resonance mass of $200\GeV$. These uncertainties increase for increasing 
resonance masses and reach values of $+58.2\%$ and $-37.0\%$ for a resonance mass of $4\,\TeV$.
The exact uncertainty values for all considered mass points are shown in 
Table~\ref{STNuisanceParametersValues}, while Table~\ref{tab:ListofFrac_and_Kappas} shows the 
exclusive jet-bin fractions and the corresponding $\kappa_{i}$ values, i.e. the input quantities used 
to calculate these nuisance parameters. The distribution of the exclusive jet-bin fractions is also 
shown as a function of the resonance mass in Figure~\ref{fig:Dist_XSec_and_XSecFrac}.
%\begin{figure}[!ht]
\begin{figure}[!htbp]
\noindent
\center
\includegraphics[width=0.49\textwidth]{fig/note/EventCategory_Uncertainties_ggF.pdf}
\caption{Distribution of the nuisance parameters, which describe the uncertainties arising due to the migration between event
categories. }
\label{fig:EventCategoryUncertainties}
\end{figure}

%\begin{figure}[!ht]
\begin{figure}[!htbp]
\noindent
\center
\includegraphics[width=0.49\textwidth]{fig/note/CrossSections_ggF.pdf}
\includegraphics[width=0.49\textwidth]{fig/note/EventFractions_ggF.pdf}
\caption{Inclusive cross sections per event category and relative event fractions displayed as a function of the resonance mass.}
\label{fig:Dist_XSec_and_XSecFrac}
\end{figure}

%\begin{table}[!h]
\begin{table}[!htbp]
\centering
\caption{Nuisance parameters describing the jet bin migration uncertainties as calculated with the 
Stewart-Tackmann method for various resonance mass point.}
\label{STNuisanceParametersValues}
\begin{tabular}{ l | c | c | c | c | c }
\hline
\hline
Mass point $[\GeV]$ & $S^0_{\text{ggF}}$ & $S^1_{\text{ggF}}$ & $S^1_{\text{VBF 1-jet}}$ & $S^2_{\text{VBF 1-jet}}$ & $S_{\text{VBF $\geq$ 2-jets}}$ \\
\hline

$200$  & $1.027$ & $0.958$ & $1.299$ & $0.968$ & $1.302 / 0.732$ \\
$300$  & $1.029$ & $0.951$ & $1.301$ & $0.965$ & $1.285 / 0.738$ \\
$400$  & $1.031$ & $0.949$ & $1.277$ & $0.967$ & $1.269 / 0.739$ \\
$500$  & $1.031$ & $0.945$ & $1.277$ & $0.960$ &     $ --- $ \\
$600$  & $1.033$ & $0.940$ & $1.282$ & $0.966$ & $1.306 / 0.728$ \\
$700$  & $1.036$ & $0.940$ & $1.271$ & $0.965$ &     $ --- $ \\
$750$  & $1.037$ & $0.940$ & $1.270$ & $0.965$ &     $ --- $ \\
$800$  & $1.038$ & $0.938$ & $1.274$ & $0.966$ &     $ --- $ \\
$900$  & $1.039$ & $0.939$ & $1.265$ & $0.967$ &     $ --- $ \\
$1000$ & $1.041$ & $0.935$ & $1.268$ & $0.963$ & $1.352 / 0.711$ \\
$1200$ & $1.044$ & $0.929$ & $1.267$ & $0.962$ & $1.342 / 0.711$ \\
$1400$ & $1.045$ & $0.931$ & $1.266$ & $0.964$ & $1.394 / 0.696$ \\
$1600$ & $1.047$ & $0.929$ & $1.262$ & $0.962$ & $1.366 / 0.701$ \\
$1800$ & $1.047$ & $0.930$ & $1.259$ & $0.960$ & $1.430 / 0.683$ \\
$2000$ & $1.049$ & $0.929$ & $1.263$ & $0.962$ &     $ --- $ \\
$2200$ & $1.051$ & $0.925$ & $1.263$ & $0.960$ &     $ --- $ \\
$2400$ & $1.051$ & $0.919$ & $1.262$ & $0.959$ & $1.440 / 0.676$ \\
$2600$ & $1.050$ & $0.919$ & $1.264$ & $0.956$ &     $ --- $ \\
$2800$ & $1.054$ & $0.926$ & $1.263$ & $0.958$ &     $ --- $ \\
$3000$ & $1.064$ & $0.919$ & $1.260$ & $0.963$ & $1.484 / 0.659$ \\
$3200$ & $1.069$ & $0.920$ & $1.260$ & $0.963$ &     $ --- $ \\
$3400$ & $1.078$ & $0.916$ & $1.264$ & $0.963$ & $1.530 / 0.645$ \\
$3600$ & $1.086$ & $0.918$ & $1.259$ & $0.980$ &     $ --- $ \\
$3800$ & $1.097$ & $0.916$ & $1.269$ & $0.960$ &     $ --- $ \\
$4000$ & $1.107$ & $0.916$ & $1.273$ & $0.957$ & $1.578 / 0.631$ \\
\hline
\hline
\end{tabular}
\end{table}

%\begin{table}[!h]
\begin{table}[!htbp]
\centering
\caption{Exclusive jet bin fractions and $\kappa_{i}$ values for various resonance mass points.}
\label{tab:ListofFrac_and_Kappas}
\begin{tabular}{ l | c | c | c | c | c | c }
\hline
\hline
Mass point $[\GeV]$ & $f_{0}$ & $f_{1}$ & $f_{2}$ & $\kappa_{\geq 0}$ & $\kappa_{\geq 1}$ & $\kappa_{\geq 2}$ \\
\hline
$200$ & $0.851$ & $0.14$ & $0.0089$ & $1.023$ & $1.279$ & $1.667$ \\
$300$ & $0.83$ & $0.16$ & $0.0107$ & $1.024$ & $1.28$ & $1.705$ \\
$400$ & $0.814$ & $0.175$ & $0.0119$ & $1.025$ & $1.257$ & $1.641$ \\
$500$ & $0.802$ & $0.185$ & $0.013$ & $1.025$ & $1.257$ & $1.773$ \\
$600$ & $0.79$ & $0.196$ & $0.0139$ & $1.026$ & $1.261$ & $1.625$ \\
$700$ & $0.784$ & $0.201$ & $0.0148$ & $1.028$ & $1.25$ & $1.614$ \\
$750$ & $0.783$ & $0.202$ & $0.0151$ & $1.029$ & $1.249$ & $1.605$ \\
$800$ & $0.778$ & $0.207$ & $0.015$ & $1.029$ & $1.253$ & $1.616$ \\
$900$ & $0.775$ & $0.209$ & $0.0158$ & $1.03$ & $1.244$ & $1.569$ \\
$1000$ & $0.765$ & $0.218$ & $0.0167$ & $1.031$ & $1.247$ & $1.626$ \\
$1200$ & $0.748$ & $0.233$ & $0.0185$ & $1.033$ & $1.245$ & $1.621$ \\
$1400$ & $0.753$ & $0.229$ & $0.018$ & $1.034$ & $1.245$ & $1.594$ \\
$1600$ & $0.746$ & $0.235$ & $0.0194$ & $1.035$ & $1.24$ & $1.609$ \\
$1800$ & $0.745$ & $0.235$ & $0.0197$ & $1.035$ & $1.237$ & $1.634$ \\
$2000$ & $0.745$ & $0.236$ & $0.0192$ & $1.036$ & $1.241$ & $1.605$ \\
$2200$ & $0.737$ & $0.245$ & $0.0184$ & $1.037$ & $1.243$ & $1.707$ \\
$2400$ & $0.718$ & $0.260$ & $0.0221$ & $1.036$ & $1.239$ & $1.626$ \\
$2600$ & $0.718$ & $0.260$ & $0.0223$ & $1.036$ & $1.241$ & $1.694$ \\
$2800$ & $0.738$ & $0.241$ & $0.0208$ & $1.040$ & $1.240$ & $1.644$ \\
$3000$ & $0.717$ & $0.263$ & $0.0202$ & $1.046$ & $1.239$ & $1.636$ \\
\hline
\hline
\end{tabular}
\end{table}
\FloatBarrier
