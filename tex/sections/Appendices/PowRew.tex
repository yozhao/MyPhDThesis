\section{\textsc{Powheg}-to-\textsc{MadGraph} reweighting}
\label{sec:app:powrew}

The \textsc{Powheg} generator version that was used to simulate the NWA signal samples
is only capable of producing maximal one jet in association to a ggF induced
Higgs-like resonance in the matrix element, while further jets are emulated by the  
parton shower generator, \textsc{Pythia8}. As a consequence, the contribution of the ggF signal topology in 
the $N_{\text{jet}} \geq 2$ VBF event category is assumed to be insufficiently described.
However, a more realistic modelling of higher jet-multiplicities is provided using the
\textsc{MadGraph5\_aMC@NLO}~\cite{Alwall:2014hca} generator in order to simulate $gg\rightarrow H$
events in association with up to two jets in the matrix element. Here,
the overlap between identical final states generated at the matrix element and the parton
shower stage is removed using the FxFx merging~\cite{Frederix:2012ps}. 

Thus the expected mismodelling in the $N_{\text{jet}} \geq 2$ VBF event category corresponding to
the predictions of the \textsc{Powheg} generator are corrected by dedicated scale factors. These scale 
factors are determined as the double ratios
\begin{equation}
k=\frac{N^{\text{2-jet VBF}}_{\textsc{MadGraph5}}/N^{\text{inclusive ggF}}_{\textsc{MadGraph5}}}{N^{\text{2-jet VBF}}_{\textsc{Powheg}}/N^{\text{inclusive ggF}}_{\textsc{Powheg}}}\;,
\end{equation}
in which $N^{\text{2-jet VBF}}_{\textsc{MadGraph5}}$ and 
$N^{\text{inclusive ggF}}_{\textsc{MadGraph5}}$ are the event yields in the $N_{\text{jet}} \geq 2$ VBF 
and inclusive ggF event categories respectively (obtained using the event selection at generator 
level), as predicted by the \textsc{MadGraph5} generator. The parameters 
$N^{\text{2-jet VBF}}_{\textsc{Powheg}}$ and $N^{\text{inclusive ggF}}_{\textsc{Powheg}}$ are the corresponding event yields as predicted by \textsc{Powheg}. 

These scale factors are calculated for several hypothetical heavy-Higgs masses and the corresponding 
results are presented in Table~\ref{tab:POWHEG-to-MG5_kFactors}. According to these findings, 
the lowest considered mass values, $0.2\TeV$, correspond to the largest $k$-factor value, $1.136$.
The size of the $k$-factor decreases continuously with increasing resonance mass and reaches finally 
a value of $0.843$ for the $4.0\TeV$ mass point. 

In addition, \textsc{Powheg}-to-\textsc{MadGraph} scale factors are also calculated for the $N_{\text{jet}}=1$ VBF category, as the 
gluon fusion induced production of heavy Higgs-like particles is not yet intensively studied within 
such phase space regions using LHC data. The corresponding $k$-factors range from $0.905$
for the lowest considered resonance mass to $0.729$ for the largest considered mass value. 
As for the $k$-factors corresponding to the ratio of event yields in the $N_{\text{jet}}\geq 2$ VBF and the
inclusive ggF category, the size of the scale factors decreases with increasing resonance mass.
The full set of $k$-factors is shown in Table~\ref{tab:POWHEG-to-MG5_kFactors1VBFJet}.

An extrapolation is applied separately on both sets of $k$-factors using second 
order polynomials in order to obtain $k$-factors for other Higgs mass values as well. In 
case the $k$-factors corresponding to the comparison of the event yields in the $N_{\text{jet}}\geq 2$ VBF 
and the inclusive ggF category are considered, the extrapolation function is obtained to be  
\begin{equation}
1.726\cdot 10^{-8} \times m_{H}^2 / \GeV^2 - 1.323 \cdot 10^{-4} \times m_{H}/\GeV + 1.130 \;,
\end{equation}

while the extrapolation function obtained after fitting the set of $k$-factors that corresponds
to the use of the $N_{\text{jet}}=1$ VBF category is given by
\begin{equation}
3.593\cdot 10^{-8} \times m_{H}^2 / \GeV^2 - 1.880 \cdot 10^{-4} \times m_{H}/\GeV + 0.918 \;.
\end{equation}

The curves of these two extrapolation functions are presented in Figure~\ref{fig:PowhegKFFit} together 
with the two sets of $k$-factors. 
%\begin{table}
\begin{table}[!htbp]
\center
\caption{\textsc{Powheg}-to-\textsc{MadGraph} scale factors for various resonance mass points using the ratio of the number of events in the $N_{\text{jet}}\geq 2$ VBF category to the number of events in the inclusive ggF category.}
\resizebox{\textwidth}{!}{
\begin{tabular}{ l | c | c | c }
\hline
Mass point $[\GeV]$ & $r_{\text{2-jet VBF, ggF}}$ (\textsc{MadGraph5}) & $r_{\text{2-jet VBF, ggF}}$ (\textsc{Powheg}) & scale factor $k$ \\
\hline
200  & 0.017 & 0.015 & 1.136 \\
300  & 0.020 & 0.019 & 1.071 \\
400  & 0.025 & 0.022 & 1.107 \\
600  & 0.032 & 0.031 & 1.041 \\
1000 & 0.043 & 0.042 & 1.013 \\
1200 & 0.044 & 0.046 & 0.956 \\
1400 & 0.048 & 0.050 & 0.973 \\
1600 & 0.050 & 0.053 & 0.950 \\
1800 & 0.052 & 0.055 & 0.956 \\
2400 & 0.056 & 0.060 & 0.938 \\
3000 & 0.057 & 0.063 & 0.902 \\
3400 & 0.058 & 0.064 & 0.901 \\
4000 & 0.056 & 0.065 & 0.851 \\
\hline
\end{tabular}
}
\label{tab:POWHEG-to-MG5_kFactors}
\end{table}

%\begin{table}
\begin{table}[!htbp]
\center
\caption{\textsc{Powheg}-to-\textsc{MadGraph} scale factors for various resonance mass points using the ratio of the number of events in the $N_{\text{jet}}=1$ VBF category to the number of events in the inclusive ggF category.}
\resizebox{\textwidth}{!}{
\begin{tabular}{ l | c | c | c }
\hline
Mass point $[\GeV]$ & $r_{\text{1-jet VBF, ggF}}$ (\textsc{MadGraph5}) & $r_{\text{1-jet VBF, ggF}}$ (\textsc{Powheg}) & scale factor $k$ \\
\hline
 200  & 0.053 & 0.059 & 0.905 \\
 300  & 0.058 & 0.065 & 0.894 \\
 400  & 0.059 & 0.070 & 0.845 \\
 600  & 0.057 & 0.074 & 0.766 \\
 1000 & 0.055 & 0.075 & 0.726 \\
 1200 & 0.058 & 0.075 & 0.771 \\
 1400 & 0.053 & 0.074 & 0.717 \\
 1600 & 0.053 & 0.073 & 0.724 \\
 1800 & 0.050 & 0.072 & 0.692 \\
 2400 & 0.049 & 0.069 & 0.703 \\
 3000 & 0.043 & 0.066 & 0.648 \\
 3400 & 0.047 & 0.065 & 0.722 \\
 4000 & 0.045 & 0.062 & 0.727 \\
\hline
\end{tabular}
}
\label{tab:POWHEG-to-MG5_kFactors1VBFJet}
\end{table}

%\begin{figure}[htbp]
\begin{figure}[!htbp]
\center
\includegraphics[width=0.6\textwidth]{fig/appendix/PowhegKF.pdf}
\caption{\small Distribution of the two sets of \textsc{Powheg}-to-\textsc{MadGraph} scale factors displayed for various 
resonance mass values. These scale factors are represented as black dots for the ratio of event yields 
in the $N_{\text{jet}}\ge 2$ VBF and the inclusive ggF categories and as blue triangles for the ratio of event yields
in the $N_{\text{jet}}=1$ VBF and the inclusive ggF categories. The two second order polynomials, which are 
used as extrapolation functions are shown in addition as a black and a blue dashed lines.  }
\label{fig:PowhegKFFit}
\end{figure}
\FloatBarrier
