%\section{Parton distribution function and renormalisation and factorisation scales in QCD}
\section{PDF and QCD ${\mu}_R$ and ${\mu}_F$ scales}
\label{sec:app:pdf}

Parton distribution functions (PDFs)~\cite{Gao:2017yyd}
of the proton are essential for making theoretical predictions for experiments at the LHC and other high-energy hadron colliders.
It is defined as the probability density for finding a particle with a certain longitudinal momentum fraction $x$ at resolution scale $Q^2$, $f(x,Q^2)$.
Being driven by low-scale non-perturbative dynamics, PDFs cannot be computed from first principles, at least with current technology,
and therefore they are determined using experimental data from a variety of hard-scattering cross sections in lepton-proton and proton-proton collisions.

PDFs and their associated uncertainties play a decisive role in several LHC applications.
PDF uncertainties also affect the production of new high-mass resonances, as predicted by many BSM scenarios~\cite{Beenakker:2015rna}, since they probe PDFs at large values of the momentum fraction $x$, for which current experimental constraints are scarce.

The most generally used PDF sets in ATLAS are CT10~\cite{Lai:2010vv} (or CT14~\cite{ct14}), NNPDF3.0~\cite{NNPDF30} and MMHT2014~\cite{mmht2014}.
Under some well-specified conditions, PDF sets can be statistically combined into an unified set, such as PDF4LHC15 sets~\cite{Butterworth:2015oua}, which combine the three sets using the Monte Carlo method~\cite{Watt:2012tq}, and are subsequently reduced to small number of Hessian eigenvectors~\cite{Gao:2013bia,Carrazza:2015aoa} or Monte Carlo replicas~\cite{Carrazza:2015hva} to facilitate phenomenological applications.


The cross section of a hard-scattering process usually depends on the strong coupling constant ${\alpha}_s$, which is a function of the renormalisation scale, and the calculation of the cross section also depends on the factorisation scale~\cite{Andreev:2017vxu}:
\begin{equation}
\sigma = \sum\limits_{k=g,q,\bar{q}} \sum\limits_{n} \alpha_{s}^{n} (\mu_{R}) \hat{\sigma}^{(n)}_{k}(x,\mu_{R},\mu_{F})\,.
\end{equation}
where $k$ corresponds to all parton flavours, $n$ is the order of the perturbation theory, and $\hat{\sigma}^{(n)}_{k}$ are the hard coefficients. 
The renormalisation scale, ${\mu}_R$, is a point chosen to define the renormalised quantities in case of an infinite theoretical calculation (though in principle the prediction of the theory should be independent of the choice of ${\mu}_R$).
The factorisation scale, ${\mu}_F$, is usually thought to be the scale separating long- and short-distance interactions. Below the scale, perturbation theory is no longer reliable, and the soft and collinear divergences could be absorbed into the PDF.

\FloatBarrier
