\section{The contribution of the $t$-channel to the VBF production mode}
\label{sec:app:tchannel}

The various VBF signal samples that are used for the studies presented in this note 
are simulated for each mass point hypothesis separately from the background. 
Thus both interference effects but also the non-resonant $t$-channel contribution of 
the searched resonance are not taken into account when comparing the sum of the background
predictions and the signal hypothesis to the observations in data.   
This section focuses on a quantification of the missing $t$-channel contribution, while
details on the interference effects between the signal and the diboson $WW$ background were 
already described in Appendix~\ref{sec:app:inter}. For this study, only processes that 
correspond to an electroweak production of $W^{+}W^{-} jj$ final states are considered. 

Due to technical reasons, the estimation of the missing $t$-channel contribution, requires 
the generation of three Monte-Carlo samples (per mass point) in addition to the 
background only hypothesis. These samples contain:
\begin{enumerate}
\item $s$-channel production of a heavy resonance (with the resonance decaying via $H\to W^+W^-$).
\item Simultaneous production of the signal and background (includes interference effects and the $t$-channel contribution). 
\item Non-resonant $W^{+}W^{-} jj$ production (only $t$-channel and background).
\end{enumerate}

These samples are generated at leading order QCD using \textsc{MadGraph5\_aMC@NLO} v2.3.2 and assuming SM-like 
couplings between the heavy scalar and vector bosons. Contributions of diagrams that correspond to a 
non pure electroweak production of the $W^{+}W^{-} jj$ final state are removed and do not enter the 
cross section calculations nor the spectrum of the invariant diboson mass $m_{WW}$ or the transverse 
diboson mass $\mT$ (Figures~\ref{fig:tchannel} and \ref{fig:tchannel_mT}). \par

Cross section values for the various $W^{+}W^{-} jj$ production modes that include a contribution 
of a heavy scalar are presented in Table~\ref{tab:tchannel} for resonance masses $m_{H}$ of $300\GeV$, 
$1.5$\,TeV and 3\,TeV. At the same time, the width of these resonances is set 
for every mass point to $0.15 m_{H}$. The cross section of the background only hypothesis is $\sigma_{B} = 0.7650 \pm 0.0003$\,pb.
Here, the cross section of the resonant $s$-channel production is referred to as $\sigma_{S}$, while 
the cross sections for the simultaneous production of the scalar and the background including 
(excluding) a resonant $W^{+}W^{-}$ production are denoted as $\sigma_{S+B}$ ($\sigma_{B+{\text{$t$-chan}}}$). 
In addition, the $t$-channel cross section, which is estimated via $\sigma_{B+{\text{$t$-chan}}}-\sigma_{B}$, 
is listed as well. For a mass of $300\GeV$, the $t$-channel contribution is predicted to increase the 
non-resonant $W^{+}W^{-} jj$ production cross section by approximately $1.0\%$ and for a resonance 
mass of 1.5\,TeV the increase would be about $0.2\%$. For a mass of 3\,TeV, the $t$-channel
contribution is compatible with zero considering the statistical uncertainties on the cross section
calculations. Therefore, a missing of the $t$-channel contribution to the the background process 
is significantly smaller than the theoretical uncertainties on the background cross section. 
In addition, the contribution of the $t$-channel is shown in Figure~\ref{fig:tchannel} as a function 
of the invariant diboson mass $m_{WW}$ and in Figure~\ref{fig:tchannel_mT} as a function of the 
transverse diboson mass $\mT$. Their distribution is shown together with the cross section 
modulation obtained when considering interference effects between the resonant and non-resonant 
$W^{+}W^{-} jj$ production modes. 
%\begin{table}
\begin{table}[!hbtp]
\center
\caption{Cross sections for resonant and non-resonant $W^{+}W^{-} jj$ production.}
\resizebox{\textwidth}{!}{
\begin{tabular}{ l | c | c | c | c}
\hline
Mass point  &   $\sigma_{S}$ [pb]      &      $\sigma_{S+B}$ [pb]    &   $\sigma_{B+{\text{$t$-chan}}}$ [pb]   &   $\sigma_{B+{\text{$t$-chan}}}-\sigma_{B}$ [pb] \\
\hline
 $300\GeV$  &  $0.1423   \pm 0.0001$     &  $0.8577 \pm 0.0004$   &  $0.7732 \pm 0.0003$  &  $0.0082\pm 0.0004$ \\
1.5\,TeV  &  $0.06901  \pm 0.00003$    &  $0.8426 \pm 0.0004$   &  $0.7662 \pm 0.0003$  &  $0.0012\pm 0.0004$ \\
3\,TeV  &  $0.009744 \pm 0.000004$   &  $0.7804 \pm 0.0003$   &  $0.7647 \pm 0.0003$  &  $-0.0003\pm 0.0004$ \\
\hline
\hline
\end{tabular}
}
\label{tab:tchannel}
\end{table}

%\begin{figure}[htbp]
\begin{figure}[!htbp]
 \centering
\includegraphics[width=0.45\textwidth]{fig/note/mWW_tChannelStudy_h_m_WW_300GeV_300GeV.pdf}
\includegraphics[width=0.45\textwidth]{fig/note/mWW_tChannelStudy_subtracted_h_m_WW_300GeV_300GeV.pdf}
\includegraphics[width=0.45\textwidth]{fig/note/mWW_tChannelStudy_h_m_WW_1500GeV_1500GeV.pdf}
\includegraphics[width=0.45\textwidth]{fig/note/mWW_tChannelStudy_subtracted_h_m_WW_1500GeV_1500GeV.pdf}
\includegraphics[width=0.45\textwidth]{fig/note/mWW_tChannelStudy_h_m_WW_3000GeV_3000GeV.pdf}
 \caption{Cross section for resonant and non-resonant $W^{+}W^{-} jj$ production modes as a function of the $m_{WW}$ shown in addition to their interference and the contribution of the missing $t$-channel.}
 \label{fig:tchannel}
\end{figure}

%\begin{figure}[htbp]
\begin{figure}[!htbp]
 \centering
\includegraphics[width=0.45\textwidth]{fig/note/MT_tChannelStudy_300GeV.pdf}
\includegraphics[width=0.45\textwidth]{fig/note/MT_tChannelStudy_subtracted_300GeV.pdf}
\includegraphics[width=0.45\textwidth]{fig/note/MT_tChannelStudy_MT_1500GeV.pdf}
\includegraphics[width=0.45\textwidth]{fig/note/MT_tChannelStudy_subtracted_MT_1500GeV.pdf}
\includegraphics[width=0.45\textwidth]{fig/note/MT_tChannelStudy_MT_3000GeV.pdf}
 \caption{Cross section for resonant and non-resonant $W^{+}W^{-} jj$ production modes as a function of the $\mT$ shown in addition to their interference and the contribution of the missing $t$-channel.}
 \label{fig:tchannel_mT}
\end{figure}
\FloatBarrier
