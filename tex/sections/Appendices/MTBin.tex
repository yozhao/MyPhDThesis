\section{The optimisation of the $\mT$ binning}
\label{sec:app:mtbin}

In this section, the optimisation method for the binning of the $\mT$ distribution is discussed.
As with the event selection optimisation, the optimisation of the $\mT$ binning is also very important to improve the signal sensitivity in the statistical fit.
%A preliminary optimisation strategy is presented in Appendix~\ref{sec:app:mtbin:pre}, where a method is proposed to get the optimised binning from a scan on a fine-binned $\mT$ distribution by requiring a maximum significance.
A preliminary optimisation strategy is first proposed to get the optimised binning from a scan on a fine-binned $\mT$ distribution by requiring a maximum significance.
However, as a result, the $\mT$ distributions look quite fluctuating. Some bins have too small bin size and very large statistical uncertainties on the backgrounds. 
Nevertheless, the optimised binning is found to have some tendency that it is regularly divided in logarithmic scale (log-scale).
%This is adopted by the optimisation strategy described in Appendix~\ref{sec:app:mtbin:imp}, which has been developed to improve the optimisation.
This is adopted by the improved optimisation strategy, which has been developed to improve the optimisation.
%The results for the optimised binning are shown in Appendix~\ref{sec:app:mtbin:bin}.
Finally the results for the optimised binning are also shown in this section.

\subsection*{Preliminary optimisation strategy}
%\label{sec:app:mtbin:pre}

Similar to the event selection optimisation method in Section~\ref{sec:sel:opt}, 
significance is a very important quantity which will be required to be at the maximum value with the optimised binning,
and it has been defined earlier as in Equation~\ref{eq:sgn}. 
However, this is a general definition of significance.
To take the binning information into account, it is still necessary to extend the significance definition over the $\mT$ distribution,
as defined in Equation~\ref{eq:sgn1d}.

The first step is to do a mass dependent binning optimisation.
For each mass point of signal, a scan on every $\mT$ bin is performed.
During the scan, when a bin, bin $i$, satisfies $s_{i-1}^{2}+s_{i}^{2}<s_{i-1,i}^{2}$, it will be merged with the previous bin, bin $i-1$.
$s_{i-1}$ and $s_{i}$ are the significance values for bins $i-1$ and $i$, respectively, 
while $s_{i-1,i}$ is the significance value for a merged bin from bin $i-1$ and bin $i$.
If a bin has negative number of events of signal or backgrounds, it will also be merged with the previous bin.
In the optimisation, the last bin will always include overflow events.
At the end of this step, it is supposed to get an optimised binning for each mass point of signal.
However, a mass dependent binning makes the analysis much more complicated.
For such a search analysis, it is necessary to further simplify the binning.
This will be done in the second step.

The second step requires also a scan on the mass points. 
For each mass point of signal, significance as well as the uncertainty of it will be calculated for all the optimised binning that is obtained from the first step.
By a visual comparison of all these significance values and uncertainties in each signal region, 
a binning that gives a large significance but small uncertainty will be chosen to be the optimised binning for all signals.
As with the event selection optimisation, the binning optimisation is done with the NWA signals.

The optimised binning gives much improved upper limits than the binning with a constant bin size.
However, the $\mT$ distributions with the new binnings are found to be fluctuating dramatically in some bins.
An example in the quasi-inclusive signal region with the optimised $\mT$ binning is shown in Figure~\ref{fig:mt-preopt}.
For a better display, the distribution with a binning in log-scale is also shown.
\begin{figure}[!htbp]
\center
\includegraphics[width=0.48\textwidth]{fig/appendix/MT-InclSR-PreOpt-lin.pdf}
\includegraphics[width=0.48\textwidth]{fig/appendix/MT-InclSR-PreOpt-log.pdf}
\caption{$\mT$ distribution with the preliminary optimised binning in the quasi-inclusive SR. The binning is shown in both linear scale (left) and log-scale (right).}
\label{fig:mt-preopt}
\end{figure}

\subsection*{Improved optimisation strategy}
%\label{sec:app:mtbin:imp}

To avoid the fluctuation mentioned above, an improved optimisation strategy is needed.
Despite some bins are fluctuating and have very irregular bin sizes, the bin size appears to be regular and roughly equal when the binning is shown in log-scale, especially in the middle range of $\mT$ where staistics are high enough compared with the tails.

Therefore the improved strategy is based on the distribution of $\log\mT$.
Given that the distributions of signals are always changing from one mass point to another but the backgrounds are always the same,
the distribution of backgrounds is always the main consideration in the optimisation.
The first bin is always fixed to 70--100\GeV, due to the fact that there are not much statistics of backgrounds in this range and no events selected below $\mT=70$\GeV.
In the middle range, 100--1000\GeV, where the backgrounds are mainly distributed, the binning is divided into $N$ bins with a constant $\log\mT$ bin size: $\frac{\log{1000}-\log{100}}{N}=\frac{1}{N}$.
At the high $\mT$ tail, 1000--3000\GeV, the binning is required to have a bin size that is two times larger ($\frac{2}{N}$ in log-scale) than that in the middle range.
$N$ is the parameter to be optimised and is supposed to give the maximum significance.

To get the optimised $N$, a scan is performed on it from a small integer ($\sim$ 5) to a large integer ($\sim$ 60) with a step of unit.
%An example of this can be seen in Figure~\ref{fig:scanN}, where the scan of significance on $N$ in the quasi-inclusive ggF SR for some mass points of the NWA ggF signal is shown. 
%The optimised $N$ in this signal region is chosen to be 14 considering that for most mass points, especially the masses that are not very high (since signals with very high masses are mainly distributed at the tails, where the backgrounds and data have low statistics), the significance has already approached the plateau but with relatively small and acceptable statistical error compared with larger values of $N$. Similar, for both the VBF 1-jet and 2-jet SRs, $N$ is optimised to be 6.
The optimised $N$ is found to be 14 for the ggF SR, and 6 for the VBF 1-jet and 2-jet SRs.
Therefore, finally, the $\mT$ distributions in the signal regions are divided into 18 (8) bins for the ggF quasi-inclusive ($N_{\text{jet}}=1$  and $\geq 2$ VBF) categories.
%\begin{figure}[!htbp]
%\center
%\includegraphics[width=0.85\textwidth]{fig/appendix/ScanN-ggFSR.pdf}
%\caption{Scan of significance on the parameter $N$ in the quasi-inclusive ggF SR. ``RSS'' refers to the significance.}
%\label{fig:scanN}
%\end{figure}

\subsection*{Optimised binning}
%\label{sec:app:mtbin:bin}

The $\mT$ distributions with optimised binnings can be found in Sections~\ref{sec:res:prefit} (Figure~\ref{fig:PreSRMT}) and \ref{sec:res:postfit} (Figure~\ref{fig:PostSRMT}).
The bin boundaries are summarised in Table~\ref{tab:mtbin_lvlv}. 
\begin{table}[htbp]
\center
\caption{Bin boundaries in $\log$ [linear] scale of the $\mT$ distributions used in the fit for the three signal regions are shown.}\label{tab:mtbin_lvlv}
\resizebox{\columnwidth}{!}{
\begin{tabular}{cccccccccc}\hline
\multicolumn{10}{c}{Inclusive ggF SR}\\\hline
% 0 & 100 & 120 & 140 & 160 & 180 & 200 & 220 & 240 & 260 & 280 & 300 & 320 & 340 \\
%360 & 380 & 400 & 450 & 500 & 550 & 600 & 650 & 700 & 800 & 900 & 1000 & 3000 & \\\hline
$\sim 1.8~[70]$ & 2.0~[100] & 2.07~[120] & 2.14~[140] & 2.21~[160] & 2.28~[190] & 2.36~[230] & 2.43~[270] & 2.5~[315] & 2.57~[370] \\
  2.64~[440] & 2.71~[510] & 2.78~[600] & 2.86~[725] & 2.93~[850] & 3.0~[1000] & 3.14~[1380] & 3.28~[1900] & 3.48~[3000] & \\\hline
\end{tabular}
}
\resizebox{\columnwidth}{!}{
\begin{tabular}{ccccccccc}\hline
\multicolumn{9}{c}{$N_{\text{jet}}=1$ and $\geq 2$ VBF SRs}\\\hline
%0 & 100 & 150 & 200 & 250 & 300 & 350 & 400 & 500 & 600 & 3000 \\\hline
$\sim 1.8~[70]$ & 2.0~[100] & 2.17~[150] & 2.33~[215] &  2.5~[315] & 2.67~[470] & 2.83~[680] & 3.0~[1000] & 3.48~[3000] \\\hline
\end{tabular}
}
\end{table}
\FloatBarrier
