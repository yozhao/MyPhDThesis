\chapter{Introduction}
\label{chap:int}

\pagenumbering{arabic}
\setcounter{page}{1} 

The Higgs boson, 
as an elementary particle predicted by the Standard Model (SM) in the early 1960s, 
was discovered in 2012 by the ATLAS~\cite{ATLASHiggsDiscovery} and CMS~\cite{CMSHiggsDiscovery} Collaborations at the Large Hadron Collider (LHC) for the first time in the history of particle physics. 
Therefore the existence of the Higgs field, which was thought to give a particle mass and produce the Higgs boson through spontaneous symmetry breaking, was confirmed.
The measured properties~\cite{atlas:combined-paper-run1,HIGG-2013-17,cms:comb-paper-run1,HIGG-2015-07} of the Higgs boson are within experimental uncertainties, consistent with the SM predictions. 

Nevertheless, the SM is still thought to be an incomplete theory. Many scenarios beyond the SM (BSM) are proposed with an extended Higgs sector~\cite{deFlorian:2016spz,Ivanov:2017dad}. 
In several other extensions to the SM, such as the composite Higgs models~\cite{Agashe:2004rs,Giudice:2007fh} and the warped extra dimensions models~\cite{Randall:1999ee,Agashe:2007zd,Antipin:2007pi,Antipin:2008hj}, diboson vector and tensor resonances are also predicted.

In this thesis, a search for a neutral heavy Higgs boson or other heavy resonances is presented, motivated by the statement mentioned above.
The search uses the data collected by the ATLAS detector at the LHC from the $pp$ collisions at the center-of-mass energy $\sqrt{s}=13\TeV$, corresponding to an integrated luminosity of 36.1\,fb$^{-1}$.


%\section*{Overview of previous public searches and results}
%\label{sec:int:ov}

A previous search for a heavy Higgs boson in the $H\rightarrow WW\rightarrow \ell\nu\ell\nu$ ($\ell=e,\mu$) decay channel was performed by the ATLAS~\cite{Aad:2015agg} experiment based on a data sample with an integrated luminosity of 20.3\,fb$^{-1}$ at $\sqrt{s}=8$\TeV. 
No excess of events beyond the SM background prediction was found. Upper limits were set on the product of the production cross section and the $H\to WW$ branching ratio in three different scenarios: a high-mass Higgs boson with a complex-pole scheme (CPS) lineshape and the width predicted for a SM Higgs boson, one with a narrow width approximation (NWA), and one with an intermediate width.
%For the CPS scenario, the combined observed $95\%$ CL upper limits on $\sigma_{H} \times BR(H\to WW)$ for the ggF production mode range approximately from 4\,pb at $m_{H}=220\GeV$ to 90\,fb at $m_{H}=1\TeV$. For the VBF production mode, the equivalent values are 300\,fb and 40\,fb, respectively. 
%For the NWA scenario, the corresponding limits range approximately from 1.5\,pb at $m_{H}=300\GeV$ to 44\,fb at $m_{H}=1.5\TeV$ for the ggF production mode, and from 220\,fb to 19\,fb for the VBF production mode within the same Higgs mass range. 
%The intermediate-width signal scenario was explored with a width in the range of $0.2\Gamma_{H,SM}\leq\Gamma_{H}\leq{0.8}\Gamma_{H,SM}$, where $\Gamma_{H}$ is the width of the hypothetical particle and $\Gamma_{H,SM}$ is the width of a SM Higgs boson for the same mass. For this scenario, the observed $95\%$ CL upper limits on $\frac{\sigma_{H}}{\kappa^{\prime{2}}} \times BR(H\to WW)$ were also given for the ggF and VBF combined production mode, where $\kappa^{\prime{2}}$ is a single, constant scale factor in the electroweak singlet (EWS) model, by which the production cross-section and partial width of the heavy Higgs boson is assumed to be related to those of the SM Higgs boson. The observed limits, for example, for $\kappa^{\prime{2}}=0.4$, range approximately from 9\,pb at $m_{H}=200\GeV$ to 120\,fb at $m_{H}=1\TeV$.
%
The search was also performed in the $H\to WW\to \ell\nu qq$ decay channel in the same study~\cite{Aad:2015agg}. A combination of the $\ell\nu\ell\nu$ and $\ell\nu qq$ final states gave stronger upper limits. 
%For the CPS scenario, the combined observed $95\%$ CL limits on $\sigma_{H}\times BR(H\to WW)$ range from 990\,fb (230\,fb) at $m_{H}=300\GeV$ to 35\,fb (27\,fb) at $m_{H}=1\TeV$, for the ggF (VBF) production mode, respectively.
%For the NWA scenario, the limits range from 830\,fb (240\,fb) at $m_{H}=300\GeV$ to 22\,fb (6.6\,fb) at $m_{H}=1.5\TeV$, for the ggF (VBF) production mode, respectively.
%For the intermediate-width scenario, the limits, for example, for $\kappa^{\prime{2}}=0.4$, range approximately from 1\,pb at $m_{H}=300\GeV$ to 40\,fb at $m_{H}=1\TeV$, for the ggF and VBF combined production mode.
%
The CMS experiment also published a search for a high-mass scalar decaying into two $W$ bosons or two $Z$ bosons, including the fully leptonic final state~\cite{Khachatryan:2015cwa},
%{cms:ww-paper-run1}, 
using datasets at $\sqrt{s}=7$ and 8\TeV of 5.1\,fb$^{-1}$ and up to 19.7\,fb$^{-1}$ of integrated luminosity, respectively.
The search was performed in the Higgs boson mass range 145--1000\GeV. Except for the SM Higgs boson with $m_{H}=125\GeV$ which was considered as a background process, addtional Higgs bosons with SM-like properties are excluded in the entire search range at the $95\%$ confidence level (CL).
%
A search for heavy resonances in the (bulk) Randall-Sundrum (RS) models in the fully leptonic decays of the $WW$ channel, using a dataset of 4.7\,fb$^{-1}$ at 7\TeV~\cite{EXOT-2012-11}, was reported by the ATLAS experiment. 
No significant excess of events was observed and upper limits on the production cross-section times branching ratio were set for two benchmark models: a spin-2 RS graviton $G^{\ast}$ and the bulk RS graviton $G_{\rm bulk}^{\ast}$. 
The observed lower limits at $95\%$ CL on the masses of the two models are: 1.23\TeV for $G^{\ast}$ and 0.84\TeV for $G_{\rm bulk}^{\ast}$, assuming the coupling strength $k/\bar{M}_{\rm Pl}=0.1$ and $k/\bar{M}_{\rm Pl}=1.0$, respectively, 
where $k$ is the curvature of the warped fifth dimension and $\bar{M}_{\rm Pl}=\frac{M_{\rm Pl}}{\sqrt{8\pi}}$ is the reduced Planck mass.
%
The ATLAS and CMS experiments have obtained limits in the heavy vector triplet (HVT), the bulk RS and some other exotic models, based on other decay modes of the $VV$ channels, $V$ being either a $W$ or a $Z$ boson~\cite{Khachatryan:2014hpa,Khachatryan:2014gha,Aad:2014pha,Khachatryan:2014xja,Aad:2015ufa,Aad:2015owa,EXOT-2014-18,EXOT-2016-01,Sirunyan:2016cao,Sirunyan:2017nrt}. 
%As a result, the RS $G^{\ast}$ model decaying into $WW$ assuming $k/\bar{M}_{\rm Pl}=0.1$ is excluded up to 1.2\TeV, and the decay to $VV$ is excluded up to 1.1\TeV assuming $k/\bar{M}_{\rm Pl}=1$.
%The extended gauge model (EGM) with a spin-1 $W^{\prime}$ boson, the HVT model with $A_{g_{V}=1}$, $A_{g_{V}=3}$ and $B_{g_{V}=3}$, decaying to $VV$, are excluded up to 1.81\TeV, 2.3\TeV, 760\GeV and 2.6\TeV, respectively. A and B are two benchmark models for the HVT hypothesis. In Model A, weakly coupled vector resonances arise from an extension of the SM gauge group, and the branching factions to fermions and gauge bosons are comparable. In Model B, thea heavy vector triplet is produced in a strongly coupled scenario, i.e. in a Composite Higgs model, and fermionic couplings are suppressed. $g_{V}$ represents the coupling strength of the triplet field to the vector bosons.
%An extension of the SM with an additional heavy, CP-even scalar singlet decaying to longitudinally polarised bosons is excluded up to 2.65\TeV. 
%%PS. RS-G: k=0.1: 1.2\TeV, k=1: 760\GeV, 810\GeV, 1.1\TeV
%%PS. EGM-W'$: 1.52\TeV, 1.47\TeV, 1.49\TeV, 1.3-1.5\TeV, 1.81\TeV
%%PS. HVT Ag1: 1.49\TeV, 2.3\TeV Bg3: 1.56\TeV, 2.6\TeV, 2.4\TeV


%\section*{Personal contributions}
%\label{sec:int:pc}

For the sake of a complete description of the analysis and results that are presented in this thesis, not only my own work but also the work from everyone in the working group are discussed.
My personal contributions to the analysis are mainly listed as follows:
\begin{enumerate}
\item Optimisation of event selection in signal region (SR).
\item Analysis of data, which includes background estimation and making cutflows, comparison between data and Monte Carlo (MC) prediction.
\item Estimation of experimental systematic uncertainty and theoretical systematic uncertainty on top and $WW$ backgrounds.
\item Production of input for the statistical treatment.
\item Optimisation of the binning of transverse mass distributions for the statistical analysis.
\end{enumerate}


%\section*{Orgnisation of the thesis}
%\label{sec:int:org}

The main body of the thesis is organised as follows.
Chapter~\ref{chap:mod} gives an introduction to the SM, BSM physics and the signal models that are used in the analysis to interpret the results.
Chapter~\ref{chap:det} presents briefly the LHC and the ATLAS detector, as well as the event reconstruction.
Chapter~\ref{chap:sam} describes the data and MC samples that are used in the analysis.
Chapter~\ref{chap:obj} introduces the definition of the objects or variables that are used in the analysis.
Chapter~\ref{chap:sel} discusses the event selection in the SRs, including the optimisation strategy.
Chapter~\ref{chap:bkg} shows the backgrounds estimation.
Chapter~\ref{chap:sys} discusses the systematic uncertainties.
Chapter~\ref{chap:res} presents the results.
Chapter~\ref{chap:con} provides a conclusion and an outlook of the analysis.
%\begin{description}
%%\item[Chapter~\ref{chap:int}:] Brief introduction to the Higgs boson and the motivation of the research. An overview of previous public searches and results and my personal contributions in the analysis as well as the orgnisation of the thesis are also given in this section. %Sections~\ref{sec:int:org}--\ref{sec:int:pc}.
%\item[Chapter~\ref{chap:mod}:] Introduction to the SM, BSM physics and the signal models that are used in the analysis to interpret the results.
%\item[Chapter~\ref{chap:det}:] Introduction to the LHC and the ATLAS detector, as well as the event reconstruction.
%\item[Chapter~\ref{chap:sam}:] Introduction to the data and Monte Carlo (MC) samples that are used in the analysis.
%\item[Chapter~\ref{chap:obj}:] Introduction to the definition of the objects or variables that are used in the analysis.
%\item[Chapter~\ref{chap:sel}:] Introduction to the event selection in the signal regions (SRs), including the optimisation strategy.
%\item[Chapter~\ref{chap:bkg}:] Introduction to the backgrounds estimation.
%\item[Chapter~\ref{chap:sys}:] Introduction to the systematic uncertainties.
%\item[Chapter~\ref{chap:res}:] Presentation of the results.
%\item[Chapter~\ref{chap:con}:] Conclusion and outlook of the analysis.
%%\item[Appendices:] Auxiliary materials.
%\end{description}
%%Also, the appendices The rest of the thesis is organised as follows:
