\section{Physics beyond the Standard Model}
\label{sec:mod:bsm}

Although the SM perfectly describes all the particles found so far, it is still thought to be an incomplete theory, especially when it comes to a few phenomena that can not be explained by the SM, such as the gravity, dark matter and dark energy, neutrino masses and the asymmetry between matter and antimatter. 
Therefore BSM physics is believed to exist and a lot of efforts have been made to search for it.
There are many theories~\cite{Cornell:2015yxa} that have been brought up and developed in recent decades.
Supersymmetry (SUSY) is a well-known one, in which each particle from one group would have an associated particle in the other, known as its superpartner.
Theorists also predict existences of addtional BSM Higgs boson, such as a SM-like heavy Higgs boson, which is predicted to have very similar properties to the SM Higgs boson but still belongs to an extended scalar sector to the SM.
%Two different scenarios for the SM-like heavy Higgs boson prediction are finally used in the analysis for interpretation. One is the NWA Higgs model, as already discussed in Chapter~\ref{chap:int}, and the other one is a Higgs boson with a large width assumption (LWA).
%Some other exotic BSM signal models are also used to interpret the results in the analysis, and they can be found in Section~\ref{sec:mod:sig}.

%%%

There are 19 free parameters in the SM that could not be calculated a priori and must be determined from experiments. 
The naturalness criterion dictates that the parameters should take values of order 1, and are not fine-tuned.
The mass of the Higgs boson has quadratically divergent loop corrections that correspond to the scale of any new physics, $\Lambda_{\rm cutoff}$.
\begin{equation}
m^2_{H} = m^2_{H, \rm bare} + \Delta m^2_{H} = m^2_{H, \rm bare} + \alpha\Lambda^2_{\rm cutoff}\,.
\end{equation}
The SM is accurate up to the Planck Scale, $\Lambda_{\rm Planck} \sim 10^{19}$\GeV. But in such a case, a fine-tuning between the bare mass and the correction term is needed to fix the observed, $m^2_{H} \simeq 10^4$\GeV. 
This fine-tuning breaks the principle of naturalness and leads to the hierarchy problem: why the weak scale, $\Lambda_{\rm EW}$, so much lower than the Planck scale, $\Lambda_{\rm Planck}$?

To resolve the fine-tuning problem, composite Higgs models~\cite{Dugan:1984hq, AGASHE2005165} suggested a new strongly interacting sector with a larger symmetry group, explaining electroweak symmetry breaking without a fundamental scalar. 
With a careful choice of this expanded symmetry group, spontaneous symmetry breaking, at a scale $\Lambda_{\rm comp} \ll \Lambda_{\rm Planck}$ is able to produce a composite Goldstone boson transforming as the SM Higgs doublet, and an unbroken symmetry that corresponds to the electroweak $SU(2) \times U(1)$ symmetry group. 
In this sense, the global symmetry is also explicitly broken, such as with Yukawa and gauge coupling terms, so that the Goldstone boson that corresponds to the composite Higgs can be massive. 
The approximate symmetry of the new strongly interacting sector also keeps the Higgs mass low, which addresses the problem of naturalness. 
Among the predictions of these models are resonances of composite scalars and new heavy gauge bosons near the $\TeV$ scale.

Another solution for the hierarchy problem is to postulate warped extra dimensions~\cite{PhysRevLett.83.3370,PhysRevLett.83.4690,PhysRevD.63.075004}, also referred to as the RS models. 
According to the RS models, the universe is embedded in a five dimensional space (bulk) with constant negative scalar curvature (anti-de Sitter space). 
The SM particles are localised on a (3 + 1)-dimensional subspace (3-brane), called the weak or TeV brane, whilst there is a separate 3-brane where gravity is relatively strong, called the Planck brane. 
Only gravity is allowed to propagate in the bulk through the extra dimension. 
The five dimensional metric can not be factorized. The flat four-dimensional Minkowski metric, $\eta_{\mu\nu}={\rm diag}(-1,1,1,1)$, has an additional warping factor depending explicitly on the extra dimension, $\phi$, as described in Equation~\ref{eq:ds2} for the space-time interval, $ds^2$, where, $x^{\mu}$ are the familiar four-dimensional space-time coordinates:
\begin{equation}
\label{eq:ds2}
ds^2 = e^{-2kr_c|\phi|}\eta_{\mu\nu}dx^{\mu}dx^{\nu}+r^2_cd\phi^2\,.
\end{equation}
The curvature scale, $k$, is assumed to be of order the Planck scale. The size of the extra dimension is determined by the compactification radius, $r_c$. 
The Planck brane is placed at $\phi = 0$ and the weak brane placed at $\phi = \pi$.
In this way, physical mass parameters on the weak brane accumulate a warping factor with regard to their higher dimensional values, $m = e^{-kr_c\pi}m_0$, whilst the Planck mass on a brane depends only weakly on the fifth dimension. 
Provided that $kr_c \simeq 12$, the observed hierarchy on the weak brane can emerge geometrically from the warped extra dimension, as depicted in Figure~\ref{fig:brane}. 
The RS models predict that Kaluza-Klein (KK) excitations~\cite{Han:1998sg}, from the massless graviton propagating in the extra dimension, are coupled to SM gauge bosons.
The lowest modes are expected near the $\TeV$ scale where their masses and couplings are determined.
\begin{figure}[!htbp]
 \centering
 \includegraphics[width=0.8\textwidth]{fig/phys/Brane.png}
 \caption{In the RS models, the Planck brane and the weak brane are separated by warped extra dimension. The Planck scale on a brane only weakly depends on the extra dimension, whilst the mass scales receive a warping factor~\cite{Gabella06therandall-sundrum}.}
 \label{fig:brane}
\end{figure}

%A minimal solution to provide mass to gauge bosons in the electroweak sector, is spontaneous symmetry breaking in the SM, with a single complex scalar transforming as a weak isospin doublet, in a gauge invariant way. 
%As a result, the minimal Higgs sector of the SM purports to simultaneously address two disparate problems: the weak gauge bosons acquire mass by ``eating'' degrees of freedom from the complex scalar, and all fermions acquire mass by Yukawa couplings placed by hand. 
Spontaneous symmetry breaking has been introduced in previous section in this thesis, as a minimal solution to provide mass to gauge bosons, while the Yukawa coupling is placed for fermions to acquire mass.
However, theories with an extended Higgs sector~\cite{deFlorian:2016spz,Ivanov:2017dad} preserve all of the SM gauge symmetries but split the functions of the SM doublet between multiple doublets, proposing to have even more exotic extensions with scalars that transform as weak isospin singlets or triplets. 
The simplest extensions, known as $N$ Higgs doublet models (NHDM), suggest $N$ complex scalar fields that transform as weak isospin doublets with weak hypercharge, $Y = 1$. 
For instance, with $N = 2$, the two complex scalar doublets can be described by eight real scalars: three generate mass for the weak bosons, three neutral scalars, and two charged Higgs. 
Besides the spread of the Yukawa couplings among the additional doublets, theories with extended Higgs sector also offer mechanisms for the CP violation. And the singlet extension can explain dark matter, while a triplet extension explains neutrino masses without introducing a right-handed neutrino. 
New resonances that are predicted in an extended Higgs sector provide a rich phenomenology through couplings to the massive weak bosons.

Due to the success of the electroweak theory, which unified the electromagnetic and weak forces, so that the two forces could be regarded as two aspects of the same phenomenon, attempts have been made to construct a single theory that is able to unify the four fundamental forces.
In the Grand Unified Theories~\cite{PhysRevD.10.275,PhysRevLett.32.438,doi:10.1063/1.2947450,FRITZSCH1975193} (GUT), the SM gauge group is embedded in a single larger group, therefore the strong and electroweak interactions unify at a large scale, $\Lambda_{\rm GUT}$, manifesting themselves as separate interactions at lower energies. 
%The running of the three gauge couplings shows a curious near convergence, motivating $\Lambda_{\rm GUT} \simeq 10^{16}\GeV$. In addition to the overt goal of unification, GUTs generally offer a mechanism for electric charge quantization, and specifically an explanation for why protons and electrons have exactly opposite charges. Among the challenges of GUTs, the prediction of magnetic monopoles and of proton decay is strongly constrained by experiment. The simplest such theory involves the group $SU(5)$, although there is no single accepted formulation; notably, $SO(10)$ requires no extra fermions, and features each generation transforming in one irreducible multiplet. In GUTs, a large number of new gauge bosons are predicted, corresponding to the generators of the larger symmetry group. Since the larger symmetry is not realized at the electroweak scale, a series of symmetry breaking scales reduces the larger symmetry to the observed SM gauge group, while the gauge bosons of the broken symmetries acquire mass near the symmetry breaking scales.
The ultimate goal of the GUT is to incorporate the gravitational force into the unification by formulating a satisfactory theory of quantum gravitation. This would produce a theory of everything (TOE), and one candidate is string theory.



Several selected benchmark models with different spin values are studied in this thesis and they are briefly described in Section~\ref{sec:mod:sig}.

\FloatBarrier
