\section{The Standard Model}
\label{sec:mod:sm}

The SM~\cite{WNCottinggham:IntroToSM-2nd} of particle physics is the theory that describes three of the four known fundamental forces in the universe --- the electromagnetic, weak and strong interactions. All known elementary particles can be well classified according to the SM.
The SM has been developed during the last century within the context of gauge quantum field theories. Matter and forces are described by means of a reasonably limited number of fields. The quanta of these fields are particles, which at present are believed to be fundamental. The SM has had an enormous success in explaining several phenomena. A fundamental part of the SM is the spontaneous symmetry breaking mechanism responsible for the generation of masses of particles --- the Higgs mechanism. A key prediction of the SM as a consequence of the introduction of this mechanism is the existence of a massive scalar boson --- the Higgs boson. This prediction has been proved in 2012 - 2013 by the discovery and measurements of a new boson particle performed by the ATLAS and CMS experiments. The characteristics of the new particle are compatible with those predicted for the SM Higgs boson. 

%%

\subsection{Elementary particles}

In the SM, the elementary particles are grouped into 4 parts: quarks, leptons, gauge bosons and the scalar Higgs boson. There are three basic properties that are used to describe an elementary particle: mass, charge and spin. Each property is assigned a number value, which can not be changed. Figure~\ref{fig:sm-particles} shows these elementary particles in the SM.
An elementary particle can be a fermion or a boson. 
Fermions are the building blocks of matter and have mass, while bosons behave as force carriers for fermion interactions. 
%and some of them have no mass. 
Fermions all have spin $\frac{1}{2}$, in unit of $\hbar$, and can be grouped into two types: leptons and quarks.
Bosons can also be divided into two types: gauge bosons and scalar bosons.
Every elementary particle has its own antiparticle.
An antiparticle usually has the same mass, opposite electric charge and the same spin state with the elementary particle.
For some particles, their antiparticles are themselves, like the $Z$ boson and the photon.
%neutrinos.

\begin{figure}[!htbp]
 \centering
 \includegraphics[width=0.6\textwidth]{fig/phys/sm-particles.png}
 \caption{Elementary particles in the SM.}
 \label{fig:sm-particles}
\end{figure}

Leptons interact only through the electromagnetic interaction if they are charged and the weak interaction no matter whether they are charged or not. 
There are three generations of leptons that have been found so far. 
%There are six flavours of leptons, with each generation including two favours, of which one is a charged lepton and the other one is called neutrino which is not charged. The mass of leptons increases as the generation is higher.
%For example, 
The first generation of leptons is composed of the electron ($e^{-}$) and electron neutrino ($\nu_{e}$), and it is the lightest generation.
The second generation of leptons includes the muon ($\mu^{-}$) and muon neutrino ($\nu_{\mu}$)
while the third generation includes the tau ($\tau^{-}$) and tau neutrino ($\nu_{\tau}$) and it is the heaviest generation.
Neutrinos have much smaller mass than the charged leptons in each generation of leptons.
And neutrinos are believed to oscillate between different flavours in flight. For example, an electron neutrino that is produced in a beta decay~\cite{CWN:IntroNuclearPhysics-1986} reaction may interact in a distant detector as a muon or tau neutrino.
Usually in high energy collision physics experiments, when we talk about leptons, neutrinos are commonly not counted, due to the fact that they are not observable in the detectors, and they are reconstructed in form of missing energy (more discussion about this can be seen in Section~\ref{sec:obj:met}).

Quarks interact through the electromagnetic and weak interactions and also the strong interactions, and quarks have also six flavours that are also grouped into three generations. 
Similar to leptons, the first generation is the lightest and the third one is the heaviest.
The first generation is composed of the up quark ($u$) and the down quark ($d$).
The second generation includes the charm quark ($c$) and the strange quark ($s$).
And the third generation is made up of the top quark ($t$) and the bottom quark ($b$).
Quarks $u$, $c$ and $t$ all carry $\frac{2}{3}$ charge, while $d$, $s$ and $b$ quarks have $-\frac{1}{3}$ charge.
A hadron is a composite particle that is usually made of 2 or 3 quarks bounded together by the strong force.
And hadrons can be grouped into baryons (made of three quarks) and mesons (made of a quark and an antiquark).
For example, protons and neutrons are both hadrons, and a proton is composed of two $u$ quarks and a $d$ quark, while a neutron is formed by $udd$ quarks. Pions ($\pi^{+}:u\bar{d}$, $\pi^{0}:u\bar{u}$ or $d\bar{d}$, $\pi^{-}:d\bar{u}$) are an example of mesons.

Gauge bosons include the $W$ boson, the $Z$ boson, gluons and photons. 
The $W$ boson can be either a $W^{-}$ or a $W^{+}$ and they are each other's antiparticles.
The $Z$ boson carries no charge and is its own antiparticle.
The $W$ and $Z$ bosons are known as the intermediate vector bosons for the weak interaction.
A $W$ boson can decay leptonically to a charged lepton and a neutrino
%a lepton (neutrinos are not considered as leptons in this thesis) and a neutrino 
or hadronically to a quark and an antiquark.
A $Z$ boson can decay to a fermion and its antiparticle. The decay can be either leptonic or hadronic.
Gluons and photons both have no mass and carry no charge, and they are also regarded as antiparticles to themselves.
In quantum chromodynamics (QCD)~\cite{Greiner:QCD-3rd}, gluons act as the exchange particles between quarks in the form of the strong interaction.
In QCD, quarks carry three types of color charge (red: $r$, green: $g$ and blue: $b$), while antiquarks carry three types of anticolor charge ($\bar{r}$, $\bar{g}$ and $\bar{b}$).
There are eight independent types of gluon, known as the eight gluon colors, that are eight mixed states of the colors and anticolors.
Photons are the force carrier for the electromagnetic force, massless and always travels at the speed of light within a vacuum.

In the SM, scalar bosons are bosons that have zero spin. So far, the only scalar boson that has been found is the Higgs boson, with a mass around 125\GeV, in the SM, with no electric or color charge.
The Higgs boson is a quantum excitation of one of the four components of the Higgs field (see Section~\ref{sec:mod:sm:higgs}).
And it is a very unstable particle decaying into other particles almost immediately.
The decay branching ratios of the Higgs boson are shown in Figure~\ref{fig:HBR}.
\begin{figure}[!htbp]
 \centering
 \includegraphics[width=0.48\textwidth]{fig/phys/HiggsBR200GeV.jpg}
 \caption{Standard Model Higgs boson decay branching ratios at different mass points.}
 \label{fig:HBR}
\end{figure}

According to the SM, the most important processes for the production of the Higgs boson are as follows:
%a Higgs boson can be produced 
%as follows:
%in the four main processes:
\begin{description}
\item[Gluon gluon fusion (ggF):] the easiest way to produce a Higgs boson, since there is a loop of virtual quarks (especially for heavy quarks $t$ and $b$) in the production and the coupling of them to the Higgs boson is proportional to their masses.
\item[Vector boson fusion (VBF):] the Higgs boson is emitted by a virtual $W$ or $Z$ boson that is exchanged between two colliding (anti-)fermions.
\item[Higgs Strahlung (VH):] when a fermion collides with an anti-fermion, they are supposed to merge into a virtual $W$ or $Z$ boson that afterwards can emit a Higgs boson.
\item[Top fusion (ttH):] two gluons collide and both decay into a quark-antiquark pair, and from each pair a quark and an antiquark can then combine to form a Higgs boson.
\end{description}
The Feynman diagrams for these production modes are summarised in Figure~\ref{fig:HProd}.
At the LHC, the ggF process is dominant in the production of Higgs bosons, and the VBF process is the second most important. VH is the third largest process and ttH is the smallest. The cross section of the processes for Higgs production at $\sqrt{s}=13$\TeV is shown in Figure~\ref{fig:HXS}. The cross section for the ggF production mode is shown in blue color, and the VBF, VH and ttH modes are shown in red, green and purple, respectively.
\begin{figure}[!htbp]
 \centering
 \includegraphics[width=0.55\textwidth]{fig/phys/HiggsProdFeynman.png}
 \caption{Feynman diagrams for the production of the Standard Model Higgs boson: ggF (top left), ttH (top right), VH (bottom left) and VBF (bottom right).}
 \label{fig:HProd}
\end{figure}

\begin{figure}[!htbp]
 \centering
 \includegraphics[width=0.65\textwidth]{fig/phys/HXS13TeV.pdf}
 \caption{\small SM-like Higgs boson production cross sections as a function of mass. ``NLO QCD'' and ``NNLO QCD'' refer to next-to-leading order and next-to-next-to-leading order QCD calculations, respectively. ``NNLO+NNLL'' means that the logarithmically enhanced contributions due to multiple soft emissions have been resummed up to next-to-next-to-leading logarithmic accuracy and the result has been consistently matched to the fixed order NNLO result\cite{Grazzini:2012mi}.}
 \label{fig:HXS}
\end{figure}

%The Dirac equation for a charged massive fermion predicts, correctly, the existance of an antiparticle of the same mass and spin, but opposite charge, and opposite magnetic moment relative to the direction of the spin. Of the charged leptons, only the electron $e^{-}$ carrying charge -e and its antiparticle $e^{+}$, are stable. The muon $\mu^{-}$ and tau $\tau^{-}$ and their antiparticles, the $\mu^{+}$ and $\tau^{+}$, differ from the electron and positron only in their masses and their finite lifetimes. It is believed to be true of all interactions that the leptons preserve electric charge --- conservation of electric charge, and a lepton can change only to another of the same type, and a lepton and antilepton of the same type can only be created or destroyed together --- conservation of lepton number, antileptons being counted negatively.
%
%Quarks, like leptons, are spin $\frac{1}{2}$ Dirac fermions, but the electric charge they carry are $\frac{2}{3}e$, $-\frac{1}{3}e$. Quarks carry quark number, antiquarks being counted negatively. The net quark number of an isolated system has never been observed to change. However, the number of different flavours of quarks are not separately conserved: changes are possible through the weak interaction. An isolated quark has also never been observed. Quarks are always confined in compound systems that extend over distances of about 1 fm. The most elementary quark systems are baryons which have net quark number three and mesons which have net quark number zero. In particular, the proton ($uud$) and neutron ($udd$) are baryons. Mesons are essentially a quark and an antiquark, bound transiently by the strong interaction field. The term hadron is used generically for a quark system. Hadron states containing quarks can be classified using the concept of isospin. For example, the $u$ and $d$ quarks are regarded as a doublet of states $\ket{u}$ and $\ket{d}$, with $I = \frac{1}{2}$ and $I_{3} = +\frac{1}{2}$, $-\frac{1}{2}$, respectively. The total isospin of a baryon made up of three $u$ or $d$ quarks is then $I = \frac{3}{2}$ or $I = \frac{1}{2}$. The $I = \frac{1}{2}$ states make up doublets, like the proton and  neutron, having charges $e$($uud$) and 0($udd$).

%%

\subsection{Fundamental interactions}

There are four types of fundamental interactions that have been distinguished so far in nature. On the scales of particle physics, gravitional forces are insignificant, and 
usually neglected in the SM.
%therefore excluded from the SM.
Nevertheless the four forces are described briefly as follows:
\begin{description}
\item[Electromagnetic interaction:] photons are the quanta of this force field and force carriers between charged fermions.
\item[Weak interaction:] $W$ and $Z$ bosons are the quanta of this force field. Based on the uncertainty principle, it is a short ranged force, with a range of approximately $10^{-3}$ fm.
\item[Strong interaction:] the quanta are gluons. It also has a short interaction range, approximately 1 fm, due to the fact that the gluon fields are always confining.
\item[Gravitional force:] known as gravity, which is the weakest in the four fundamental forces, and has no significant effect at the level of particle physics. The range of interaction is infinite, but the force becomes weaker as the range increases.
\end{description}

%%%
%
%\subsection{Electroweak theory}
%
%Both the electromagnetic force and the weak force are described by the electroweak (EW) theory, though the two forces are quite different.
%
%The EW theory~\cite{PaulL:IntroSMEW} is actually based on the SU(2) $\times$ U(1) Lagrangian
%\begin{equation}
%\mathcal{L}_{SU(2) \times U(1)} = \mathcal{L}_{gauge} + \mathcal{L}_{\Phi} + \mathcal{L}_{f} + \mathcal{L}_{Yuk}\,.
%\end{equation}
%
%The gauge part is 
%\begin{equation}
%\mathcal{L}_{gauge} = -\frac{1}{4}W^{i}_{\mu\nu}W^{\mu\nu i}-\frac{1}{4}B_{\mu\nu}B^{\mu\nu}\,,
%\end{equation}
%where $W^{i}_{\mu}, i = 1, 2, 3$ and $B_{\mu}$ are respectively the $SU(2)$ and $U(1)$ gauge fields, with field strength tensors
%\begin{equation}
%\begin{aligned}
%    B_{\mu\nu}={} & \partial_{\mu}B_{\nu}-\partial_{\nu}B_{\mu} \\
%W^{i}_{\mu\nu}={} & \partial_{\mu}W^{i}_{\nu}-\partial_{\nu}W^{i}_{\mu}-g\varepsilon_{ijk}W^{j}_{\mu}W^{k}_{\nu} \,,
%\end{aligned}
%\end{equation}
%where $g(g')$ is the $SU(2)$ ($U(1)$) gauge coupling and $\varepsilon_{ijk}$ is the totally antisymmetric symbol. The $SU(2)$ fileds have three and four-point self-interactions.
%$B$ is a $U(1)$ field associated with the weak hypercharge $Y=Q-T^{3}$, where $Q$ and $T^{3}$ are respectively the electric charge operator and the third component of weak $SU(2)$. It has no self-interactions. The $B$ and $W^{3}$ fields will eventually mix to form the photon and $Z$ boson.
%
%The scalar part of the Lagrangian is
%\begin{equation}
%\mathcal{L}_{\Phi}=(D^{\mu}\Phi)^{\dagger}D_{\mu}\Phi-V(\Phi) \,,
%\end{equation}
%where 
%%$\Phi=\binom{\Phi^{+}}{\Phi^{0}}$ 
%$\Phi=\begin{pmatrix}\Phi^{+}\\\Phi^{0}\end{pmatrix}$ 
%is a complex Higgs scalar, which is a doublet under $SU(2)$ with $SU(1)$ charge $y_{\Phi}=+\frac{1}{2}$.
%The gauge covariant derivative is
%\begin{equation}
%D_{\mu}\Phi=(\partial_{\mu}+ig\frac{\tau^{i}}{2}W^{i}_{\mu}+\frac{ig'}{2}B_{\mu})\Phi \,,
%\end{equation}
%where the $\tau^{i}$ are the Pauli matrices. The square of the covariant derivative leads to three and four-point interactions between the gauge and scalar fields.
%$V(\Phi)$ is the Higgs potential. The combination of $SU(2)\times U(1)$ invariance and renormalizability restricts $V$ to the form
%\begin{equation}
%V(\Phi) = +\mu^{2}\Phi^{\dagger}\Phi+\lambda(\Phi^{\dagger}\Phi)^{2} \,.
%\end{equation}
%For $\mu^{2}<0$ there will be a spontaneous symmetry breaking. The $\lambda$ term describes a quartic self-interaction between the scalar fields. Vacuum stability requires $\lambda > 0$.
%
%$\mathcal{L}_{f}$ is the fermion term. %
%The standard model is anomaly free for the assumed fermion content. There are no $SU(3)^{3}$ anomalies because the quark assignment is non-chiral, and no $SU(2)^{3}$ anomalies because the representations are real. The $SU(2)^{2}Y$ and $Y^{3}$ anomalies cancel between the quarks and leptons in each family, by what appears to be an accident. The $SU(3)^{2}Y$ and $Y$ anomalies cancel between the $L$ and $R$ fields, ultimately because the hypercharge assignments are made in such a way that $U(1)_{Q}$ will be non-chiral.
%
%The last term is 
%\begin{equation}
%\mathcal{L}_{Yuk} = -\sum\limits^{F}_{m,n=1}
%[\Gamma^{u}_{mn}\bar{q}^{0}_{mL}\tilde{\Phi}u^{0}_{nR} + \Gamma^{d}_{mn}\bar{q}^{0}_{mL}\Phi d^{0}_{nR} + 
%\Gamma^{e}_{mn}\bar{l}^{0}_{mn}\Phi e^{0}_{nR} + \Gamma^{\nu}_{mn}\bar{l}^{0}_{mL}\tilde{\Phi}\nu^{0}_{nR}]
%+ h.c. \,,
%\end{equation}
%where the matrices $\Gamma_{mn}$ describe the Yukawa couplings between the single Higgs doublet, $\Phi$, and the various flavours $m$ and $n$ of quarks and leptons. One needs representations of Higgs fields with $y=+\frac{1}{2}$ and $-\frac{1}{2}$ to give masses to the down quarks and electrons $(+\frac{1}{2})$, and to the up quarks and neutrinos $(-\frac{1}{2})$. The representation $\Phi^{\dagger}$ has $y=-\frac{1}{2}$, but transforms as the $2^{\ast}$ rather than the 2. However, in $SU(2)$ the $2^{\ast}$ representation is related to the 2 by a similarity transformation, and $\tilde{\Phi}\equiv i\tau^{2}\Phi^{\dagger}=\begin{pmatrix}\Phi^{0^{\dagger}}\\-\Phi^{-}\end{pmatrix}$ transforms as a 2 with $y_{\tilde{\Phi}}=-\frac{1}{2}$. All of the masses can therefore be generated with a single Higgs doublet if one makes use of both $\Phi$ and $\tilde{\Phi}$. The fact that the fundamental and its conjugate are equivalent does not generalise to higher unitary groups. Furthermore, in supersymmetric extensions of the standard model the supersymmetry forbids the use of a single Higgs doublet in both ways in the Lagrangian, and one must add a second Higgs doublet. Similar statements apply to most theories with an additional $U(1)^{\prime}$ gauge factor, i.e., a heavy $Z^{\prime}$ boson.
%
%%%
%
%\subsection{Spontaneous symmetry breaking}
%
%Gauge invariance (and therefore renormalizability) does not allow mass terms in the Lagrangian for the gauge bosons or for chiral fermions. 
%Massless gauge bosons are not acceptable for the weak interactions, which are known to be short-ranged. 
%Hence, the gauge invariance must be broken spontaneously, which preserves the renormalizability. 
%The lowest energy (vacuum) state does not respect the gauge symmetry and induces effctive masses for particles propagating through it.
%
%%\begin{equation}
%%
%%\end{equation}

%%

\subsection{The Higgs mechanism}
\label{sec:mod:sm:higgs}

%In the SM, the Higgs mechanism is a very important and essential theory to explain why gauge bosons have masses.
%To describe the Higgs mechanism, the Higgs field, which is a quantum field and permeats everywhere in the universe, is introduced by the SM.
%The field will cause spontaneous symmetry breaking below the critical temperature~\cite{Dobado:2017xxb}, which thus triggers the Higgs mechanism and gives masses to the bosons that participate in the interactions, specifically for the mass generation of $W$ and $Z$ bosons through electroweak symmetry breaking.
%Fermions can also acquire masses from interactions with the Higgs field, but not in the same way with bosons.
%
%The Higgs field is an scalar $SU(2)$ doublet, with a weak hypercharge ($U(1)$) of 1. It is always accompanied by the Higgs boson particle in order to interact with and give masses to other particles.
%%
%Under $U(1)$ rotations, it is multiplied by a phase, which thus mixes the real and imaginary parts of the complex spinor into each other---combining to the standard two component complex representation of the group $U(2)$.
%The Higgs field, through the interactions specified (summarized, represented, or even simulated) by its potential, induces spontaneous breaking of three out of the four generators (``directions'') of the gauge group $U(2)$. This is often written as $SU(2) × U(1)$, (which is strictly speaking only the same on the level of infinitesimal symmetries) because the diagonal phase factor also acts on other fields in particular quarks. Three out of its four components would ordinarily amount to Goldstone bosons, if they were not coupled to gauge fields.
%However, after the symmetry breaking, these three of the four degrees of freedom in the Higgs field mix with the $W$ and $Z$ bosons and are only observable as components of these weak bosons, which are now massive; while the one remaining degree of freedom becomes the Higgs boson.


The central question of electroweak physics is: ``Why are the $W$ and $Z$ boson masses non-zero?''.
To express this mathematically, a $U(1)$ gauge theory with a single gauge field is considered~\cite{Dawson:1998yi}, known as the {\it Abelian Higgs Model}.
The Lagrangian is simply
\begin{equation}
\mathcal{L}=-\frac{1}{4}F_{\mu\nu}F^{\mu\nu}\,,
\end{equation}
where
\begin{equation}
F_{\mu\nu}=\partial_{\nu}A_{\mu}-\partial_{\mu}A_{\nu}\,.
\end{equation}
The statement of local $U(1)$ gauge invariance is that the Lagrangian is invariant under the transformation:$A_{\mu}(x)\to A_{\mu}(x)-\partial_{\mu}\eta(x)$ 
for any $\eta$ and $x$. Suppose we now add a mass term for the gauge boson to the Lagrangian,
\begin{equation}
\mathcal{L}=-\frac{1}{4}F_{\mu\nu}F^{\mu\nu}+\frac{1}{2}m^{2}A_{\mu}A^{\mu}\,.
\end{equation}
It is easy to see that the mass term violates the local gauge invariance. It is thus $U(1)$ gauge invariance which requires the gauge boson to be massless.

We now extend the model by adding a single complex scalar field with charge $-e$ which couples to the gauge boson. The Lagrangian is now,
\begin{equation}
\mathcal{L}=-\frac{1}{4}F_{\mu\nu}F^{\mu\nu}+|D_{\mu}\phi|^{2}-V(\phi)\,,
\end{equation}
where
\begin{equation}
\begin{aligned}
D_{\mu}={}&\partial_{\mu}-ieA_{\mu} \\
V(\phi)={}&\mu^{2}|\phi|^{2}+\lambda(|\phi|^{2})^{2}\,.
\end{aligned}
\end{equation}
$V(\phi)$ is the most general renormalizable potential allowed by the $U(1)$ gauge invariance.

This Lagrangian is invariant under global $U(1)$ rotations, $\phi\to{e^{i\theta}}\phi$, and also under local gauge transformations:
\begin{equation}
\begin{aligned}
A_{\mu(x)}\to	{}&A_{\mu}(x)-\partial_{\mu}\eta(x) \\
\phi(x)\to	{}&e^{-ie\eta(x)}\phi(x) \,.
\end{aligned}
\end{equation}

There are two possibilities for the theory\footnote{We assume $\lambda>0$. If $\lambda<0$, the potential is unbounded from below and has no state of minimum energy.}. If $\mu^{2}>0$ the potential has the shape shown in Figure~\ref{fig:hat1} and preserves the symmetries of the Lagrangian. The state of lowest energy is that within $\phi =0$, the vacuum state.
The theory is simply quantum electrodynamics with a massless gauge boson and a charged scalar field $\phi$ with mass $\mu$.
\begin{figure}[!htbp]
 \centering
 \includegraphics[width=0.5\textwidth]{fig/phys/ScalarPotential1.png}
 \caption{Scalar potential with $\mu^{2}<0$.}
 \label{fig:hat1}
\end{figure}

%The alternative scenario is more interesting. 
%In this 
In the alternative case $\mu^{2}<0$ and the potential can be written as,
\begin{equation}
V(\phi)=-|\mu^{2}||\phi|^{2}+\lambda(|\phi|^{2})^{2}\,,
\end{equation}
which has the Mexican hat shape shown in Figure~\ref{fig:hat2}. In this case the minimum energy state is not at $\phi =0$ but rather at
\begin{equation}
\langle\phi\rangle=\sqrt{-\frac{\mu^{2}}{2\lambda}}\equiv\frac{\upsilon}{\sqrt{2}}\,.
%<\phi>=\sqrt{-\frac{\mu^{2}}{2\lambda}}\equiv\frac{\nu}{\sqrt{2}}\,.
\end{equation}
$\langle \phi \rangle$
%$<\phi>$ 
is called the vacuum expectation value (VEV) of $\phi$. Note that the direction in which the vacuum is chosen is arbitrary, but it is conventional to choose it to lie along the direction of the real part of $\phi$. The VEV then clearly breaks the global $U(1)$ symmetry.
\begin{figure}[!htbp]
 \centering
 \includegraphics[width=0.5\textwidth]{fig/phys/ScalarPotential2.png}
 \caption{Scalar potential with $\mu^{2}<0$.}
 \label{fig:hat2}
\end{figure}

It is convenient to rewrite $\phi$ as
\begin{equation}
\phi\equiv\frac{1}{\sqrt{2}}e^{i\frac{\chi}{\upsilon}}(\upsilon+h)\,,
%\phi\equiv\frac{1}{\sqrt{2}}e^{i\frac{\chi}{\nu}}(\nu+h)\,,
\label{eq:phi}
\end{equation}
where $\chi$ and $h$ are real fields which have no VEVs.
If we substitute Equation~\ref{eq:phi} back into the original Lagrangian, the interactions in terms of the fields with no VEVs can be found,
\begin{equation}
\begin{aligned}
\mathcal{L}={}&-\frac{1}{4}F_{\mu\nu}F^{\mu\nu}-e{\upsilon}A_{\mu}\partial^{\mu}\chi+\frac{e^{2}\upsilon^{2}}{2}A_{\mu}A^{\mu} \\
%\mathcal{L}={}&-\frac{1}{4}F_{\mu\nu}F^{\mu\nu}-e{\nu}A_{\mu}\partial^{\mu}\chi+\frac{e^{2}\nu^{2}}{2}A_{\mu}A^{\mu} \\
&+\frac{1}{2}(\partial_{\mu}h\partial^{\mu}h+2\mu^{2}h^{2})+\frac{1}{2}\partial_{\mu}\chi\partial^{\mu}\chi \\
&+(h, \chi \text{interactions})\,.
\end{aligned}
\label{eq:lag}
\end{equation}
Equation~\ref{eq:lag} describes a theory with a gauge boson of mass 
$M_{A}=e\upsilon$, 
%$M_{A}=e\nu$, 
a scalar field $h$ with mass-squared $-2\mu^{2}>0$ and a massless scalar field $\chi$.
The mixed $\chi - A$ term is confusing, however, this term can be removed by making a gauge transformation:
\begin{equation}
A^{\prime}_{\mu}\equiv A_{\mu}-\frac{1}{e\upsilon}\partial_{\mu}\chi\,.
%A^{\prime}_{\mu}\equiv A_{\mu}-\frac{1}{e\nu}\partial_{\mu}\chi\,.
\label{eq:trans}
\end{equation}
After making the gauge transformation of Equation~\ref{eq:trans}, the $\chi$ field disappers from the theory and we say that it has been ``eaten'' to give the gauge boson mass. This is called the Higgs mechanism and the $\chi$ field is often called a 
Goldstone boson.
In the gauge of Equation~\ref{eq:trans}, the particle content of the theory is apparent; a massive gauge boson and a scalar field $h$, which we also call a Higgs boson.
The Higgs mechanism can be summarized by saying that the spontaneous breaking of a gauge theory by a non-zero VEV results in the disappearance of a Goldstone boson and its transformation into the longitudinal component of a massive gauge boson.


The simple Abelian $U(1)$ gauge theory discussed above describes actually electromagnetism, and the gauge boson that becomes massive through spontaneous symmetry breaking is the photon. However, in nature photons are generally massless particles, thus the $U(1)$ symmetry is unbroken.

The Higgs mechanism can also be described by non-Abelian gauge theories, such as the {\it Weinberg-Salam Model}, which is an $SU(2)_L \times U(1)_Y$ gauge theory and describes the electroweak interaction.
In this model, the Higgs mechanism has its most important application and allows $W$ and $Z$ bosons to be massive.
The model contains three $SU(2)_L$ gauge bosons, $W^i_{\mu},i=1,2,3$, and one $U(1)_Y$ gauge boson, $B_{\mu}$, with kinetic energy terms,
\begin{equation}
\mathcal{L}_{\rm KE} = -\frac{1}{4}W^i_{\mu\nu}W^{\mu\nu i} -\frac{1}{4}B_{\mu\nu}B^{\mu\nu} \,,
\end{equation}
where
\begin{equation}
\begin{aligned}
W^i_{\mu\nu}={}& \partial_\nu W^i_{\mu} - \partial_\mu W^i_{\nu} + g\epsilon^{ijk}W^j_{\mu}W^k_{\nu}\,, \\
B_{\mu\nu}  ={}& \partial_{\nu}B_{\mu} - \partial_{\mu}B_{\nu}\,.
\end{aligned}
\end{equation}

The gauge fields are coupled to a complex scalar $SU(2)$ doublet, $\Phi = \begin{pmatrix}\phi^{+}\\ \phi^{0}\end{pmatrix}$, and a scalar potential:
\begin{equation}
V(\Phi)=\mu^2 |\Phi^{\dagger}\Phi| + \lambda \left( |\Phi^{\dagger}\Phi|\right)^2 \,,
\end{equation}
where $\lambda > 0$.
As with the Abelian model, the state of minimum energy for $\mu^2 < 0$ is not at $\Phi = 0$ and the scalar field develops a VEV.

We can arbitrarily choose
\begin{equation}
\langle\Phi\rangle = \frac{1}{\sqrt{2}} \begin{pmatrix}0\\\upsilon\end{pmatrix} \,,
\end{equation}
and with this choice, the scalar doublet has $U(1)_Y$ charge (hypercharge) $Y_{\Phi}=1$ and the electromagnetic charge is
\begin{equation}
Q=\frac{\tau_3+Y}{2} \,.
\end{equation}
Consequently,
\begin{equation}
Q\langle\Phi\rangle=0
\end{equation}
and electromagnetism is unbroken by the scalar VEV.

The contribution of the scalar doublet to the Lagrangian is
\begin{equation}
\mathcal{L}_s = (D^{\mu}\Phi)^{\dagger}(D_{\mu}\Phi) - V(\Phi) \,,
\end{equation}
where 
\begin{equation}
D_{\mu} = \partial_{\mu} + i\frac{g}{2}\tau\cdot W_{\mu} + i\frac{g^{\prime}}{2}B_{\mu}Y \,.
\end{equation}
Since in unitary gauge there are no Goldstone bosons and only the physical Higgs scalar remains in the spectrum after the spontaneous symmetry breaking has occurred, the scalar doublet in unitary gauge can be writen as
\begin{equation}
\Phi = \frac{1}{\sqrt{2}} \begin{pmatrix}0\\\upsilon+h\end{pmatrix} \,,
\end{equation}
which gives contributions to the gauge boson masses from the scalar kinetic energy term of the Lagrangian,
\begin{equation}
\frac{1}{2}(0,\upsilon)\left(\frac{1}{2}g\tau\cdot W_{\mu}+\frac{1}{2}g^{\prime}B_{\mu}\right)^2\begin{pmatrix}0\\\upsilon\end{pmatrix} \,.
\end{equation}
The physical gauge fields are therefore two charged fields, $W^{\pm}$, and two neutral gauge bosons $Z$ and $\gamma$.
\begin{equation}
\begin{aligned}
W^{\pm}_{\mu} ={}& \frac{1}{\sqrt{2}} (W^1_{\mu}\mp iW^2_{\mu}) \\
Z^{\mu} ={}& \frac{-g^{\prime}B_{\mu}+gW^3_{\mu}}{\sqrt{g^2+{g^{\prime}}^2}} \\
\gamma^{\mu} ={}& \frac{gB_{\mu}+g^{\prime}W^3_{\mu}}{\sqrt{g^2+{g^{\prime}}^2}} \,.
\end{aligned}
\end{equation}
Thus the gauge bosons can obtain masses from the Higgs mechanism:
\begin{equation}
\begin{aligned}
M_W ={}& \frac{g\upsilon}{2} \\
M_Z ={}& \frac{\sqrt{g^2+{g^{\prime}}^2}\upsilon}{2} \\
M_{\gamma} ={}& 0 \,.
\end{aligned}
\end{equation}

Since the massless photon must couple with electromagnetic strength, $e$, the weak mixing angle $\theta_W$ is defined by the coupling constants,
\begin{equation}
\begin{aligned}
e ={}& g\sin\theta_W \\
e ={}& g^{\prime}\cos\theta_W \,.
\end{aligned}
\end{equation}

%%

\subsection{Proton-proton interaction and parton distribution function}
\label{sec:sm:pdf}

The cross section~\cite{Andreev:2017vxu} of the proton-proton interactions can be expressed by:
\begin{equation}
\sigma = \sum\limits_{i,j}\int dx_1dx_2f_i(x_1,\mu_F,Q^2)f_j(x_2,\mu_F,Q^2)\hat{\sigma}_{i,j}(x_1,x_2,\mu_R,\mu_F)\,,
\end{equation}
where, $f$ is the parton distribution function (PDF), defined as the probability density for finding a parton with a certain longitudinal momentum fraction $x$ at resolution scale $Q^2$, $f(x,Q^2)$.
The partonic cross section, $\hat{\sigma}_{ij}$, usually depends on the strong coupling constant ${\alpha}_s$, which is a function of the renormalisation scale, and its calculation also depends on the factorisation scale:
\begin{equation}
\hat{\sigma}_{i,j} = \sum\limits_{n} \alpha_{s}^{n} (\mu_{R}) \hat{\sigma}^{(n)}(x_1,x_2,\mu_{R},\mu_{F})\,.
\end{equation}
where $n$ is the order of the perturbation theory, and $\hat{\sigma}^{(n)}$ is the hard coefficient. 
The factorisation scale, ${\mu}_F$, is usually thought to be the scale separating long- and short-distance interactions. Below the scale, perturbation theory is no longer reliable, and the soft and collinear divergences could be absorbed into the PDF.
The renormalisation scale, ${\mu}_R$, is a point chosen to define the renormalised quantities in case of an infinite theoretical calculation (though in principle the prediction of the theory should be independent of the choice of ${\mu}_R$).

%Parton distribution functions (PDFs)~\cite{Gao:2017yyd}
%of the proton are essential for making theoretical predictions for experiments at the LHC and other high-energy hadron colliders.
%It is defined as the probability density for finding a particle with a certain longitudinal momentum fraction $x$ at resolution scale $Q^2$, $f(x,Q^2)$.
PDF is usually driven by low-scale non-perturbative dynamics, and can not be computed from first principles, at least with current technology,
and therefore they are determined using experimental data from a variety of hard-scattering cross sections in lepton-proton and proton-proton collisions.

PDFs and their associated uncertainties play a decisive role in several LHC applications.
PDF uncertainties also affect the production of new high-mass resonances, as predicted by many BSM scenarios~\cite{Beenakker:2015rna}, since they probe PDFs at large values of the momentum fraction $x$, for which current experimental constraints are scarce.

The most generally used PDF sets in ATLAS are CT10~\cite{Lai:2010vv} (or CT14~\cite{ct14}), NNPDF3.0~\cite{NNPDF30} and MMHT2014~\cite{mmht2014}.
Under some well-specified conditions, PDF sets can be statistically combined into an unified set, such as PDF4LHC15 sets~\cite{Butterworth:2015oua}, which combine the three sets using the Monte Carlo method~\cite{Watt:2012tq}, and are subsequently reduced to small number of Hessian eigenvectors~\cite{Gao:2013bia,Carrazza:2015aoa} or Monte Carlo replicas~\cite{Carrazza:2015hva} to facilitate phenomenological applications.

\FloatBarrier
