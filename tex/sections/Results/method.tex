\section{General statistics methodology}
\label{sec:res:method}

The statistical method used in this analysis to interpret the results is same as that used in Ref.~\cite{Aad:2012an}. 
A likelihood function ${\cal L}$ is defined as the product of Poisson probabilities associated with the number of events in bins of the $\mT$ distributions in the signal regions and of the total yields in the control regions. 
The $\mT$ distributions in the SRs use the same binnings as described in Appendix~\ref{sec:app:mtbin}. 
%The statistical treatment for the background uses extrapolation factors to describe how the fitted background rates extrapolate from the control regions to the signal regions as well as among the various control regions.
%Rather than using extrapolation coefficients to derive the background rate, an equivalent parametrisation is used that employs strength parameters for the backgrounds normalised using control regions. Although $\mT$ shape information is not used in the extrapolations, the shapes of the $\mT$ distributions of these backgrounds in the signal region are taken from MC prediction, and consequently there are uncertainties on these $\mT$ shapes.
Each 
source of systematic uncertainty is parameterised by a corresponding nuisance parameter $\theta$ constrained by a Gaussian function.

The likelihood function can be expressed in the following formula:
\begin{equation}
{\cal L}(\mu, \mu_b)=P(N|\mu s+\mu_b b^{\text{exp}}_{\text{SR}})\times
P(M|\mu_b b^{\text{exp}}_{\text{CR}})
\end{equation}
where $N$ and $M$ are the number of data events in the signal and control
regions respectively, $s$ is the expected signal yield
in the signal region, 
$b^{\text{exp}}_{\text{SR}}$ and $b^{\text{exp}}_{\text{CR}}$ are expected background
yields in the signal and control regions respectively, $\mu$ is the signal
strength parameter, defined as the ratio of the measured $\sigma_H\times {\text{BR}}(H\to WW)$ to that predicted
~\footnote{The SM cross-section prediction is used to define $\mu$ for the NWA and LWA scenarios.} 
, and $\mu_b$ is the strength parameter for background $b$.
The backgrounds treated this way are the $WW$ background in the inclusive ggF and $N_{\text{jet}}=1$ VBF categories, and the top-quark background in the inclusive ggF and $N_{\text{jet}}\geq 2$ VBF categories. 

The full likelihood includes a product over $\mT$ bins and over lepton and
jet final states:
\begin{equation}\displaystyle
{\cal L}(\mu, \vec{\theta})=\left\{\prod_{j=0}^{N_{\text{category}}}
\prod_{i=1}^{N_{\text{bins}}}P(N_{ij}|\mu s_{ij}+\sum_m^{N_{\text{bkg}}}b_{ijm})\right\}\times \prod_{i=1}^{N_\theta}N(\tilde{\theta}|\theta)
\end{equation}
where the vector $\vec{\theta}$ represents the nuisance parameters and the
$N(\tilde{\theta}|\theta)$ are auxiliary measurements that constrain 
$\vec{\theta}$. Nuisance parameters corresponding to the various systematic
uncertainties and background determinations in the analysis can broadly be
divided into four types:
%\begin{enumerate}
\begin{description}
\item[Type I:] Systematics that do not change the $\mT$ shape (flat systematics) take
the form $\nu_{\text{flat}}(\theta)=\kappa^\theta$, where $\kappa$ is 
determined by measuring $\nu_{\text{flat}}$ at $\theta=\pm 1$. In this case, 
the constraint term on $\theta$ that is present in the likelihood is a unit
Gaussian, and $\kappa^{\theta}$ is log-normally distributed to prevent 
predicted event yields from taking unphysical values.
\item[Type II:] In the case that a systematic can affect the shape, the shape variation
is first separated into a flat component and a pure shape component, such that
varying the pure shape component has no effect on the expected rate. 
The flat component is treated as described above. The pure shape component 
uses vertical linear interpolation to estimate the variation, and so is 
distributed as a truncated Gaussian. Explicitly, $\nu_{\text{shape}}(\theta)=
1+\epsilon\theta$, where $\epsilon$ is again determined by measuring 
$\nu_{\text{shape}}$ at $\theta=\pm 1$ and the constraint is a unit Gaussian. 
The truncation is imposed such that $\nu_{\text{shape}}(\theta<\frac{-1}{\epsilon})=0$. Systematic sources can have both a normalisation (type I) and shape 
component. In such cases, the same $\theta$ is shared between both functions 
$\nu_{\text{flat}}(\theta)$ and $\nu_{\text{shape}}(\theta)$.
\item[Type III:] The third type pertains to the treatment of purely statistical 
uncertainties, i.e., uncertainties from MC statistics or data-driven methods.
The constraint represents an auxiliary measured number of events $\tilde{\theta}$ with an expected number $\theta\lambda$. In other words, it is the Poisson 
probability $P(\tilde{\theta}|\theta\lambda)=\frac{(\theta\lambda)^{\tilde{\theta}}e^{\theta\lambda}}{\tilde{\theta}!}$. For an uncertainty $\sigma_b$ on an
expectation $b_0$, $\tilde{\theta}=\lambda=\frac{b^2_0}{\sigma^2_{b_0}}$, and 
$\nu_{\text{stat}}(\theta)=\theta$.
\item[Type IV:] The final type arises when a high-statistics data control region is used
to constrain the normalisation of a background. This is similar to the third
type. The constraint is the Poisson probability $P(\tilde{\theta}|\lambda(\theta))=\frac{(\theta\lambda(\theta))^{\tilde{\theta}}e^{\lambda(\theta)}}{\tilde{\theta}!}$, $\tilde{\theta}$ being the observed events in the CR. The expected
number of events is $\lambda=\mu s+\theta b_{\text{target}}+\sum_i^{N_{\text{bkg}}-1}b_i$, where $b_{\text{target}}$ is the background targeted by the CR. 
This treatment properly takes into account the contamination in the CR due to 
both the signal and other backgrounds. The background strength parameters 
multiply expected backgrounds anywhere that the respective backgrounds are 
present.
%\end{enumerate}
\end{description}
It is possible for a single nuisance parameter to affect multiple signal and
background rates in a correlated manner, while other nuisance parameters can
pertain to specific physics processes. 
%As mentioned before, in the case of shape systematics care is taken to only use shape variations which 
Shape systematics are considered only for shape variations that
are statistically significant given the size of MC samples (see Figure~\ref{fig:shape_top} and \ref{fig:shape_ww}). 
For a given systematic source, spurious variations of the $\mT$ shape can arise from poor MC
statistics, leading to fit convergence problems if that systematic is used
as a shape variation as well as a normalisation variation. The $\mT$ shape 
variation of the total background is dominated by the normalisation variations 
of individual backgrounds. Since individual backgrounds are not equally 
distributed across the $\mT$ spectrum, systematics that lead to normalisation 
variations of individual backgrounds result in variations in the $\mT$ shape 
of the total background.

The analysis has signal regions optimised for the
ggF and VBF signal production modes, but the presence of both signal processes
is accounted for in all signal regions. Limits are obtained separately for
ggF and VBF production in all interpretations. To derive the expected limits
on the ggF production mode, the VBF production cross section is set to zero,
so that the expected limits correspond to the background-only hypothesis.
To derive the observed limits on the ggF (VBF) production mode, the VBF (ggF)
production cross section is treated as a nuisance parameter in the fit and 
profiled using a flat prior, as is used for the normalisation of backgrounds 
using CRs. This approach avoids making any assumption on the presence or 
absence of the signal in other production modes, by using the signal regions
themselves to set the normalisation of the production mode not being tested. 

The modified frequentist method known as CL$_s$, combined with asymptotic 
approximation, is used to compute 95\% CL upper limits. The method uses a
test statistic $q_\mu$, a function of the signal strength $\mu$.
The test statistic is defined as:
\begin{equation}
q_\mu=-2\ln\left(\frac{{\cal L}(\mu; \hat{\bf \theta}_\mu)}{{\cal L}(\hat{\mu};\hat{\bf \theta})}\right)\,.
\end{equation}
The denominator does not depend on $\mu$. The quantities $\hat{\mu}$ and 
$\hat{\bf \theta}$ are values of $\mu$ and ${\bf \theta}$, respectively, that
unconditionally maximise ${\cal L}$. The numerator depends on the values 
$\hat{\bf \theta}_\mu$ that maximise ${\cal L}$ for a given value of $\mu$.


Type III (statistical) uncertainties are applicable to each $\mT$ bin in each signal and
control region in the analysis. The applicability of uncertainties of types I
and II depends on the estimation method of particular backgrounds, as discussed 
below. 
\begin{description}
%\begin{itemize}
\item[Continuum $WW$:] Systematic uncertainties of types I and II are applicable. Most of
the experimental uncertainties are common between the control and signal 
regions, and thus largely cancel out. In the $N_{\text{jet}}\geq 2$ VBF category, the normalisation is 
taken from MC prediction, so that uncertainties of types I and II are applicable.
\item[$t\bar{t}+$ single top:] 
In the inclusive ggF and $N_{\text{jet}}\geq 2$ VBF categories, the 
normalisation is constrained from control regions, so that uncertainties of
types I and II are applicable, but again most experimental systematic 
uncertainties cancel out in the extrapolation.
\item[$W+$ jets:] A data-driven method is used to determine both the 
normalisation and the shape. There is a systematic uncertainty on the fake rate,
%that includes a shape component, 
so that uncertainties of types I are 
applicable.
\item[$Z/\gamma^\ast+$ jets, $WZ/ZZ/W\gamma$:] These (generally small) 
backgrounds are estimated purely using MC, so that uncertainties of type I are applicable.
\end{description}
%\end{itemize}
Finally, the signal prediction is estimated from MC, and uncertainties of 
types I and II are applicable.
\FloatBarrier
