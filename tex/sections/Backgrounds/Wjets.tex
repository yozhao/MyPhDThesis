\section{$W+$jets background}
\label{sec:bkg:wj}

Production of $W$~bosons in association with jets may enter the SR when a jet is misidentified as a lepton. Due to the difficulties in accurately modelling the misidentification process in simulation,
the $W+$jets background contribution is estimated using the fake-factor (FF) based data-driven method developed for the SM $H\rightarrow WW$ analysis~\cite{ATLAS:2014aga}.
The estimation uses a sample of events satisfying all event selection criteria, except one of the two lepton candidates fails to meet the quality criteria for ``fully identified'' leptons but satisfies a less restrictive selection (denoted as ``anti-identified''). These selection criteria are listed in Table~\ref{tab:antiid}.
\begin{table}[!htbp]
 \centering
 \caption{The requirements for fully identified and anti-identified leptons.}
 \label{tab:antiid}
 \resizebox{\textwidth}{!}{
 \small
   \begin{tabular}{c|c|c|c}
 \hline
    Id electron &    Anti-id electron &   Id muon       &   Anti-id muon\\
 \hline
 \multicolumn{2}{c|}{$\pt >$ 15 $\mathrm{GeV}$}                                      &   \multicolumn{2}{c}{$\pt >$ 15 $\mathrm{GeV}$}\\
  \multicolumn{2}{c|}{$|\eta|  <2.47$,excluding  $1.37<|\eta|< 1.52$}  &    \multicolumn{2}{c}{$|\eta|  <2.45$}  \\
\multicolumn{2}{c|}{$|z_{0}\sin\theta|<0.5$ mm } &   \multicolumn{2}{c}{$|z_{0}\sin\theta|<0.5$ mm }\\
 Pass LHTight if $\pT < 25\GeV$                                         & \multirow{2}{*}{Pass LHLoose}                                         &  \multicolumn{2}{c}{Pass Quality Tight if $\pt < 25$ GeV }\\
 Pass LHMedium if $\pT > 25\GeV$                               &                                                                                                       &  \multicolumn{2}{c}{Pass Quality Medium if $\pt > 25$ GeV }  \\
 \multicolumn{2}{c|}{$|d_{0}|/\sigma(d_{0})<5$ }                                                                                                               & $|d_{0}|/\sigma(d_{0}) <  3$   & $|d_{0}|/\sigma(d_{0}) <  6$  \\
 Pass Gradient isolation                                                        & Veto against identified electron                                      & Pass Gradient Isolation       &  Veto against identified muon\\
 \hline
 \end{tabular}
}
\end{table}

The anti-identified sample is orthogonal to the identified sample and has loosened isolation and impact parameter (likelihood identification) criteria for muons (electrons).
From this data sample the non-$W+$jets contribution, dominated by top quark and $WW$ backgrounds, is subtracted based on MC predictions.
 The $W+$jet purity of the samples is 46\%, 59\% and 22\% for the quasi-inclusive ggF, $N_{\text{jet}}=1$ and $N_{\text{jet}}\geq 2$ VBF categories, respectively.

The $W+$jets contamination in the signal region is determined by scaling the number of events in the selected data sample by the fake-factor, an extrapolation factor, which is measured in a data sample of di-jet events. The di-jet sample is collected with prescaled low-$\pT$ single lepton triggers. Events are selected with exactly one fake candidate object, back-to-back with the leading jet. The electroweak processes in the di-jet event sample, dominated by $W$+jets and $Z/\gamma^\ast$ background contributions, are subtracted. 
The fake-factor is the ratio of the number of fully identified leptons to the number of anti-identified leptons, measured in bins of lepton $\pt$ and $\eta$. 
The estimation of $W+$jets background can be expressed by the following equation:
\begin{eqnarray}
N^{\text{$W$+jets}}_{\text{id+id}}&=&N^{\text{$W$+jets}}_{\text{id+anti-id}}\times {\text{FF}}\nonumber \\
&=&(N_{\text{id+anti-id}}-N^{\text{EW}}_{\text{id+anti-id}})\times \frac{N_{\text{id}}}{N_{\text{anti-id}}}\,,\label{eq:wjets}
\end{eqnarray}
where $N^{\text{$W$+jets}}_{\text{id+anti-id}}$ is the number of events in the selected data sample,
$N_{\text{id+anti-id}}$ and $N^{\text{EW}}_{\text{id+anti-id}}$ correspond to the number of data events and the EW background contribution that is subtracted from the data,
and $\frac{N_{\text{id}}}{N_{\text{anti-id}}}$ is the definition of the FF, a ratio of the number of fully identified leptons to the number of anti-identified leptons.

%
%The fake-factors are determined from fake-enriched di-jet events in data in bins of anti-identified lepton $\pt$ and $\eta$. 
The measured fake-factors are applied to the anti-id lepton in the $W$+jets control sample. Most of the time the anti-id lepton is softer, not triggered lepton in the event. To measure a suitable fake-factor for these anti-id leptons a data sample collected using the low-$\pT$ single-lepton prescaled trigger ({\texttt HLT\_e12\_lhvloose\_nod0\_L1EM10VH} for electrons and {\texttt {HLT\_mu14\_L1\_MU10} for muons) is used. The corresponding fake-factors are called ``nominal" fake-factors and are shown in Figure~\ref{fig:ff_nominal_lvlv}. The original $\pT$ distributions of the fully identified and anti-identified leptons before the EW background subtraction are shown in Figure~\ref{fig:dijetnominal}.
When the $\pT$ of the anti-id lepton is sufficiently high, it can be that the anti-id lepton in the $W$+jets control sample is the only lepton which fires one of the un-prescaled
single-lepton triggers used by the analysis, listed in Table~\ref{tab:trigger}. 
The relative fraction of the two samples in the $W$+jets control sample is 92\% and 8\%.
If the nominal fake-factor is applied to this latter event sample, a small trigger bias is introduced in the $W$+jets background estimation. To avoid this trigger bias, separate fake-factors are extracted using a data set from the di-jet events triggered by the un-prescaled triggers mentioned above. These fake-factors are called ``triggered" fake-factors and are shown in Figure~\ref{fig:ff_trigger_lvlv}.
The difference between these two sets of fake-factors are expected given the different isolation requirements used in the prescaled and un-prescaled triggers. Also the triggered fake-factors are period dependent as the triggers have changed from one period to another (see Table~\ref{tab:trigger}).
%\begin{figure}[htbp]
\begin{figure}[!htbp]
 \centering
\includegraphics[width=0.495\textwidth]{fig/note/FFmultiple_Pt_etaBin1_Electron-2.pdf}
\includegraphics[width=0.495\textwidth]{fig/note/FFmultiple_Pt_etaBin2_Electron-2.pdf}
\includegraphics[width=0.495\textwidth]{fig/note/FFmultiple_Pt_etaBin1_Muon-2.pdf}
\includegraphics[width=0.495\textwidth]{fig/note/FFmultiple_Pt_etaBin2_Muon-2.pdf}
 \caption{\small Nominal fake-factors (data points in magenta) determined from fake-enriched di-jet samples in data for electrons (top) and muons (bottom) and in central (left) and forward (right) regions. The blue and red lines correspond to variations of the fake-factors by scaling the electroweak subtraction by $\pm 20$\%. The error bars show the statistical uncertainty of the corresponding data samples.}
 \label{fig:ff_nominal_lvlv}
\end{figure}

%\begin{figure}[htbp]
\begin{figure}[!htbp]
 \centering
\includegraphics[width=0.45\textwidth]{fig/note/e-CutID_Nominal-lepPt-log.pdf}
\includegraphics[width=0.45\textwidth]{fig/note/e-CutAntiID_Nominal-lepPt-log.pdf}
\includegraphics[width=0.45\textwidth]{fig/note/m-CutID_Nominal-lepPt-log.pdf}
\includegraphics[width=0.45\textwidth]{fig/note/m-CutAntiID_Nominal-lepPt-log.pdf}
 \caption{Distributions of $\pT$ of fully identified (left) and anti-identified (right) electrons (top) and muons (bottom) selected from di-jet events in data for determining the nominal fake-factors.}
 \label{fig:dijetnominal}
\end{figure}

%\begin{figure}[htbp]
\begin{figure}[!htbp]
 \centering
\includegraphics[width=0.495\textwidth]{fig/note/FFmultiple_Pt_etaBin1_Electron.pdf}
\includegraphics[width=0.495\textwidth]{fig/note/FFmultiple_Pt_etaBin2_Electron.pdf}
\includegraphics[width=0.495\textwidth]{fig/note/FFmultiple_Pt_etaBin1_Muon.pdf}
\includegraphics[width=0.495\textwidth]{fig/note/FFmultiple_Pt_etaBin2_Muon.pdf}
 \caption{\small Triggered fake-factors determined from fake-enriched di-jet samples in data for electrons (top) and muons (bottom) and in central (left) and forward (right) regions. The blue, magenta and red points correspond to different periods of data taking. The error bars show the statistical uncertainty of the corresponding data samples.} 
 \label{fig:ff_trigger_lvlv}
\end{figure}

%
A closure test has also been performed by comparing the $W$+jets MC prediction with the corresponding estimation using the fake-factor method. The MC prediction is based on the selection with two fully identified leptons and the fake-factor estimation is obtained by applying the fake-factors determined with the same MC sample to an event sample selected with one fully identified lepton and one anti-identified lepton.
The comparison results are shown in Table~\ref{tab:closure} and Figure~\ref{fig:closure}. The statistics of the $W$+jets is very limited in particular in the VBF phase space. However, agreement is observed within the limited statistical precision.

%\begin{table}[htb]
\begin{table}[!htbp]
  \caption{Comparison of the $W$+jets estimation based on the fake-factor method with the corresponding MC prediction at the pre-selection level and in different SRs. The uncertainties of the event yields are statistical only.}
\label{tab:closure}
  \vspace{0.4cm}
  \centering
  \begin{tabular}{c|cc}
   \hline
Category & FF estimation & MC prediction \\\hline
Pre-selection & $4857 \pm 285$ & $4756\pm 130$ \\
ggF SR & $742\pm 104$ & $656\pm 47$ \\
VBF $N_{\text{jet}}=1$ SR & $73\pm 53$ & $29\pm 20$\\
VBF $N_{\text{jet}}\ge 2$ SR & 0 & $1.8\pm 1.3$\\\hline
\end{tabular}
\end{table}

%\begin{figure}[htbp]
\begin{figure}[!htbp]
 \centering
\includegraphics[width=0.45\textwidth]{fig/note/Fake_closure_Mll.pdf}
\includegraphics[width=0.45\textwidth]{fig/note/Fake_closure_MT.pdf}
 \caption{\small Distributions of $m_{\ell\ell}$ (left) and $\mT$ (right) for the estimation obtained with the fake-factor method (data points) and the corresponding MC prediction (histogram) at the pre-selection level.
The error bands in the lower plots correspond to the statistical uncertainty of the $W$+jets MC sample.}
 \label{fig:closure}
\end{figure}

%
The fake-factor method has also been applied to a different event sample in which all the cuts are identical to the nominal analysis except that the charge of the two leading leptons is the same (so-called the Same-Sign (SS) sample). In this sample, the $W$+jets background is one of the leading background contributions as it is shown in Figure~\ref{fig:SSfake} at the pre-selection level (left) and a region similar to the quasi-inclusive ggF signal region (right). Agreement between data and background contributions provides a nontrivial validation of the fake-factor method.
%\begin{figure}[htbp]
\begin{figure}[!htbp]
 \centering
\includegraphics[width=0.45\textwidth]{fig/note/SS_Presel_subPt-lin.pdf}
\includegraphics[width=0.45\textwidth]{fig/note/SS_InclSR_subPt-lin.pdf}
 \caption{\small Comparison of the same-sign data sample with the corresponding background contributions at the pre-selection level for $\pT^{\ell,\text{sublead}}$ (left) and in a region similar to the quasi-inclusive ggF signal region for $m_{\ell\ell}$ (right). 
The hatched band in the upper panel and the shaded band in the lower panel show the 
statistical uncertainty only on the background predictions.}
 \label{fig:SSfake}
\end{figure}

The dominating systematic uncertainty on the $W+$jets estimation originates from sample composition differences between the di-jets and $W+$jets samples. 
All systematic uncertainties associated to this background estimate are discussed in Section~\ref{sec:sys:wjsys}.
\FloatBarrier
