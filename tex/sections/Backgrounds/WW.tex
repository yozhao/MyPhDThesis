\section{$WW$ background}
\label{sec:bkg:ww}

The same selection are used in the $WW$ CR for the quasi-inclusive ggF category ($WW$ ${\rm CR_{ggF}}$) as for the SR, except for the cut on $|\Delta\eta_{\ell\ell}|$ which is reversed so that the CR and SR are orthogonal. The selection conditions are shown in Table~\ref{tab:selcompback_lvlv}.

The $\mT$ distributions of the $q\bar{q}\to WW$ {\sc Sherpa} MC sample in the SR$_\text{ggF}$ and $WW$ CR$_\text{ggF}$ are compared with the corresponding predictions at generator level, by combining the NNLO QCD calculations using the {\sc Matrix} package~\cite{NNLOQCD} with the NLO electroweak (EW) corrections~\cite{NLOEW}.
Whilst the integrated yields of the distributions agree within up to 3\% in both the SR$_\text{ggF}$ and the $WW$ CR$_\text{ggF}$, a small $\mT$ shape difference is still observed in particular in the SR. 
The $\mT$ distributions  are thus corrected through a reweighting to the combined NNLO QCD and NLO EW predictions for the {\sc Sherpa} MC samples.
More details could be found in Appendix~\ref{sec:app:wwrew}.

For the $gg\to (h^\ast)\to WW$ process, in which the SM 125\GeV Higgs boson is off-shell, it is modelled at LO using the \textsc{Sherpa} generator with a $k$-factor of 1.7. The $k$-factor is used to account for higher-order cross-section corrections. An uncertainty of 60\% has been assigned, following the studies in Refs.~\cite{Melnikov:2015laa,Caola:2015rqy,Bonvini:2013jha,Li:2015jva}. 
This $k$-factor of 1.7 is actually applied on top of another recommended $k$-factor of 0.91 for \textsc{Sherpa} diboson samples to account for a different EW scheme with regard to that in the \textsc{Powheg} diboson samples\cite{Gutschow:2113154}.

A comparison of the distributions for the common variables, but with the cut on the variable removed once at a time, between data and MC in the CR is presented in Figure~\ref{fig:wwggfcr:var}.
%\begin{figure}[htbp]
\begin{figure}[!htbp]
 \centering
 \includegraphics[width=0.45\textwidth]{fig/nm1/log/ggFWWCR-Mll.pdf}
 \includegraphics[width=0.45\textwidth]{fig/nm1/log/ggFWWCR-LeadPt.pdf}
 \includegraphics[width=0.45\textwidth]{fig/nm1/log/ggFWWCR-SubPt.pdf}
 \includegraphics[width=0.45\textwidth]{fig/nm1/log/ggFWWCR-WMT.pdf}
 \includegraphics[width=0.45\textwidth]{fig/nm1/log/ggFWWCR-NBJet.pdf}
 \caption{\small Comparison of distributions between data and MC in the ggF $WW$ CR where one of these cuts is removed from the selection: $m_{\ell\ell}$ (top left),
$\pt^{\ell,\text{lead}}$ (top right), $\pt^{\ell,\text{sublead}}$ (middle left), max($\mT^{W}$) (middle right) and $N_{\text{$b$-jet}}$ (bottom).
The hatched band in the upper panel as well as the shaded band in the lower panel shows the combined statistical and experimental uncertainties on MC.
The last bin contains the overflow. Normalisation factors obtained from a comparison between data and predictions are also applied. The red dashed vertical line indicates the cut value.}
\label{fig:wwggfcr:var}
\end{figure}

The post-fit normalisation factor that is obtained from the simultaneous fit to data for the $WW$ contributions in the quasi-inclusive ggF categories is $1.14\pm 0.09$. The uncertainty quoted here includes both statistical and systematic uncertainties. The purity of the $WW$ background in the control region after the simultaneous fit is $51\%$.

In order to increase the data statistics, the $WW$ CR for the $N_{\rm jet}=1$ VBF category ($WW$ ${\rm CR_{VBF1J}}$) uses a slightly different selection (shown in Table~\ref{tab:selcompback_lvlv}) from the one in the SR, but still orthogonal to the SR. 
The normalisation factor obtained from the same simultaneous fit for the $WW$ contribution in the
$WW$ ${\rm CR_{VBF1J}}$ is $1.0\pm 0.2$, where the uncertainty quoted includes the full statistical and systematic error. The post-fit purity of the $WW$ background in the control region is $44\%$.

The comparison of the distributions between data and MC for the common variables in this CR is shown in Figure~\ref{fig:wwvbf1jcr:var}.
%\begin{figure}[htbp]
\begin{figure}[!htbp]
 \centering
 \includegraphics[width=0.45\textwidth]{fig/nm1/log/VBFWW1JCR-LeadPt.pdf}
 \includegraphics[width=0.45\textwidth]{fig/nm1/log/VBFWW1JCR-SubPt.pdf}
 \includegraphics[width=0.45\textwidth]{fig/nm1/log/VBFWW1JCR-NBJet.pdf}
 \caption{\small Comparison of distributions between data and MC in the VBF 1-jet $WW$ CR: $\pt^{\ell,\text{lead}}$ (top left),
$\pt^{\ell,\text{sublead}}$ (top right) and $N_{\text{$b$-jet}}$ (bottom), where the cut is removed for $N_{\text{$b$-jet}}$.
The hatched band in the upper panel as well as the shaded band in the lower panel shows the combined statistical and experimental uncertainties on MC.
The last bin contains the overflow. Normalisation factors obtained from a comparison between data and predictions are also applied. The red dashed vertical line indicates the cut value.}
\label{fig:wwvbf1jcr:var}
\end{figure}

The contribution of the $WW$ background in the $N_{\rm jet}\geq 2$ VBF category is small (approximately 20\%). Its prediction is taken from simulation, since it is difficult to isolate a kinematic region with a sufficient number of $WW$ events without a large contamination from the top quark background.

As with the top quark backgrounds, the cutflow of the pre-fit event yields for this CR is also shown in Table~\ref{tab:cutflowcrlvlv}.

Figure~\ref{fig:WWCR_lvlv} shows the post-fit $\mT$ distributions in the $WW$ ${\rm CR_{ggF}}$ and ${\rm CR_{VBF1J}}$. The different backgrounds are scaled to the event yields that are obtained from the simultaneous fit. 
As with the top quark control regions, only the integrated event yields of the $WW$ control regions are used in the fit. 
%\begin{figure}[tbp]
\begin{figure}[!htbp]
 \centering
\includegraphics[width=0.48\textwidth]{fig/mt/postfit/prefit-WithNF-GGFWWCR-rescale1-NWA-ggH800-public.pdf}
\includegraphics[width=0.48\textwidth]{fig/mt/postfit/prefit-WithNF-VBFWW1JCR-rescale1-NWA-ggH800-public.pdf}
 \caption{\small Post-fit $\mT$ distributions in the quasi-inclusive ggF and $N_{\rm jet}=1$ VBF $WW$ 
control regions. The last bin contains the overflow. 
The hatched band in the upper panel as well as the shaded band in the lower panel shows the combined statistical and experimental uncertainties on MC.
The top quark and $WW$ backgrounds are scaled by the indicated normalisation factors that are obtained from the simultaneous fit to all SRs and CRs. The event yield of the heavy Higgs boson signal is normalised to the expected limits on $\sigma_H\times {\rm BR}(H\to WW)$, and is shown for masses of 700\GeV and 2\TeV, with an NWA lineshape.}
 \label{fig:WWCR_lvlv}
\end{figure}
\FloatBarrier
