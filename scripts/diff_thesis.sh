pathold=../history/MyPhDThesis
cat ${pathold}/thesis.tex | grep -v "^%" | grep -v "^  %" > ${pathold}/tmp_old.tex
cat thesis.tex | grep -v "^%" | grep -v "^  %" > tmp_new.tex
latexdiff --flatten ${pathold}/tmp_old.tex ./tmp_new.tex > thesis-diff.tex
rm thesis-diff.aux  thesis-diff.bcf  thesis-diff.log  thesis-diff.out  thesis-diff.pdf  thesis-diff.run.xml thesis-diff.toc thesis-diff.bbl  thesis-diff.blg
pdflatex thesis-diff
pdflatex thesis-diff
rm -f ${pathold}/tmp_old.tex tmp_new.tex
rm thesis-diff.aux  thesis-diff.bcf  thesis-diff.log  thesis-diff.out thesis-diff.run.xml thesis-diff.toc thesis-diff.bbl  thesis-diff.blg
