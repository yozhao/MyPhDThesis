#!/bin/bash

## Check sentences starting with a single character, e.g. 'A is a constant ...'

for a in {A..Z}
do
  grep -r "^${a} is" tex/sections
  grep -r ". ${a} is" tex/sections
done
