#!/bin/bash

secpath=${PhDThesisPATH}/tex/sections

texfiles=( $(find $secpath -name "*.tex") )

### Check sentences starting with a single character, e.g. 'A is a constant ...'
#if false;then
#  for a in {A..Z}
#  do
#    grep -r "^${a} is" tex/sections
#    grep -r ". ${a} is" tex/sections
#  done
#fi
#
### check roman style
#if true;then
#  echo "Check if it is roman style for all units and text in equations ==>"
#  for file in ${texfiles[*]};do
#    [[ $file != */Introduction.tex ]] && continue; # debug only
#    echo "...File: $file"
#  done
#fi

for file in ${texfiles[*]};do
  #[[ $file != */Introduction.tex ]] && continue; # debug only
  echo "File: $file"

  # check and ensure roman style for all units and text in equations
  if true;then
    grep -r 'fb^{-1}' $file
    grep -r '{T}' $file
  fi
done
