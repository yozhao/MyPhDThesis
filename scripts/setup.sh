#!/bin/bash

## setup PhDThesisPATH
export PhDThesisPATH=$(pwd)

## setup PATH to include: ./scripts/ and ATLAS latex env
PATH=${PhDThesisPATH}/scripts:$PATH
#PATH=/afs/cern.ch/sw/XML/texlive/latest/bin/x86_64-linux:$PATH
